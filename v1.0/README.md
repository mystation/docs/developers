---
home: true
heroImage: http://mystation.fr/static/img/logo.png
heroText: MyStation
tagline: for developers
actionText: Get Started →
actionLink: /guide/introduction/
features:
- title: Node.js
  details: Javascript, its promising future and Node.js
- title: Vue + Webpack
  details: Vue-powered UI with Webpack
- title: Laravel
  details: An API server developed with the famous framework Laravel
footer: GPL v3 Licensed | Copyright © 2019-present Hervé Perchec
---
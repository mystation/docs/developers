---
prev: /guide/apps/
---

# Develop an app for MyStation

## Create new project

::: tip NOTE
Before starting, make sure you have MyStation installed ([documentation](/guide/get-started/))
:::

Clone the repository (replace `myapp` by the name of your app):

```bash
git clone https://gitlab.com/mystation/adk.git myapp
```

Install dependencies:

```bash
npm install
```

Edit the file `package.json` to update app informations:

```json
{
  "name": "myapp",
  "version": "1.0.0",
  "description": "MyStation - MyApp app",
  "private": false,
  "repository": {
    "type": "git",
    "url": "https://gitlab.com/myapp.git"
  },
  "author": "John Doe <john@doe.com>",
  ...
```

Update the file `src/app.config.json`:

```json
{
    "name": "myapp",
    "version": "1.0.0",
    "mystation_version": "1.0.0",
    "title": "MyApp",
    "pancakes": [],
    "color": "#14DD56",
    "resources": []
}
```

Don't forget to update the git repository remote: (replace `<repository_url>`):
```bash
git remote rm origin
git remote add origin <repository_url>
```

Generate README.md file:

```bash
npm run readme
```

::: tip
Now you can do your first commit ;)
:::

## Configuration


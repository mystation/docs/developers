---
editLink: false
next: /guide/apps/develop/
---

# MyStation Apps

The goal of MyStation is to be involvable. The "app" system has been designed to do it.
You can consult the [official apps list](http://mystation.fr/apps/) or contribute and develop your own app for MyStation.

Would you like to develop an app?
Follow the [documentation](/guide/apps/develop/) and find [examples](/guide/apps/examples/) that will help you!

# Raspberry Pi

## Raspberry Pi OS image

Based on default image:

`Raspberry Pi OS (32-bit) - RELEASED: 2020-05-27`

### Modifications

#### Image configuration (optional)

> This step is optional. It prevents default RPI OS behavior from automatically extend partition to SD card max size.
> It is important if you plan to share the image later or if you want to optimize img size.

Before SD Card first utilisation (just after the image writing with Raspberry Pi Imager for example):
- Browse SD card content
- Find *cmdline.txt* (this file contains command runned at boot)
- Duplicate this file to *old_cmdline.txt* (for example)
- Edit content of *cmdline.txt*:
  - remove 'init=/usr/lib/raspi-config/init_resize.sh'
- Insert SD Card into the Raspberry and boot
- Run these commands:
  ```bash
  # First, get START sector of partition
  echo $(sudo parted -m /dev/mmcblk0 unit s print | tr -d 's' | grep -e "^2:" | cut -d ":" -f 2)
  # Increase rootfs partition size: use fdisk
  # mmcblk0 is the disk
  sudo fdisk /dev/mmcblk0
  ```
  - `d` to delete
  - `2` to delete partition 2
  - `n` to create new partition
  - `p` for primary
  - `2` to create partition 2
  - Start: enter previously found START sector
  - `+6G` to resize to 6GB
  - Choose 'No' for signature removing prompt
  - `w` to write and apply changes
  Reboot `sudo reboot`
  Then, resize file system to new partition size:
  ```bash
  # mmcblk0p2 is the rootfs partition (id: 2)
  sudo resize2fs /dev/mmcblk0p2
  ```

#### 1 - raspi-config

- Enable SSH
- Set locales:
  - en_GB utf_8
  - en_US utf_8 (default)
  - fr_FR utf_8
- Change hostname to: `rpi-mystation`

#### 2 - Packages installation

- Update first:
  ```bash
  sudo apt-get update && sudo apt-get upgrade
  ```
- apache2:
  ```bash
  sudo apt-get install apache2
  ```
- php7.3, composer & required extensions:
  ```bash
  sudo apt-get install php7.3 composer php-xml php-zip php-mysql php7.3-gd
  ```
- mariadb-server:
  ```bash
  sudo apt-get install mariadb-server
  ```
- node & npm:
  ```bash
  # Add repository
  curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
  # Install
  sudo apt-get install nodejs
  ```

#### 3 - Users

- Create *admin* as sudoer (login: `admin`, password: `admin`):
  ```bash
  sudo adduser admin
  sudo adduser admin sudo
  sudo visudo /etc/sudoers.d/010_admin-nopasswd
  # insert content:
  admin ALL=(ALL) NOPASSWD: ALL
  # give rights to www-data
  sudo visudo
  # add to the end:
  # Allow www-data to execute command
  www-data ALL=(ALL) NOPASSWD:ALL
  ```
- Lock *pi* user:
  ```bash
  sudo usermod -L pi
  ```

#### 4 - Apache configuration

- Disable default site:
  ```bash
  sudo a2dissite 000-default
  ```

#### 5 - MySQL configuration

- Disable *root* user & create *admin* user:
  ```bash
  sudo mysql
  # SQL script:
  CREATE USER 'admin'@'localhost' IDENTIFIED BY 'admin';
  GRANT ALL ON *.* TO 'admin'@'localhost';
  FLUSH PRIVILEGES;
  REVOKE ALL ON *.* FROM 'root'@'localhost';
  ```
  Reboot system `sudo reboot`

#### 6 - Extras

Login as `admin`

- Add some aliases
  ```bash
  # as 'admin' user
  echo "alias ll='ls -lArth'" >> ~/.bash_aliases
  ```
- Disable 'Welcome to Raspberry Pi' desktop wizard:
  ```bash
  sudo rm /etc/xdg/autostart/piwiz.desktop
  sudo reboot now
  ```

#### 7 - MyStation installation

- Create mystation directory:
  ```bash
  sudo mkdir /mystation
  ```
- Change owner to admin:
  ```bash
  sudo chown admin:admin /mystation
  ```
- Clone sources from git:
  ```bash
  cd /mystation
  git clone https://gitlab.com/mystation/mystation.git .
  ```
- Execute 'install' script:
  ```bash
  bash /mystation/install/install.sh
  ```

#### Post install configuration (optional)

> This step is optional but required if you have followed optional *Image configuration* first step.

Don't forget to edit *cmdline.txt* file (add this content BEFORE reading SD card to create img):
```bash
init=/usr/lib/raspi-config/init_resize.sh
# Note that old file is saved as 'old_cmdline.txt'
# (or the name you gave) to retrieve content
```

It will reactivate partition auto-resizing (default behavior).
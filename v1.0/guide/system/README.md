---
editLink: false
prev: /guide/monitor/
next: /guide/system/api-server/
---

# System

This chapter concerns the global system.

MyStation needs a database to work and uses natively a MySQL database. You can find the detailed documentation [here](/guide/system/database/).

The [API Server](/guide/system/api-server/) enables the communication between the user interface and the database.

Finally, MyStation uses different tools (called *processes*) like an UDP server, a watchdog, etc...

Please check the documentation of these [system processes](/guide/system/processes/).

## Requirements

MyStation needs some dependencies to work correctly:

- Apache2 v2.4.38 (Raspbian)
- PHP v7.3.X
- Composer v1.8.X
- MySQL (mariadb) v10.3.X
- Node.js v10.21.X & NPM v6.14.X
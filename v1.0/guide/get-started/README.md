---
next: /guide/ui/
---

# Get started

MyStation is designed to be installed on a Raspberry Pi with the last version of Raspberry Pi OS ([https://www.raspberrypi.org/downloads/raspberry-pi-os/](https://www.raspberrypi.org/downloads/raspberry-pi-os/)).
All specifications are available in the [System > Raspberry Pi](/guide/system/raspberry-pi) section.

Please consult the repository MyStation group on Gitlab.
> [https://gitlab.com/mystation/](https://gitlab.com/mystation/)

```bash
# Clone the repository
git clone https://gitlab.com/mystation/mystation.git
# Start initialization
npm run init
# Take a coffee ;)
```

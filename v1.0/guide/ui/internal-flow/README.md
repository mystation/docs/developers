---
prev: /guide/ui/
---

# Internal flow

[[toc]]

## Presentation

<mermaid class="mermaid-flowchart" filePath="/guide/ui/internal-flow/mermaid/internal_flow.mmd"></mermaid>

## Vue instance

::: tip Vue.js framework
Please check the official Vue.js documentation: [https://vuejs.org/v2/guide/](https://vuejs.org/v2/guide/)
:::

The _MyStation_ interface uses the Vue.js framework, everything start with a root Vue instance.

1. 🧱 Vue instance creation from _MyStation_ main properties:
   
   - [internationalization](#internationalization) (i18n)
   - [store](#store) (vuex)
   - [boot()](#initialization) method
   - each [service](#services) directly as an instance property

2. 🔒 Assign to `window.__MYSTATION_VUE_INSTANCE__`

3. ⏳ Dynamic call of _async_ `this.boot()` method during the  __'created'__ hook of the Vue instance. More details in [the initialization section](#initialization)

> Some dependencies of _MyStation_ are shared and available everywhere:

```javascript
/* MyStation instance */
window.__MYSTATION_VUE_INSTANCE__
/* Shared dependencies */
// axios
window.lib.axios
// color
window.lib.color
// lodash
window.lib._.camelcase
window.lib._.capitalize
window.lib._.kebabcase
window.lib._.maxBy
window.lib._.minBy
window.lib._.snakecase
window.lib._.sortBy
window.lib._.uppercase
// vue
window.lib.Vue
```
For more informations about versions, check the dependencies list of _MyStation_.

## MyStation functioning

### Internationalization

The interface translations are managed by the plugin [vue-i18n](https://kazupon.github.io/vue-i18n/) for Vue.js.

> The errors (messages destined to the user) are separately translated to be used by the [ErrorManager](/guide/ui/api/mystation/services/error-manager) service

#### Supported langs

> Translations files are available in the code sources in JSON format

| Lang     | locale | source |
| -        | -      | -      |
| English  | en     | [en.json](https://gitlab.com/mystation/mystation-front/-/blob/master/src/i18n/en.js) |
| French   | fr     | [fr.json](https://gitlab.com/mystation/mystation-front/-/blob/master/src/i18n/fr.js) |

::: see-also
- [Global API → I18n](/guide/ui/api/i18n)
:::

### Store

::: tip Vuex store
Please check the documentation of the Vuex plugin for Vue.js: [https://vuex.vuejs.org/guide/](https://vuex.vuejs.org/guide/)
:::

_MyStation_ uses __Vuex__, the official plugin for data sharing between components or the Vue instances.

To simplify data management, the store is splitted by modules, in addition to the root store.

The store and its modules use the services:
- [APIManager](/guide/ui/api/mystation/services/app-manager) for API requests (__axios__ by default)
- [ErrorManager](/guide/ui/api/mystation/services/error-manager) for errors management

::: see-also
- [Global API → Store](/guide/ui/api/store)
:::

#### Store modules

##### Apps

Store module for apps data.

- Server data management
- Management of _MyStation_ official apps

_MyStation_ has a model for apps.

::: see-also
- [Global API → Store → Modules → Apps](/guide/ui/api/store/modules/Apps)
- [Global API → MyStation → Models → App](/guide/ui/api/mystation/models/App)
:::

##### Users

Store modules for users data.

- Server data management
- Current user data management
  - Login method
  - Authentication checking method (See: [Authentication](#authentication))
  - logout method

_MyStation_ has a model for users.

::: see-also
- [Global API → Store → Modules → Users](/guide/ui/api/store/modules/Users)
- [Global API → MyStation → Models → User](/guide/ui/api/mystation/models/User)
:::

### Services

Services are the internal system of _MyStation_ interface.

::: see-also
- [Global API → MyStation → Services](/guide/ui/api/mystation)
:::

#### Apps manager

The app manager makes possible:
- app installation (installation is triggered by an API request to the server, more information in [API Server](/guide/system/api-server))
- app uninstallation
- apps bundles loading (scripts are asynchronously loaded)
- apps translations registering
- apps components registering
  - main component
  - pancake (Dashboard element (widget) related to an app(optional))
- store modules registering (an app can have one or more modules for each resource)

::: see-also
- [Global API → MyStation → Services → AppManager](/guide/ui/api/mystation/services/app-manager)
:::

#### Event bus

The event bus is simply a Vue instance that uses `$emit` and `$on` methods.

The __hooks__ events, for example, are declared in the event bus.

::: see-also
- [Global API → MyStation → Services → EventBus](/guide/ui/api/mystation/services/event-bus)
:::

#### API Manager

The API Manager simplify the asynchronous HTTP requests to any server.

By default, the API Manager includes the server API and the MyStation Web official API (http://mystation.fr/api/).

> Default APIs are declared in the configuration file (See: [Configuration](/guide/ui/internal-flow/#configuration))

::: see-also
- [Global API → MyStation → Services → APIManager](/guide/ui/api/mystation/services/api-manager)
:::

### Modules

The __modules__ of the _MyStation_ interface are functions or objects having function type properties. The context `this` in these modules is the _MyStation_ root instance context(`window.__MYSTATION_VUE_INSTANCE__`).

Each __module__ is added to the root instance.

Usually, those are "shortcut" functions to __store__ actions, __services__ methods, etc...
```javascript
/** 
 * Example: module 'auth'
 */
this.auth.user() // Root instance context
this.$root.auth.user() // Anywhere (in components for example)
```

::: see-also
- [Global API → MyStation → Modules](/guide/ui/api/mystation)
:::

### Initialization

After the call of `this.boot()`, the initialization starts:

- __modules__ assignment (See [Modules](/guide/ui/internal-flow/#modules) ci-dessus)
- Initialization of each __hook__ in the event bus
- `'boot-hook'` event emitting to boot (see [Hooks](#lifecycle-hooks))

::: see-also
- [Global API → MyStation](/guide/ui/api/mystation/)
:::

### Lifecycle hooks

::: warning
Do not mix up _MyStation_ __hooks__ with default Vue.js instance hooks.
:::

The main logic of the _MyStation_ interface is splitted by methods called __hooks__, that execute instructions _synchronously_:
- `BOOT` : authentication checking
- `USER_LOGGED` : system, users and apps data loading from server
- `APPS_DATA_LOADED` : app manager synchronization (AppManager service) and apps bundles loading
- `APPS_BUNDLES_LOADED` : each app is independently registered

These methods are associated to an event in the event bus and _asynchronously_ called. (See schema below)

> Each hook has a `before()` and `after()` method that can be overriden.

<mermaid class="mermaid-sequence" filePath="/guide/ui/internal-flow/mermaid/hooks.mmd"></mermaid>

::: see-also
- [Global API → MyStation → Hook](/guide/ui/api/mystation/hook)
:::

### Authentication

The authentication works with tokens, following the oAuth2 standard. At the API server level, this is the Passport package that be used.

The user enters his credentials that are sent to the server. Then, the server returns a token that will be stored in the LocalStorage of the browser.

### Configuration

The configuration file contains the required informations that the system needs to work correctly.
This file is located at: `src/config/config.js`.

## Shared dependencies

**MyStation** exposes some dependencies into the object `window.lib`:

|Name                    |Target                        |Documentation                                                                                                |
|-                       |-                             |-                                                                                                            |
|axios                   |`window.lib.axios`            |[https://axios-http.com/docs/intro](https://axios-http.com/docs/intro)                                       |
|color                   |`window.lib.color`            |[https://www.npmjs.com/package/color](https://www.npmjs.com/package/color)                                   |
|lodash.camelcase        |`window.lib._.camelcase`      |[https://www.npmjs.com/package/lodash.camelcase](https://www.npmjs.com/package/lodash.camelcase)             |
|lodash.capitalize       |`window.lib._.capitalize`     |[https://www.npmjs.com/package/lodash.capitalize](https://www.npmjs.com/package/lodash.capitalize)           |
|lodash.kebabcase        |`window.lib._.kebabcase`      |[https://www.npmjs.com/package/lodash.kebabcase](https://www.npmjs.com/package/lodash.kebabcase)             |
|lodash.maxby            |`window.lib._.maxby`          |[https://www.npmjs.com/package/lodash.maxby](https://www.npmjs.com/package/lodash.maxby)                     |
|lodash.minby            |`window.lib._.minby`          |[https://www.npmjs.com/package/lodash.minby](https://www.npmjs.com/package/lodash.minby)                     |
|lodash.snakecase        |`window.lib._.snakecase`      |[https://www.npmjs.com/package/lodash.snakecase](https://www.npmjs.com/package/lodash.snakecase)             |
|lodash.sortby           |`window.lib._.sortby`         |[https://www.npmjs.com/package/lodash.sortby](https://www.npmjs.com/package/lodash.sortby)                   |
|lodash.uppercase        |`window.lib._.uppercase`      |[https://www.npmjs.com/package/lodash.uppercase](https://www.npmjs.com/package/lodash.uppercase)             |
|vue                     |`window.lib.Vue`              |[https://vuejs.org/v2/guide/](https://vuejs.org/v2/guide/)                                                   |

## Apps & extensions

Les Apps et extensions sont des scripts indépendants du système de MyStation.

Pour plus d'informations concernant les Apps, veuillez vous référer à la documentation liée au [Développement d'app](/fr/guide/apps/).

::: warning
Les extensions ne sont pas encore implémentées dans cette version de MyStation.
:::

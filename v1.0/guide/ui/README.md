---
editLink: false
prev: /guide/get-started/
next: /guide/ui/internal-flow/
---

# User interface

## Internal flow

[Details](/guide/ui/internal-flow/)

## Global API

The global API documentation is available [here](/guide/ui/api/)
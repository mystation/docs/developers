---
title: Apps
headline: "MyStation store: Apps module"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation store: Apps module

<a name="module_store/modules/Apps"></a>

## store/modules/Apps
MyStation store module: Vuex Store module```javascript// Importimport Apps from '@/store/modules/Apps'```


<span class="color-beige"><strong>Schema:</strong></span>

* [store/modules/Apps](#module_store/modules/Apps)
    * [Apps](#exp_module_store/modules/Apps--Apps) : <code>Object</code> ⏏
        * [.state](#module_store/modules/Apps--Apps.state) : <code>Object</code>
            * [.apps](#module_store/modules/Apps--Apps.state.apps) : <code>Array.&lt;App&gt;</code>
            * [.appsLoading](#module_store/modules/Apps--Apps.state.appsLoading) : <code>boolean</code>
            * [.marketApps](#module_store/modules/Apps--Apps.state.marketApps) : <code>Array.&lt;MarketApp&gt;</code>
            * [.marketAppsLoading](#module_store/modules/Apps--Apps.state.marketAppsLoading) : <code>boolean</code>
        * [.getters](#module_store/modules/Apps--Apps.getters) : <code>Object</code>
            * [.getApp(name)](#module_store/modules/Apps--Apps.getters.getApp) ⇒ <code>App</code> \| <code>undefined</code>
            * [.getMarketApp(name)](#module_store/modules/Apps--Apps.getters.getMarketApp) ⇒ <code>App</code> \| <code>undefined</code>
        * [.actions](#module_store/modules/Apps--Apps.actions) : <code>Object</code>
            * [.loadApps()](#module_store/modules/Apps--Apps.actions.loadApps) ⇒ <code>boolean</code> \| <code>Error</code>
            * [.loadMarketApps()](#module_store/modules/Apps--Apps.actions.loadMarketApps) ⇒ <code>Error</code> \| <code>Array.&lt;Object&gt;</code>
        * [.mutations](#module_store/modules/Apps--Apps.mutations) : <code>Object</code>
            * [.SET_APPS(apps)](#module_store/modules/Apps--Apps.mutations.SET_APPS) ⇒ <code>void</code>
            * [.SET_APPS_LOADING(value)](#module_store/modules/Apps--Apps.mutations.SET_APPS_LOADING) ⇒ <code>void</code>
            * [.SET_MARKET_APPS(apps)](#module_store/modules/Apps--Apps.mutations.SET_MARKET_APPS) ⇒ <code>void</code>
            * [.SET_MARKET_APPS_LOADING(value)](#module_store/modules/Apps--Apps.mutations.SET_MARKET_APPS_LOADING) ⇒ <code>void</code>

<a name="exp_module_store/modules/Apps--Apps"></a>

### Apps : <code>Object</code> ⏏
Apps

<span class="color-beige"><strong>Kind:</strong></span> Exported constant  
<a name="module_store/modules/Apps--Apps.state"></a>

#### Apps.state : <code>Object</code>
Store module state

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>Apps</code>](#exp_module_store/modules/Apps--Apps)  
<span class="color-beige"><strong>Read only:</strong></span> true  
<span class="color-beige"><strong>Example:</strong></span>  
```js
// Access to the module stateStore.state.Apps
```

<span class="color-beige"><strong>Schema:</strong></span>

* [.state](#module_store/modules/Apps--Apps.state) : <code>Object</code>
    * [.apps](#module_store/modules/Apps--Apps.state.apps) : <code>Array.&lt;App&gt;</code>
    * [.appsLoading](#module_store/modules/Apps--Apps.state.appsLoading) : <code>boolean</code>
    * [.marketApps](#module_store/modules/Apps--Apps.state.marketApps) : <code>Array.&lt;MarketApp&gt;</code>
    * [.marketAppsLoading](#module_store/modules/Apps--Apps.state.marketAppsLoading) : <code>boolean</code>

<a name="module_store/modules/Apps--Apps.state.apps"></a>

##### state.apps : <code>Array.&lt;App&gt;</code>
Apps data received from server.Typically, the local installed apps

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>state</code>](#module_store/modules/Apps--Apps.state)  
<span class="color-beige"><strong>Default:</strong></span> <code>[]</code>  
<span class="color-beige"><strong>Example:</strong></span>  
```js
// Access to propertyStore.state.Apps.apps
```
<a name="module_store/modules/Apps--Apps.state.appsLoading"></a>

##### state.appsLoading : <code>boolean</code>
Indicates if apps are loading from API

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>state</code>](#module_store/modules/Apps--Apps.state)  
<span class="color-beige"><strong>Default:</strong></span> <code>false</code>  
<a name="module_store/modules/Apps--Apps.state.marketApps"></a>

##### state.marketApps : <code>Array.&lt;MarketApp&gt;</code>
Market Apps data received from MyStation official API server.

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>state</code>](#module_store/modules/Apps--Apps.state)  
<span class="color-beige"><strong>Default:</strong></span> <code>[]</code>  
<a name="module_store/modules/Apps--Apps.state.marketAppsLoading"></a>

##### state.marketAppsLoading : <code>boolean</code>
Indicates if market apps are loading from API

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>state</code>](#module_store/modules/Apps--Apps.state)  
<span class="color-beige"><strong>Default:</strong></span> <code>false</code>  
<a name="module_store/modules/Apps--Apps.getters"></a>

#### Apps.getters : <code>Object</code>
Store module getters

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>Apps</code>](#exp_module_store/modules/Apps--Apps)  
<span class="color-beige"><strong>Read only:</strong></span> true  
<span class="color-beige"><strong>Example:</strong></span>  
```js
// Access to the module getters, where <name> is the getter nameStore.getters['Apps/<name>']
```

<span class="color-beige"><strong>Schema:</strong></span>

* [.getters](#module_store/modules/Apps--Apps.getters) : <code>Object</code>
    * [.getApp(name)](#module_store/modules/Apps--Apps.getters.getApp) ⇒ <code>App</code> \| <code>undefined</code>
    * [.getMarketApp(name)](#module_store/modules/Apps--Apps.getters.getMarketApp) ⇒ <code>App</code> \| <code>undefined</code>

<a name="module_store/modules/Apps--Apps.getters.getApp"></a>

##### getters.getApp(name) ⇒ <code>App</code> \| <code>undefined</code>
Retrieve an app by unique name

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>getters</code>](#module_store/modules/Apps--Apps.getters)  
<span class="color-beige"><strong>Returns:</strong></span> <code>App</code> \| <code>undefined</code>  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | App unique name |

<a name="module_store/modules/Apps--Apps.getters.getMarketApp"></a>

##### getters.getMarketApp(name) ⇒ <code>App</code> \| <code>undefined</code>
Retrieve a market app by unique name

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>getters</code>](#module_store/modules/Apps--Apps.getters)  
<span class="color-beige"><strong>Returns:</strong></span> <code>App</code> \| <code>undefined</code>  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | App unique name |

<a name="module_store/modules/Apps--Apps.actions"></a>

#### Apps.actions : <code>Object</code>
Store module actions

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>Apps</code>](#exp_module_store/modules/Apps--Apps)  
<span class="color-beige"><strong>Access:</strong></span> protected  
<span class="color-beige"><strong>Example:</strong></span>  
```js
// Access to the module action, where <name> is the action nameStore.dispatch('Apps/<name>')
```

<span class="color-beige"><strong>Schema:</strong></span>

* [.actions](#module_store/modules/Apps--Apps.actions) : <code>Object</code>
    * [.loadApps()](#module_store/modules/Apps--Apps.actions.loadApps) ⇒ <code>boolean</code> \| <code>Error</code>
    * [.loadMarketApps()](#module_store/modules/Apps--Apps.actions.loadMarketApps) ⇒ <code>Error</code> \| <code>Array.&lt;Object&gt;</code>

<a name="module_store/modules/Apps--Apps.actions.loadApps"></a>

##### actions.loadApps() ⇒ <code>boolean</code> \| <code>Error</code>
Load apps from server.Request `[GET] /api/apps` (See [API Server documentation](/guide/system/api-server/)).

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>actions</code>](#module_store/modules/Apps--Apps.actions)  
<span class="color-beige"><strong>Returns:</strong></span> <code>boolean</code> \| <code>Error</code> - If success: true, else Error  
<a name="module_store/modules/Apps--Apps.actions.loadMarketApps"></a>

##### actions.loadMarketApps() ⇒ <code>Error</code> \| <code>Array.&lt;Object&gt;</code>
Load market apps from MyStation Web API.Request `[GET] http://mystation.fr/api/apps`.

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>actions</code>](#module_store/modules/Apps--Apps.actions)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Error</code> \| <code>Array.&lt;Object&gt;</code> - Return collection of app or Error  
<a name="module_store/modules/Apps--Apps.mutations"></a>

#### Apps.mutations : <code>Object</code>
Store module mutations

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>Apps</code>](#exp_module_store/modules/Apps--Apps)  
<span class="color-beige"><strong>Access:</strong></span> protected  
<span class="color-beige"><strong>Example:</strong></span>  
```js
// Dispatch a module mutation, where <mutation_name> is the mutation nameStore.commit('Apps/<mutation_name>', [payload])
```

<span class="color-beige"><strong>Schema:</strong></span>

* [.mutations](#module_store/modules/Apps--Apps.mutations) : <code>Object</code>
    * [.SET_APPS(apps)](#module_store/modules/Apps--Apps.mutations.SET_APPS) ⇒ <code>void</code>
    * [.SET_APPS_LOADING(value)](#module_store/modules/Apps--Apps.mutations.SET_APPS_LOADING) ⇒ <code>void</code>
    * [.SET_MARKET_APPS(apps)](#module_store/modules/Apps--Apps.mutations.SET_MARKET_APPS) ⇒ <code>void</code>
    * [.SET_MARKET_APPS_LOADING(value)](#module_store/modules/Apps--Apps.mutations.SET_MARKET_APPS_LOADING) ⇒ <code>void</code>

<a name="module_store/modules/Apps--Apps.mutations.SET_APPS"></a>

##### mutations.SET\_APPS(apps) ⇒ <code>void</code>
Mutate state.apps

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>mutations</code>](#module_store/modules/Apps--Apps.mutations)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  

| Param | Type | Description |
| --- | --- | --- |
| apps | <code>Array.&lt;Object&gt;</code> | Data received from server |

<a name="module_store/modules/Apps--Apps.mutations.SET_APPS_LOADING"></a>

##### mutations.SET\_APPS\_LOADING(value) ⇒ <code>void</code>
Mutate state.appsLoading

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>mutations</code>](#module_store/modules/Apps--Apps.mutations)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>boolean</code> | True or false |

<a name="module_store/modules/Apps--Apps.mutations.SET_MARKET_APPS"></a>

##### mutations.SET\_MARKET\_APPS(apps) ⇒ <code>void</code>
Mutate state.marketApps

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>mutations</code>](#module_store/modules/Apps--Apps.mutations)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  

| Param | Type | Description |
| --- | --- | --- |
| apps | <code>Array.&lt;Object&gt;</code> | Data received from server |

<a name="module_store/modules/Apps--Apps.mutations.SET_MARKET_APPS_LOADING"></a>

##### mutations.SET\_MARKET\_APPS\_LOADING(value) ⇒ <code>void</code>
Mutate state.marketAppsLoading

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>mutations</code>](#module_store/modules/Apps--Apps.mutations)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>boolean</code> | True or false |


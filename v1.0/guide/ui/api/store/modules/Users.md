---
title: Users
headline: "MyStation store: Users module"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation store: Users module

<a name="module_store/modules/Users"></a>

## store/modules/Users
MyStation store module: Vuex Store module```javascript// Importimport Users from '@/store/modules/Users'```


<span class="color-beige"><strong>Schema:</strong></span>

* [store/modules/Users](#module_store/modules/Users)
    * [Users](#exp_module_store/modules/Users--Users) : <code>Object</code> ⏏
        * [.state](#module_store/modules/Users--Users.state) : <code>Object</code>
            * [.users](#module_store/modules/Users--Users.state.users) : <code>Array.&lt;User&gt;</code>
            * [.usersLoading](#module_store/modules/Users--Users.state.usersLoading) : <code>boolean</code>
            * [.currentUser](#module_store/modules/Users--Users.state.currentUser) : <code>User</code>
            * [.currentUserLoading](#module_store/modules/Users--Users.state.currentUserLoading) : <code>boolean</code>
        * [.getters](#module_store/modules/Users--Users.getters) : <code>Object</code>
            * [.getUser(id)](#module_store/modules/Users--Users.getters.getUser) ⇒ <code>User</code> \| <code>undefined</code>
        * [.actions](#module_store/modules/Users--Users.actions) : <code>Object</code>
            * [.login(credentials)](#module_store/modules/Users--Users.actions.login) ⇒ <code>boolean</code> \| <code>Error</code>
            * [.logout()](#module_store/modules/Users--Users.actions.logout) ⇒ <code>void</code>
            * [.checkAuthenticated()](#module_store/modules/Users--Users.actions.checkAuthenticated) ⇒ <code>boolean</code> \| <code>Error</code>
            * [.loadCurrentUserData()](#module_store/modules/Users--Users.actions.loadCurrentUserData) ⇒ <code>boolean</code> \| <code>Error</code>
            * [.loadUsersData()](#module_store/modules/Users--Users.actions.loadUsersData) ⇒ <code>boolean</code> \| <code>Error</code>
            * [.setAuthorizationHeader(token)](#module_store/modules/Users--Users.actions.setAuthorizationHeader) ⇒ <code>void</code>
            * [.resetAuthorizationHeader()](#module_store/modules/Users--Users.actions.resetAuthorizationHeader) ⇒ <code>void</code>
            * [.initializeApp(appName)](#module_store/modules/Users--Users.actions.initializeApp) ⇒ <code>boolean</code> \| <code>Error</code>
        * [.mutations](#module_store/modules/Users--Users.mutations) : <code>Object</code>
            * [.SET_USERS(users)](#module_store/modules/Users--Users.mutations.SET_USERS) ⇒ <code>void</code>
            * [.SET_USERS_LOADING(value)](#module_store/modules/Users--Users.mutations.SET_USERS_LOADING) ⇒ <code>void</code>
            * [.SET_CURRENT_USER(user)](#module_store/modules/Users--Users.mutations.SET_CURRENT_USER) ⇒ <code>void</code>
            * [.RESET_CURRENT_USER()](#module_store/modules/Users--Users.mutations.RESET_CURRENT_USER) ⇒ <code>void</code>
            * [.SET_CURRENT_USER_LOADING(value)](#module_store/modules/Users--Users.mutations.SET_CURRENT_USER_LOADING) ⇒ <code>void</code>

<a name="exp_module_store/modules/Users--Users"></a>

### Users : <code>Object</code> ⏏
Users

<span class="color-beige"><strong>Kind:</strong></span> Exported constant  
<a name="module_store/modules/Users--Users.state"></a>

#### Users.state : <code>Object</code>
Store module state

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>Users</code>](#exp_module_store/modules/Users--Users)  
<span class="color-beige"><strong>Read only:</strong></span> true  
<span class="color-beige"><strong>Example:</strong></span>  
```js
// Access to the module stateStore.state.Users
```

<span class="color-beige"><strong>Schema:</strong></span>

* [.state](#module_store/modules/Users--Users.state) : <code>Object</code>
    * [.users](#module_store/modules/Users--Users.state.users) : <code>Array.&lt;User&gt;</code>
    * [.usersLoading](#module_store/modules/Users--Users.state.usersLoading) : <code>boolean</code>
    * [.currentUser](#module_store/modules/Users--Users.state.currentUser) : <code>User</code>
    * [.currentUserLoading](#module_store/modules/Users--Users.state.currentUserLoading) : <code>boolean</code>

<a name="module_store/modules/Users--Users.state.users"></a>

##### state.users : <code>Array.&lt;User&gt;</code>
Users data received from server.Typically, the local users

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>state</code>](#module_store/modules/Users--Users.state)  
<span class="color-beige"><strong>Default:</strong></span> <code>[]</code>  
<span class="color-beige"><strong>Example:</strong></span>  
```js
// Access to propertyStore.state.Users.users
```
<a name="module_store/modules/Users--Users.state.usersLoading"></a>

##### state.usersLoading : <code>boolean</code>
Indicates if users are loading from API

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>state</code>](#module_store/modules/Users--Users.state)  
<span class="color-beige"><strong>Default:</strong></span> <code>false</code>  
<a name="module_store/modules/Users--Users.state.currentUser"></a>

##### state.currentUser : <code>User</code>
Current / Authenticated User

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>state</code>](#module_store/modules/Users--Users.state)  
<span class="color-beige"><strong>Default:</strong></span> <code>new User()</code>  
<a name="module_store/modules/Users--Users.state.currentUserLoading"></a>

##### state.currentUserLoading : <code>boolean</code>
Indicates if current user is loading from API

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>state</code>](#module_store/modules/Users--Users.state)  
<span class="color-beige"><strong>Default:</strong></span> <code>false</code>  
<a name="module_store/modules/Users--Users.getters"></a>

#### Users.getters : <code>Object</code>
Store module getters

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>Users</code>](#exp_module_store/modules/Users--Users)  
<span class="color-beige"><strong>Read only:</strong></span> true  
<span class="color-beige"><strong>Example:</strong></span>  
```js
// Access to the module getters, where <name> is the getter nameStore.getters['Users/<name>']
```
<a name="module_store/modules/Users--Users.getters.getUser"></a>

##### getters.getUser(id) ⇒ <code>User</code> \| <code>undefined</code>
Retrieve a user by id

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>getters</code>](#module_store/modules/Users--Users.getters)  
<span class="color-beige"><strong>Returns:</strong></span> <code>User</code> \| <code>undefined</code>  

| Param | Type | Description |
| --- | --- | --- |
| id | <code>number</code> | User id |

<a name="module_store/modules/Users--Users.actions"></a>

#### Users.actions : <code>Object</code>
Store module actions

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>Users</code>](#exp_module_store/modules/Users--Users)  
<span class="color-beige"><strong>Access:</strong></span> protected  
<span class="color-beige"><strong>Example:</strong></span>  
```js
// Access to the module action, where <name> is the action nameStore.dispatch('Users/<name>')
```

<span class="color-beige"><strong>Schema:</strong></span>

* [.actions](#module_store/modules/Users--Users.actions) : <code>Object</code>
    * [.login(credentials)](#module_store/modules/Users--Users.actions.login) ⇒ <code>boolean</code> \| <code>Error</code>
    * [.logout()](#module_store/modules/Users--Users.actions.logout) ⇒ <code>void</code>
    * [.checkAuthenticated()](#module_store/modules/Users--Users.actions.checkAuthenticated) ⇒ <code>boolean</code> \| <code>Error</code>
    * [.loadCurrentUserData()](#module_store/modules/Users--Users.actions.loadCurrentUserData) ⇒ <code>boolean</code> \| <code>Error</code>
    * [.loadUsersData()](#module_store/modules/Users--Users.actions.loadUsersData) ⇒ <code>boolean</code> \| <code>Error</code>
    * [.setAuthorizationHeader(token)](#module_store/modules/Users--Users.actions.setAuthorizationHeader) ⇒ <code>void</code>
    * [.resetAuthorizationHeader()](#module_store/modules/Users--Users.actions.resetAuthorizationHeader) ⇒ <code>void</code>
    * [.initializeApp(appName)](#module_store/modules/Users--Users.actions.initializeApp) ⇒ <code>boolean</code> \| <code>Error</code>

<a name="module_store/modules/Users--Users.actions.login"></a>

##### actions.login(credentials) ⇒ <code>boolean</code> \| <code>Error</code>
Request `[POST] /api/oauth/token` with credentials data (See [API Server documentation](/guide/system/api-server/)).If server side success, save tokens in LocalStorage and dispatch 'checkAuthenticated'

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>actions</code>](#module_store/modules/Users--Users.actions)  
<span class="color-beige"><strong>Returns:</strong></span> <code>boolean</code> \| <code>Error</code> - Return value of 'checkAuthenticated'  

| Param | Type | Description |
| --- | --- | --- |
| credentials | <code>Object</code> | The user credentials |
| credentials.username | <code>string</code> | Username |
| credentials.password | <code>string</code> | Password |

<a name="module_store/modules/Users--Users.actions.logout"></a>

##### actions.logout() ⇒ <code>void</code>
Remove tokens from the local storage and reset current user

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>actions</code>](#module_store/modules/Users--Users.actions)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<a name="module_store/modules/Users--Users.actions.checkAuthenticated"></a>

##### actions.checkAuthenticated() ⇒ <code>boolean</code> \| <code>Error</code>
Check if user is authenticated.First, check if token is set in the local storage,set authorization header and request for user data from server.Else, call 'logout' and return false.

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>actions</code>](#module_store/modules/Users--Users.actions)  
<span class="color-beige"><strong>Returns:</strong></span> <code>boolean</code> \| <code>Error</code> - False or return value of 'loadCurrentUserData'  
<a name="module_store/modules/Users--Users.actions.loadCurrentUserData"></a>

##### actions.loadCurrentUserData() ⇒ <code>boolean</code> \| <code>Error</code>
Load current user data from API server.Request `[GET] /api/me` (See [API Server documentation](/guide/system/api-server/)).

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>actions</code>](#module_store/modules/Users--Users.actions)  
<span class="color-beige"><strong>Returns:</strong></span> <code>boolean</code> \| <code>Error</code> - If success: true, else Error  
<a name="module_store/modules/Users--Users.actions.loadUsersData"></a>

##### actions.loadUsersData() ⇒ <code>boolean</code> \| <code>Error</code>
Load users data from API server.Request `[GET] /api/users` (See [API Server documentation](/guide/system/api-server/)).

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>actions</code>](#module_store/modules/Users--Users.actions)  
<span class="color-beige"><strong>Returns:</strong></span> <code>boolean</code> \| <code>Error</code> - If success: true, else Error  
<a name="module_store/modules/Users--Users.actions.setAuthorizationHeader"></a>

##### actions.setAuthorizationHeader(token) ⇒ <code>void</code>
Set the authorization token header for the API instance (ServerAPI).

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>actions</code>](#module_store/modules/Users--Users.actions)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  

| Param | Type | Description |
| --- | --- | --- |
| token | <code>string</code> | The access token |

<a name="module_store/modules/Users--Users.actions.resetAuthorizationHeader"></a>

##### actions.resetAuthorizationHeader() ⇒ <code>void</code>
Delete the authorization token header for the API instance (ServerAPI).

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>actions</code>](#module_store/modules/Users--Users.actions)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<a name="module_store/modules/Users--Users.actions.initializeApp"></a>

##### actions.initializeApp(appName) ⇒ <code>boolean</code> \| <code>Error</code>
App initialization method.Request `[POST] /api/apps/<appName>/init` (See [API Server documentation](/guide/system/api-server/))to initialize an app for the current authenticated user.Then, load current user data from server: dispatch 'loadCurrentUserData'.

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>actions</code>](#module_store/modules/Users--Users.actions)  
<span class="color-beige"><strong>Returns:</strong></span> <code>boolean</code> \| <code>Error</code> - Error or return value of 'loadCurrentUserData'  

| Param | Type | Description |
| --- | --- | --- |
| appName | <code>string</code> | The unique app name |

<a name="module_store/modules/Users--Users.mutations"></a>

#### Users.mutations : <code>Object</code>
Store module mutations

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>Users</code>](#exp_module_store/modules/Users--Users)  
<span class="color-beige"><strong>Access:</strong></span> protected  
<span class="color-beige"><strong>Example:</strong></span>  
```js
// Dispatch a module mutation, where <mutation_name> is the mutation nameStore.commit('Users/<mutation_name>', [payload])
```

<span class="color-beige"><strong>Schema:</strong></span>

* [.mutations](#module_store/modules/Users--Users.mutations) : <code>Object</code>
    * [.SET_USERS(users)](#module_store/modules/Users--Users.mutations.SET_USERS) ⇒ <code>void</code>
    * [.SET_USERS_LOADING(value)](#module_store/modules/Users--Users.mutations.SET_USERS_LOADING) ⇒ <code>void</code>
    * [.SET_CURRENT_USER(user)](#module_store/modules/Users--Users.mutations.SET_CURRENT_USER) ⇒ <code>void</code>
    * [.RESET_CURRENT_USER()](#module_store/modules/Users--Users.mutations.RESET_CURRENT_USER) ⇒ <code>void</code>
    * [.SET_CURRENT_USER_LOADING(value)](#module_store/modules/Users--Users.mutations.SET_CURRENT_USER_LOADING) ⇒ <code>void</code>

<a name="module_store/modules/Users--Users.mutations.SET_USERS"></a>

##### mutations.SET\_USERS(users) ⇒ <code>void</code>
Mutate state.users

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>mutations</code>](#module_store/modules/Users--Users.mutations)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  

| Param | Type | Description |
| --- | --- | --- |
| users | <code>Array.&lt;Object&gt;</code> | Data received from server |

<a name="module_store/modules/Users--Users.mutations.SET_USERS_LOADING"></a>

##### mutations.SET\_USERS\_LOADING(value) ⇒ <code>void</code>
Mutate state.usersLoading

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>mutations</code>](#module_store/modules/Users--Users.mutations)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>boolean</code> | True or false |

<a name="module_store/modules/Users--Users.mutations.SET_CURRENT_USER"></a>

##### mutations.SET\_CURRENT\_USER(user) ⇒ <code>void</code>
Mutate state.currentUser

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>mutations</code>](#module_store/modules/Users--Users.mutations)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  

| Param | Type | Description |
| --- | --- | --- |
| user | <code>Object</code> | Data received from server |

<a name="module_store/modules/Users--Users.mutations.RESET_CURRENT_USER"></a>

##### mutations.RESET\_CURRENT\_USER() ⇒ <code>void</code>
Mutate state.currentUser

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>mutations</code>](#module_store/modules/Users--Users.mutations)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<a name="module_store/modules/Users--Users.mutations.SET_CURRENT_USER_LOADING"></a>

##### mutations.SET\_CURRENT\_USER\_LOADING(value) ⇒ <code>void</code>
Mutate state.currentUserLoading

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>mutations</code>](#module_store/modules/Users--Users.mutations)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>boolean</code> | True or false |


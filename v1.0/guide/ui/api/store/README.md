---
title: Store
headline: "MyStation store"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation store

<a name="module_store"></a>

## store
MyStation store: Vuex Store (see: [https://vuex.vuejs.org/guide/](https://vuex.vuejs.org/guide/))```javascript// Importimport Store from '@/store'```


<span class="color-beige"><strong>Schema:</strong></span>

* [store](#module_store)
    * [Store](#exp_module_store--Store) : <code>Vuex.Store</code> ⏏
        * [.modules](#module_store--Store.modules) : <code>Object</code>
        * [.state](#module_store--Store.state) : <code>Object</code>
            * [.system](#module_store--Store.state.system) : <code>Object</code>
                * [.currentApp](#module_store--Store.state.system.currentApp) : <code>App</code>
                * [.mainframeContent](#module_store--Store.state.system.mainframeContent) : <code>string</code>
        * [.getters](#module_store--Store.getters) : <code>Object</code>
        * [.actions](#module_store--Store.actions) : <code>Object</code>
            * [.switchMainframe(component)](#module_store--Store.actions.switchMainframe) ⇒ <code>void</code>
        * [.mutations](#module_store--Store.mutations) : <code>Object</code>
            * [.SET_CURRENT_APP(app)](#module_store--Store.mutations.SET_CURRENT_APP) ⇒ <code>void</code>
            * [.SET_MAINFRAME_COMPONENT(component)](#module_store--Store.mutations.SET_MAINFRAME_COMPONENT) ⇒ <code>void</code>

<a name="exp_module_store--Store"></a>

### Store : <code>Vuex.Store</code> ⏏
Store

<span class="color-beige"><strong>Kind:</strong></span> Exported constant  

::: syntax Store

Access to store instance
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.$store
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  this.$store
  __MYSTATION__.$store
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.$store
  ```
:::

<a name="module_store--Store.modules"></a>

#### Store.modules : <code>Object</code>
Vuex Store option: store modules

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>Store</code>](#exp_module_store--Store)  
<span class="color-beige"><strong>Access:</strong></span> protected  
<a name="module_store--Store.state"></a>

#### Store.state : <code>Object</code>
Vuex Store option. See syntax for accessor.

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>Store</code>](#exp_module_store--Store)  
<span class="color-beige"><strong>Read only:</strong></span> true  

::: syntax Store.state

Access to property
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.$store.state
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  this.$store.state
  __MYSTATION__.$store.state
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.$store.state
  ```
:::

<span class="color-beige"><strong>Example:</strong></span>  
```js
// Access to the root stateStore.state// Access to a module state, where <module_name> is the module nameStore.state.<module_name>
```

<span class="color-beige"><strong>Schema:</strong></span>

* [.state](#module_store--Store.state) : <code>Object</code>
    * [.system](#module_store--Store.state.system) : <code>Object</code>
        * [.currentApp](#module_store--Store.state.system.currentApp) : <code>App</code>
        * [.mainframeContent](#module_store--Store.state.system.mainframeContent) : <code>string</code>

<a name="module_store--Store.state.system"></a>

##### state.system : <code>Object</code>
MyStation system data

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>state</code>](#module_store--Store.state)  

<span class="color-beige"><strong>Schema:</strong></span>

* [.system](#module_store--Store.state.system) : <code>Object</code>
    * [.currentApp](#module_store--Store.state.system.currentApp) : <code>App</code>
    * [.mainframeContent](#module_store--Store.state.system.mainframeContent) : <code>string</code>

<a name="module_store--Store.state.system.currentApp"></a>

###### system.currentApp : <code>App</code>
The current app displayed in mainframe

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>system</code>](#module_store--Store.state.system)  
<span class="color-beige"><strong>Default:</strong></span> <code>null</code>  
<a name="module_store--Store.state.system.mainframeContent"></a>

###### system.mainframeContent : <code>string</code>
The name of the component displayed in mainframe

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>system</code>](#module_store--Store.state.system)  
<span class="color-beige"><strong>Default:</strong></span> <code>&quot;null&quot;</code>  
<a name="module_store--Store.getters"></a>

#### Store.getters : <code>Object</code>
Vuex Store option. See syntax for accessor.

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>Store</code>](#exp_module_store--Store)  
<span class="color-beige"><strong>Read only:</strong></span> true  

::: syntax Store.getters

Access to property, where `<name>` is the getter name
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.$store.getters[<name>]
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  this.$store.getters[<name>]
  __MYSTATION__.$store.getters[<name>]
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.$store.getters[<name>]
  ```
:::

<span class="color-beige"><strong>Example:</strong></span>  
```js
// Access to the root getterStore.getters[<name>]// Access to a module getter, where <getter_name> is the getter name// and <module_name> is the module nameStore.getters['<module_name>/<getter_name>']
```
<a name="module_store--Store.actions"></a>

#### Store.actions : <code>Object</code>
Vuex Store option. See syntax for accessor.

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>Store</code>](#exp_module_store--Store)  
<span class="color-beige"><strong>Access:</strong></span> protected  

::: syntax Store.dispatch(&lt;name&gt;, [&lt;payload&gt;])

Access to property, where `<name>` is the action name
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.$store.dispatch(<name>, [<payload>])
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  this.$store.dispatch(<name>, [<payload>])
  __MYSTATION__.$store.dispatch(<name>, [<payload>])
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.$store.dispatch(<name>, [<payload>])
  ```
:::

<span class="color-beige"><strong>Example:</strong></span>  
```js
// Dispatch a root action, where <name> is the action nameStore.dispatch(<name>, [<payload>])// Dispatch a module action, where <module_name> is the module name// and <action_name> is the action nameStore.dispatch('<module_name>/<action_name>', [<payload>]) // equivalent
```
<a name="module_store--Store.actions.switchMainframe"></a>

##### actions.switchMainframe(component) ⇒ <code>void</code>
Switch mainframe component.If switch to an app, set currentApp with App object from state.apps, else set to null.In both cases, set mainframeComponent to `<component>` value.

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>actions</code>](#module_store--Store.actions)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  

| Param | Type | Description |
| --- | --- | --- |
| component | <code>string</code> | The name of the component to include in the main frame. Must be 'Dashboard', 'Settings', 'AppMarket' or an app name. |

<span class="color-beige"><strong>Example:</strong></span>  
```js
// Display dashboard in UIStore.dispatch('switchMainframe', 'Dashboard')
```
<a name="module_store--Store.mutations"></a>

#### Store.mutations : <code>Object</code>
Vuex Store option. See syntax for accessor.

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>Store</code>](#exp_module_store--Store)  
<span class="color-beige"><strong>Access:</strong></span> protected  

::: syntax Store.commit(&lt;name&gt;, [&lt;payload&gt;])

Access to property, where `<name>` is the mutation name
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.$store.commit(<name>, [payload])
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  this.$store.commit(<name>, [payload])
  __MYSTATION__.$store.commit(<name>, [payload])
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.$store.commit(<name>, [payload])
  ```
:::

<span class="color-beige"><strong>Example:</strong></span>  
```js
// Commit a root mutation, where <name> is the mutation nameStore.commit(<name>, [payload])// Dispatch a module mutation, where <module_name> is the module name// and <mutation_name> is the mutation nameStore.commit('<module_name>/<mutation_name>', [payload])
```

<span class="color-beige"><strong>Schema:</strong></span>

* [.mutations](#module_store--Store.mutations) : <code>Object</code>
    * [.SET_CURRENT_APP(app)](#module_store--Store.mutations.SET_CURRENT_APP) ⇒ <code>void</code>
    * [.SET_MAINFRAME_COMPONENT(component)](#module_store--Store.mutations.SET_MAINFRAME_COMPONENT) ⇒ <code>void</code>

<a name="module_store--Store.mutations.SET_CURRENT_APP"></a>

##### mutations.SET\_CURRENT\_APP(app) ⇒ <code>void</code>
Mutate Store.state.system.currentApp

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>mutations</code>](#module_store--Store.mutations)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  

| Param | Type | Description |
| --- | --- | --- |
| app | <code>App</code> | The current app to set |

<a name="module_store--Store.mutations.SET_MAINFRAME_COMPONENT"></a>

##### mutations.SET\_MAINFRAME\_COMPONENT(component) ⇒ <code>void</code>
Mutate Store.state.system.mainframeContent

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>mutations</code>](#module_store--Store.mutations)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  

| Param | Type | Description |
| --- | --- | --- |
| component | <code>string</code> | Component name |


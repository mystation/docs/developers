---
title: Hook class
headline: "MyStation Hook class"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation Hook class

<a name="Hook"></a>

## Hook
MyStation Hook class.```javascript// Importimport Hook from '#mystation/Hook'```

<span class="color-beige"><strong>Kind:</strong></span> global class  

<span class="color-beige"><strong>Schema:</strong></span>

* [Hook](#Hook)
    * [new Hook(options)](#new_Hook_new)
    * [.name](#Hook+name) : <code>string</code>
    * [.run](#Hook+run) : <code>function</code>
    * [.before](#Hook+before) : <code>Object</code>
    * [.after](#Hook+after) : <code>Object</code>
    * [.triggerName](#Hook+triggerName) : <code>string</code>

Constructor
<a name="new_Hook_new"></a>

### new Hook(options)
Create a new Hook


| Param | Type | Description |
| --- | --- | --- |
| options | <code>Object</code> |  |
| options.name | <code>string</code> | Name |
| options.run | <code>function</code> | Main async 'run' Hook method |
| [options.before] | <code>Array.&lt;function()&gt;</code> | Array of 'before' callback functions |

<a name="Hook+name"></a>

### hook.name : <code>string</code>
Hook name accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>Hook</code>](#Hook)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="Hook+run"></a>

### hook.run : <code>function</code>
Hook run accessorExecute [before()] -> this._run() -> [after()]

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>Hook</code>](#Hook)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="Hook+before"></a>

### hook.before : <code>Object</code>
Hook before accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>Hook</code>](#Hook)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="Hook+after"></a>

### hook.after : <code>Object</code>
Hook after accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>Hook</code>](#Hook)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="Hook+triggerName"></a>

### hook.triggerName : <code>string</code>
Hook triggerName accessor (EventBus event name)

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>Hook</code>](#Hook)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  

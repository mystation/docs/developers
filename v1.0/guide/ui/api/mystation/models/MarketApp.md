---
title: MarketApp class
headline: "MyStation MarketApp class"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation MarketApp class

<a name="MarketApp"></a>

## MarketApp
MyStation MarketApp model: MarketApp class.```javascript// Importimport MarketApp from '#mystation/models/MarketApp'```

<span class="color-beige"><strong>Kind:</strong></span> global class  

<span class="color-beige"><strong>Schema:</strong></span>

* [MarketApp](#MarketApp)
    * [new MarketApp(data)](#new_MarketApp_new)
    * _instance_
        * _methods_
            * [.lastVersion()](#MarketApp+lastVersion) ⇒ <code>string</code>
            * [.firstVersion()](#MarketApp+firstVersion) ⇒ <code>string</code>
        * _properties_
            * [.name](#MarketApp+name) : <code>string</code>
            * [.title](#MarketApp+title) : <code>string</code>
            * [.icon](#MarketApp+icon) : <code>Buffer</code>
            * [.color](#MarketApp+color) : <code>string</code>
            * [.versions](#MarketApp+versions) : <code>Array.&lt;Object&gt;</code>
    * _static_
        * [.defaults()](#MarketApp.defaults) ⇒ <code>Object</code>

Constructor
<a name="new_MarketApp_new"></a>

### new MarketApp(data)
Create an App


| Param | Type | Description |
| --- | --- | --- |
| data | <code>Object</code> | Object that represents the app |
| data.name | <code>string</code> | The unique app name |
| data.title | <code>string</code> | The displayed name |
| data.icon | <code>Buffer</code> | App icon (base 64) |
| data.color | <code>string</code> | The color assigned to the app, in hexadecimal, with the '#' (ex: '#FAB3E7') |
| data.versions | <code>Array.&lt;Object&gt;</code> | The app versions |

<a name="MarketApp+lastVersion"></a>

### marketApp.lastVersion() ⇒ <code>string</code>
Return the last app version

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>MarketApp</code>](#MarketApp)  
<span class="color-beige"><strong>Returns:</strong></span> <code>string</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  
<a name="MarketApp+firstVersion"></a>

### marketApp.firstVersion() ⇒ <code>string</code>
Return the first app version

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>MarketApp</code>](#MarketApp)  
<span class="color-beige"><strong>Returns:</strong></span> <code>string</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  
<a name="MarketApp+name"></a>

### marketApp.name : <code>string</code>
App name accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>MarketApp</code>](#MarketApp)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="MarketApp+title"></a>

### marketApp.title : <code>string</code>
App title accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>MarketApp</code>](#MarketApp)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="MarketApp+icon"></a>

### marketApp.icon : <code>Buffer</code>
App icon accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>MarketApp</code>](#MarketApp)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="MarketApp+color"></a>

### marketApp.color : <code>string</code>
App color accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>MarketApp</code>](#MarketApp)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="MarketApp+versions"></a>

### marketApp.versions : <code>Array.&lt;Object&gt;</code>
App versions accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>MarketApp</code>](#MarketApp)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="MarketApp.defaults"></a>

### MarketApp.defaults() ⇒ <code>Object</code>
Default properties for a new empty 'MarketApp' object

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>MarketApp</code>](#MarketApp)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Object</code> - Default properties object  

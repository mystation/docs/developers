---
title: App class
headline: "MyStation App class"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation App class

<a name="App"></a>

## App
MyStation App model: App class.```javascript// Importimport App from '#mystation/models/App'```

<span class="color-beige"><strong>Kind:</strong></span> global class  

<span class="color-beige"><strong>Schema:</strong></span>

* [App](#App)
    * [new App(data)](#new_App_new)
    * _instance_
        * _properties_
            * [.name](#App+name) : <code>string</code>
            * [.version](#App+version) : <code>string</code>
            * [.title](#App+title) : <code>string</code>
            * [.icon](#App+icon) : <code>Buffer</code>
            * [.color](#App+color) : <code>string</code>
            * [.appConfig](#App+appConfig) : <code>Object</code>
            * [.pancakes](#App+pancakes) : <code>Array.&lt;Object&gt;</code>
            * [.resources](#App+resources) : <code>Array.&lt;Object&gt;</code>
    * _static_
        * [.defaults()](#App.defaults) ⇒ <code>Object</code>

Constructor
<a name="new_App_new"></a>

### new App(data)
Create an App


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| data | <code>Object</code> |  | Object that represents the app |
| data.name | <code>string</code> |  | The unique app name |
| data.version | <code>string</code> |  | The app version (ex: '1.0.0') |
| data.title | <code>string</code> |  | The displayed name |
| data.icon | <code>Buffer</code> |  | App icon (base 64) |
| data.color | <code>string</code> |  | The color assigned to the app, in hexadecimal, with the '#' (ex: '#FAB3E7') |
| [data.app_config] | <code>Object</code> | <code></code> | The app config for user configuration |
| data.pancakes | <code>Array.&lt;string&gt;</code> |  | The app pancakes component names |
| data.resources | <code>Array.&lt;Object&gt;</code> |  | The app resources |

<a name="App+name"></a>

### app.name : <code>string</code>
App name accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>App</code>](#App)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="App+version"></a>

### app.version : <code>string</code>
App version accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>App</code>](#App)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="App+title"></a>

### app.title : <code>string</code>
App title accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>App</code>](#App)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="App+icon"></a>

### app.icon : <code>Buffer</code>
App icon accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>App</code>](#App)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="App+color"></a>

### app.color : <code>string</code>
App color accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>App</code>](#App)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="App+appConfig"></a>

### app.appConfig : <code>Object</code>
App appConfig accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>App</code>](#App)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="App+pancakes"></a>

### app.pancakes : <code>Array.&lt;Object&gt;</code>
App pancakes accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>App</code>](#App)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="App+resources"></a>

### app.resources : <code>Array.&lt;Object&gt;</code>
App resources accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>App</code>](#App)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="App.defaults"></a>

### App.defaults() ⇒ <code>Object</code>
Default properties for a new empty 'App' object

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>App</code>](#App)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Object</code> - Default properties object  

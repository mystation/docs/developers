---
title: User class
headline: "MyStation User class"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation User class

<a name="User"></a>

## User
MyStation User model: User class.```javascript// Importimport User from '#mystation/models/User'```

<span class="color-beige"><strong>Kind:</strong></span> global class  

<span class="color-beige"><strong>Schema:</strong></span>

* [User](#User)
    * [new User(data, [authenticated])](#new_User_new)
    * _instance_
        * _methods_
            * [.fullName()](#User+fullName) ⇒ <code>string</code>
            * [.getApp(appName)](#User+getApp) ⇒ <code>App</code> \| <code>undefined</code>
        * _properties_
            * [.id](#User+id) : <code>number</code>
            * [.firstname](#User+firstname) : <code>string</code>
            * [.lastname](#User+lastname) : <code>string</code>
            * [.username](#User+username) : <code>string</code>
            * [.birthDate](#User+birthDate) : <code>Date</code>
            * [.role](#User+role) : <code>string</code>
            * [.createdAt](#User+createdAt) : <code>Date</code>
            * [.updatedAt](#User+updatedAt) : <code>Date</code>
            * [.apps](#User+apps) : <code>Array.&lt;App&gt;</code>
            * [.pancakes](#User+pancakes) : <code>Array.&lt;Object&gt;</code>
            * [.authenticated](#User+authenticated) : <code>boolean</code>
    * _static_
        * [.defaults()](#User.defaults) ⇒ <code>Object</code>

Constructor
<a name="new_User_new"></a>

### new User(data, [authenticated])
Create a User


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| data | <code>Object</code> |  | Object that represents the user |
| data.id | <code>string</code> |  | The unique user id |
| data.firstname | <code>string</code> |  | The user firstname |
| data.lastname | <code>string</code> |  | The user lastname |
| data.birth_date | <code>string</code> |  | The user birth date |
| data.role | <code>string</code> |  | The user role |
| data.created_at | <code>string</code> |  | When the user has been created |
| data.updated_at | <code>string</code> |  | When the user has been updated |
| [data.apps] | <code>Array.&lt;App&gt;</code> |  | The apps activated for the user |
| [authenticated] | <code>boolean</code> | <code>false</code> | If the user is authenticated (default: false) |

<a name="User+fullName"></a>

### user.fullName() ⇒ <code>string</code>
Returns concatenated firstname and lastname with a white space

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>User</code>](#User)  
<span class="color-beige"><strong>Returns:</strong></span> <code>string</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  
<a name="User+getApp"></a>

### user.getApp(appName) ⇒ <code>App</code> \| <code>undefined</code>
Returns the user app by unique name

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>User</code>](#User)  
<span class="color-beige"><strong>Returns:</strong></span> <code>App</code> \| <code>undefined</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| appName | <code>string</code> | The unique app name |

<a name="User+id"></a>

### user.id : <code>number</code>
User id accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>User</code>](#User)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="User+firstname"></a>

### user.firstname : <code>string</code>
User firstname accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>User</code>](#User)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="User+lastname"></a>

### user.lastname : <code>string</code>
User lastname accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>User</code>](#User)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="User+username"></a>

### user.username : <code>string</code>
User username accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>User</code>](#User)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="User+birthDate"></a>

### user.birthDate : <code>Date</code>
User birthDate accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>User</code>](#User)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="User+role"></a>

### user.role : <code>string</code>
User role accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>User</code>](#User)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="User+createdAt"></a>

### user.createdAt : <code>Date</code>
User createdAt accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>User</code>](#User)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="User+updatedAt"></a>

### user.updatedAt : <code>Date</code>
User updatedAt accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>User</code>](#User)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="User+apps"></a>

### user.apps : <code>Array.&lt;App&gt;</code>
User apps accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>User</code>](#User)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="User+pancakes"></a>

### user.pancakes : <code>Array.&lt;Object&gt;</code>
User pancakes accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>User</code>](#User)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="User+authenticated"></a>

### user.authenticated : <code>boolean</code>
User authenticated accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>User</code>](#User)  
<span class="color-beige"><strong>Category:</strong></span> properties  
<a name="User.defaults"></a>

### User.defaults() ⇒ <code>Object</code>
Default properties for a new empty 'User' object

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>User</code>](#User)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Object</code> - Default properties object  

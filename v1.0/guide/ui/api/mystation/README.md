---
title: MyStation API
headline: "MyStation API"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation API

<a name="module_mystation"></a>

## mystation
MyStation API```javascript// Importimport MyStation from '#mystation'```


<span class="color-beige"><strong>Schema:</strong></span>

* [mystation](#module_mystation)
    * [MyStation](#exp_module_mystation--MyStation) : <code>Object</code> ⏏
        * [.system](#module_mystation--MyStation.system) : <code>Object</code>
            * [.I18n](#module_mystation--MyStation.system.I18n) : <code>I18n</code>
            * [.Store](#module_mystation--MyStation.system.Store) : <code>Vuex.Store</code>
            * [.boot](#module_mystation--MyStation.system.boot) : <code>function</code>
        * [.modules](#module_mystation--MyStation.modules) : <code>Object</code>
            * [.auth](#module_mystation--MyStation.modules.auth) : <code>Object</code>
            * [.emit](#module_mystation--MyStation.modules.emit) : <code>function</code>
            * [.generate](#module_mystation--MyStation.modules.generate) : <code>Object</code>
            * [.hasInstalledApp](#module_mystation--MyStation.modules.hasInstalledApp) : <code>function</code>
            * [.initializeApp](#module_mystation--MyStation.modules.initializeApp) : <code>function</code>
            * [.isAppInitialized](#module_mystation--MyStation.modules.isAppInitialized) : <code>function</code>
            * [.log](#module_mystation--MyStation.modules.log) : <code>function</code>
            * [.on](#module_mystation--MyStation.modules.on) : <code>function</code>
            * [.registerApp](#module_mystation--MyStation.modules.registerApp) : <code>function</code>
            * [.ui](#module_mystation--MyStation.modules.ui) : <code>Object</code>
            * [.wait](#module_mystation--MyStation.modules.wait) : <code>function</code>
        * [.services](#module_mystation--MyStation.services) : <code>Object</code>
            * [.APIManager](#module_mystation--MyStation.services.APIManager) : <code>APIManager</code>
            * [.AppManager](#module_mystation--MyStation.services.AppManager) : <code>AppManager</code>
            * [.ErrorManager](#module_mystation--MyStation.services.ErrorManager) : <code>ErrorManager</code>
            * [.EventBus](#module_mystation--MyStation.services.EventBus) : <code>Vue</code>
            * [.Helpers](#module_mystation--MyStation.services.Helpers) : <code>Helpers</code>
            * [.LoadingManager](#module_mystation--MyStation.services.LoadingManager) : <code>LoadingManager</code>
            * [.Logger](#module_mystation--MyStation.services.Logger) : <code>Logger</code>

<a name="exp_module_mystation--MyStation"></a>

### MyStation : <code>Object</code> ⏏
MyStation

<span class="color-beige"><strong>Kind:</strong></span> Exported constant  
<a name="module_mystation--MyStation.system"></a>

#### MyStation.system : <code>Object</code>
system

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>MyStation</code>](#exp_module_mystation--MyStation)  

<span class="color-beige"><strong>Schema:</strong></span>

* [.system](#module_mystation--MyStation.system) : <code>Object</code>
    * [.I18n](#module_mystation--MyStation.system.I18n) : <code>I18n</code>
    * [.Store](#module_mystation--MyStation.system.Store) : <code>Vuex.Store</code>
    * [.boot](#module_mystation--MyStation.system.boot) : <code>function</code>

<a name="module_mystation--MyStation.system.I18n"></a>

##### system.I18n : <code>I18n</code>
I18n (see [I18n](../i18n))

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>system</code>](#module_mystation--MyStation.system)  
<a name="module_mystation--MyStation.system.Store"></a>

##### system.Store : <code>Vuex.Store</code>
Store (see [Store](../store))

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>system</code>](#module_mystation--MyStation.system)  
<a name="module_mystation--MyStation.system.boot"></a>

##### system.boot : <code>function</code>
Boot method

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>system</code>](#module_mystation--MyStation.system)  
<a name="module_mystation--MyStation.modules"></a>

#### MyStation.modules : <code>Object</code>
modules

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>MyStation</code>](#exp_module_mystation--MyStation)  

<span class="color-beige"><strong>Schema:</strong></span>

* [.modules](#module_mystation--MyStation.modules) : <code>Object</code>
    * [.auth](#module_mystation--MyStation.modules.auth) : <code>Object</code>
    * [.emit](#module_mystation--MyStation.modules.emit) : <code>function</code>
    * [.generate](#module_mystation--MyStation.modules.generate) : <code>Object</code>
    * [.hasInstalledApp](#module_mystation--MyStation.modules.hasInstalledApp) : <code>function</code>
    * [.initializeApp](#module_mystation--MyStation.modules.initializeApp) : <code>function</code>
    * [.isAppInitialized](#module_mystation--MyStation.modules.isAppInitialized) : <code>function</code>
    * [.log](#module_mystation--MyStation.modules.log) : <code>function</code>
    * [.on](#module_mystation--MyStation.modules.on) : <code>function</code>
    * [.registerApp](#module_mystation--MyStation.modules.registerApp) : <code>function</code>
    * [.ui](#module_mystation--MyStation.modules.ui) : <code>Object</code>
    * [.wait](#module_mystation--MyStation.modules.wait) : <code>function</code>

<a name="module_mystation--MyStation.modules.auth"></a>

##### modules.auth : <code>Object</code>
auth module (see [auth](modules/auth))

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>modules</code>](#module_mystation--MyStation.modules)  
<a name="module_mystation--MyStation.modules.emit"></a>

##### modules.emit : <code>function</code>
emit module (see [emit](modules/emit))

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>modules</code>](#module_mystation--MyStation.modules)  
<a name="module_mystation--MyStation.modules.generate"></a>

##### modules.generate : <code>Object</code>
generate module (see [generate](modules/generate))

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>modules</code>](#module_mystation--MyStation.modules)  
<a name="module_mystation--MyStation.modules.hasInstalledApp"></a>

##### modules.hasInstalledApp : <code>function</code>
hasInstalledApp module (see [hasInstalledApp](modules/hasInstalledApp))

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>modules</code>](#module_mystation--MyStation.modules)  
<a name="module_mystation--MyStation.modules.initializeApp"></a>

##### modules.initializeApp : <code>function</code>
initializeApp module (see [initializeApp](modules/initializeApp))

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>modules</code>](#module_mystation--MyStation.modules)  
<a name="module_mystation--MyStation.modules.isAppInitialized"></a>

##### modules.isAppInitialized : <code>function</code>
isAppInitialized module (see [isAppInitialized](modules/isAppInitialized))

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>modules</code>](#module_mystation--MyStation.modules)  
<a name="module_mystation--MyStation.modules.log"></a>

##### modules.log : <code>function</code>
log module (see [log](modules/log))

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>modules</code>](#module_mystation--MyStation.modules)  
<a name="module_mystation--MyStation.modules.on"></a>

##### modules.on : <code>function</code>
on module (see [on](modules/on))

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>modules</code>](#module_mystation--MyStation.modules)  
<a name="module_mystation--MyStation.modules.registerApp"></a>

##### modules.registerApp : <code>function</code>
registerApp module (see [registerApp](modules/registerApp))

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>modules</code>](#module_mystation--MyStation.modules)  
<a name="module_mystation--MyStation.modules.ui"></a>

##### modules.ui : <code>Object</code>
ui module (see [ui](modules/ui))

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>modules</code>](#module_mystation--MyStation.modules)  
<a name="module_mystation--MyStation.modules.wait"></a>

##### modules.wait : <code>function</code>
wait module (see [wait](modules/wait))

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>modules</code>](#module_mystation--MyStation.modules)  
<a name="module_mystation--MyStation.services"></a>

#### MyStation.services : <code>Object</code>
services

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>MyStation</code>](#exp_module_mystation--MyStation)  

<span class="color-beige"><strong>Schema:</strong></span>

* [.services](#module_mystation--MyStation.services) : <code>Object</code>
    * [.APIManager](#module_mystation--MyStation.services.APIManager) : <code>APIManager</code>
    * [.AppManager](#module_mystation--MyStation.services.AppManager) : <code>AppManager</code>
    * [.ErrorManager](#module_mystation--MyStation.services.ErrorManager) : <code>ErrorManager</code>
    * [.EventBus](#module_mystation--MyStation.services.EventBus) : <code>Vue</code>
    * [.Helpers](#module_mystation--MyStation.services.Helpers) : <code>Helpers</code>
    * [.LoadingManager](#module_mystation--MyStation.services.LoadingManager) : <code>LoadingManager</code>
    * [.Logger](#module_mystation--MyStation.services.Logger) : <code>Logger</code>

<a name="module_mystation--MyStation.services.APIManager"></a>

##### services.APIManager : <code>APIManager</code>
APIManager service (see [APIManager](services/APIManager))

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>services</code>](#module_mystation--MyStation.services)  
<a name="module_mystation--MyStation.services.AppManager"></a>

##### services.AppManager : <code>AppManager</code>
AppManager service (see [AppManager](services/AppManager))

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>services</code>](#module_mystation--MyStation.services)  
<a name="module_mystation--MyStation.services.ErrorManager"></a>

##### services.ErrorManager : <code>ErrorManager</code>
ErrorManager service (see [ErrorManager](services/ErrorManager))

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>services</code>](#module_mystation--MyStation.services)  
<a name="module_mystation--MyStation.services.EventBus"></a>

##### services.EventBus : <code>Vue</code>
EventBus service (see [EventBus](services/EventBus))

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>services</code>](#module_mystation--MyStation.services)  
<a name="module_mystation--MyStation.services.Helpers"></a>

##### services.Helpers : <code>Helpers</code>
Helpers service (see [Helpers](services/Helpers))

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>services</code>](#module_mystation--MyStation.services)  
<a name="module_mystation--MyStation.services.LoadingManager"></a>

##### services.LoadingManager : <code>LoadingManager</code>
LoadingManager service (see [LoadingManager](services/LoadingManager))

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>services</code>](#module_mystation--MyStation.services)  
<a name="module_mystation--MyStation.services.Logger"></a>

##### services.Logger : <code>Logger</code>
Logger service (see [Logger](services/Logger))

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>services</code>](#module_mystation--MyStation.services)  

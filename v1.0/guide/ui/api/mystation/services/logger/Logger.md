---
title: Logger class
headline: "MyStation Logger class"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation Logger class

<a name="Logger"></a>

## Logger ⇐ <code>ServiceAsPlugin</code>
MyStation Logger: Logger class.See also [ServiceAsPlugin](../ServiceAsPlugin)```javascript// Importimport Logger from '#mystation/services/logger/Logger'```

<span class="color-beige"><strong>Kind:</strong></span> global class  
**Extends**: <code>ServiceAsPlugin</code>  

<span class="color-beige"><strong>Schema:</strong></span>

* [Logger](#Logger) ⇐ <code>ServiceAsPlugin</code>
    * [new Logger()](#new_Logger_new)
    * _methods_
        * [.push(type, message, [location])](#Logger+push) ⇒ <code>void</code>
        * [.consoleLog(type, message)](#Logger+consoleLog) ⇒ <code>void</code>
    * _properties_
        * [.queue](#Logger+queue) ⇒ <code>Array.&lt;string&gt;</code>

Constructor
<a name="new_Logger_new"></a>

### new Logger()
Create a new Logger

<a name="Logger+push"></a>

### logger.push(type, message, [location]) ⇒ <code>void</code>
Push a log in the queue

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>Logger</code>](#Logger)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| type | <code>string</code> |  | Type ('info', 'advice', ...) |
| message | <code>string</code> |  | The message |
| [location] | <code>string</code> | <code>&quot;&#x27;ui&#x27;&quot;</code> | Where display: 'ui' by default, else 'console' |

<a name="Logger+consoleLog"></a>

### logger.consoleLog(type, message) ⇒ <code>void</code>
Log a message in the console

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>Logger</code>](#Logger)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| type | <code>string</code> | Type ('info', 'advice', ...) |
| message | <code>string</code> | The message |

<a name="Logger+queue"></a>

### logger.queue ⇒ <code>Array.&lt;string&gt;</code>
queue

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>Logger</code>](#Logger)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Array.&lt;string&gt;</code>  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  

---
title: Helpers class
headline: "MyStation Helpers class"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation Helpers class

<a name="Helpers"></a>

## Helpers ⇐ <code>ServiceAsPlugin</code>
MyStation helpers: Helpers class.See also [ServiceAsPlugin](../ServiceAsPlugin)```javascript// Importimport Helpers from '#mystation/services/helpers/Helpers'```

<span class="color-beige"><strong>Kind:</strong></span> global class  
**Extends**: <code>ServiceAsPlugin</code>  

<span class="color-beige"><strong>Schema:</strong></span>

* [Helpers](#Helpers) ⇐ <code>ServiceAsPlugin</code>
    * [new Helpers()](#new_Helpers_new)
    * [.camelize(str)](#Helpers.camelize) ⇒ <code>string</code>
    * [.capitalize(str)](#Helpers.capitalize) ⇒ <code>string</code>
    * [.kebabcase(str)](#Helpers.kebabcase) ⇒ <code>string</code>
    * [.snakecase(str)](#Helpers.snakecase) ⇒ <code>string</code>
    * [.uppercase(str)](#Helpers.uppercase) ⇒ <code>string</code>

Constructor
<a name="new_Helpers_new"></a>

### new Helpers()
Create a new instance

<a name="Helpers.camelize"></a>

### Helpers.camelize(str) ⇒ <code>string</code>
Camelize a string (lodash.camelize).Defined as Vue filter.

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>Helpers</code>](#Helpers)  
<span class="color-beige"><strong>Returns:</strong></span> <code>string</code>  

| Param | Type | Description |
| --- | --- | --- |
| str | <code>string</code> | The string to camelize |

<span class="color-beige"><strong>Example:</strong></span>  
```js
Helpers.camelize('Foo Bar') // => 'fooBar'{{ 'foo' | camelize }} // Vue filter
```
<a name="Helpers.capitalize"></a>

### Helpers.capitalize(str) ⇒ <code>string</code>
Capitalize the first letter of a string (lodash.capitalize).Defined as Vue filter.

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>Helpers</code>](#Helpers)  
<span class="color-beige"><strong>Returns:</strong></span> <code>string</code>  

| Param | Type | Description |
| --- | --- | --- |
| str | <code>string</code> | The string to capitalize |

<span class="color-beige"><strong>Example:</strong></span>  
```js
Helpers.capitalize('Foo Bar') // => 'Foo bar'{{ 'foo' | capitalize }} // Vue filter
```
<a name="Helpers.kebabcase"></a>

### Helpers.kebabcase(str) ⇒ <code>string</code>
Transform a full string to kebabcase (lodash.kebabcase).Defined as Vue filter.

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>Helpers</code>](#Helpers)  
<span class="color-beige"><strong>Returns:</strong></span> <code>string</code>  

| Param | Type | Description |
| --- | --- | --- |
| str | <code>string</code> | The string to kebabcase |

<span class="color-beige"><strong>Example:</strong></span>  
```js
Helpers.kebabcase('Foo Bar') // => 'foo-bar'{{ 'foo' | kebabcase }} // Vue filter
```
<a name="Helpers.snakecase"></a>

### Helpers.snakecase(str) ⇒ <code>string</code>
Transform a full string to snakecase (lodash.snakecase).Defined as Vue filter.

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>Helpers</code>](#Helpers)  
<span class="color-beige"><strong>Returns:</strong></span> <code>string</code>  

| Param | Type | Description |
| --- | --- | --- |
| str | <code>string</code> | The string to snakecase |

<span class="color-beige"><strong>Example:</strong></span>  
```js
Helpers.snakecase('Foo Bar') // => 'foo_bar'{{ 'foo' | snakecase }} // Vue filter
```
<a name="Helpers.uppercase"></a>

### Helpers.uppercase(str) ⇒ <code>string</code>
Transform a full string to uppercases (lodash.uppercase).Defined as Vue filter.

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>Helpers</code>](#Helpers)  
<span class="color-beige"><strong>Returns:</strong></span> <code>string</code>  

| Param | Type | Description |
| --- | --- | --- |
| str | <code>string</code> | The string to uppercase |

<span class="color-beige"><strong>Example:</strong></span>  
```js
Helpers.uppercase('Foo Bar') // => 'FOO BAR'{{ 'foo' | uppercase }} // Vue filter
```

---
title: Helpers
headline: "MyStation service: Helpers"
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation service: Helpers

<a name="module_helpers"></a>

## helpers
MyStation service: Helpers```javascript// Importimport Helpers from '#mystation/services/helpers'```

<a name="exp_module_helpers--module.exports"></a>

### module.exports : <code>Helpers</code> ⏏
Helpers serviceSee also [Helpers class](Helpers)

<span class="color-beige"><strong>Kind:</strong></span> Exported constant  

::: syntax Helpers

Access to Helpers instance
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.$Helpers
  this.$h // alias
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  this.$Helpers
  this.$h // alias__MYSTATION__.$Helpers__MYSTATION__.$h // alias
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.$Helpers
  window.__MYSTATION_VUE_INSTANCE__.$h // alias
  ```
:::


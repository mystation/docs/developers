---
title: ErrorManager class
headline: "MyStation ErrorManager class"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation ErrorManager class

<a name="ErrorManager"></a>

## ErrorManager ⇐ <code>ServiceAsPlugin</code>
MyStation Error Manager: ErrorManager class.See also [ServiceAsPlugin](../ServiceAsPlugin)```javascript// Importimport ErrorManager from '#mystation/services/error-manager/ErrorManager'```

<span class="color-beige"><strong>Kind:</strong></span> global class  
**Extends**: <code>ServiceAsPlugin</code>  

<span class="color-beige"><strong>Schema:</strong></span>

* [ErrorManager](#ErrorManager) ⇐ <code>ServiceAsPlugin</code>
    * [new ErrorManager([config])](#new_ErrorManager_new)
    * _instance_
        * _methods_
            * [.throw(errorId, options)](#ErrorManager+throw) ⇒ <code>Object</code>
            * [.pushInHistory(error)](#ErrorManager+pushInHistory) ⇒ <code>Object</code>
            * [.clear()](#ErrorManager+clear) ⇒ <code>void</code>
            * [.clearHistory()](#ErrorManager+clearHistory) ⇒ <code>void</code>
        * _properties_
            * [.errorRefs](#ErrorManager+errorRefs) ⇒ <code>Object</code>
            * [.error](#ErrorManager+error) ⇒ <code>Object</code>
            * [.last](#ErrorManager+last) ⇒ <code>Object</code>
            * [.history](#ErrorManager+history) ⇒ <code>Array</code>
    * _static_
        * [.defaultOptions()](#ErrorManager.defaultOptions) ⇒ <code>Object</code>
        * [.mergeI18nErrorRefs(errorRefs)](#ErrorManager.mergeI18nErrorRefs) ⇒ <code>Object</code>

Constructor
<a name="new_ErrorManager_new"></a>

### new ErrorManager([config])
Receive an object like:```javascript// in default case{  errorRefs: {    e0001: {      message: '',      description: ''    }  }}// in case of i18n activated{  errorRefs: {    en: {      object (see above)      ...    },    fr: {      ...    }  },  i18n: true}```


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [config] | <code>Object</code> | <code>{}</code> | The error manager config |
| [config.errorRefs] | <code>Object</code> |  | The error manager error references (objects) |
| [config.options] | <code>Object</code> |  | Error manager options |
| [config.options.i18n] | <code>boolean</code> |  | I18n support options |

<a name="ErrorManager+throw"></a>

### errorManager.throw(errorId, options) ⇒ <code>Object</code>
Throw an error that is defined in errorRefs

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>ErrorManager</code>](#ErrorManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Object</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| errorId | <code>string</code> | Error id defined in errorRefs |
| options | <code>Object</code> |  |

<a name="ErrorManager+pushInHistory"></a>

### errorManager.pushInHistory(error) ⇒ <code>Object</code>
Push error in history

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>ErrorManager</code>](#ErrorManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Object</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| error | <code>Object</code> | The error object |

<a name="ErrorManager+clear"></a>

### errorManager.clear() ⇒ <code>void</code>
Clear current error (set to null)

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>ErrorManager</code>](#ErrorManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  
<a name="ErrorManager+clearHistory"></a>

### errorManager.clearHistory() ⇒ <code>void</code>
Clear history

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>ErrorManager</code>](#ErrorManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  
<a name="ErrorManager+errorRefs"></a>

### errorManager.errorRefs ⇒ <code>Object</code>
Get errorRefs

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>ErrorManager</code>](#ErrorManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Object</code>  
<span class="color-beige"><strong>Category:</strong></span> properties  
<a name="ErrorManager+error"></a>

### errorManager.error ⇒ <code>Object</code>
When set error, push the error in history

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>ErrorManager</code>](#ErrorManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Object</code>  
<span class="color-beige"><strong>Category:</strong></span> properties  
<a name="ErrorManager+last"></a>

### errorManager.last ⇒ <code>Object</code>
Return last error in history

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>ErrorManager</code>](#ErrorManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Object</code>  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="ErrorManager+history"></a>

### errorManager.history ⇒ <code>Array</code>
Return the history

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>ErrorManager</code>](#ErrorManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Array</code>  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="ErrorManager.defaultOptions"></a>

### ErrorManager.defaultOptions() ⇒ <code>Object</code>
Return default options for the constructor

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>ErrorManager</code>](#ErrorManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Object</code>  
<a name="ErrorManager.mergeI18nErrorRefs"></a>

### ErrorManager.mergeI18nErrorRefs(errorRefs) ⇒ <code>Object</code>
Merge i18n error references

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>ErrorManager</code>](#ErrorManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Object</code>  

| Param | Type |
| --- | --- |
| errorRefs | <code>Object</code> | 


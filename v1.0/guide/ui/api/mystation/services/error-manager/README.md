---
title: ErrorManager
headline: "MyStation service: ErrorManager"
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation service: ErrorManager

<a name="module_error-manager"></a>

## error-manager
MyStation service: ErrorManager```javascript// Importimport ErrorManager from '#mystation/services/error-manager'```

<a name="exp_module_error-manager--module.exports"></a>

### module.exports : <code>ErrorManager</code> ⏏
ErrorManager serviceSee also [ErrorManager class](ErrorManager)

<span class="color-beige"><strong>Kind:</strong></span> Exported constant  

::: syntax ErrorManager

Access to ErrorManager instance
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.$ErrorManager
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  this.$ErrorManager
  __MYSTATION__.$ErrorManager
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.$ErrorManager
  ```
:::


---
title: EventBus
headline: "MyStation service: EventBus"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation service: EventBus

<a name="module_event-bus"></a>

## event-bus
MyStation service: EventBus```javascript// Importimport EventBus from '#mystation/services/event-bus'```

<a name="exp_module_event-bus--module.exports"></a>

### module.exports : <code>Vue</code> ⏏
EventBus serviceThe EventBus is the global event bus of MyStation system.All Vue instances have access to this.$EventBus and its properties:- $on- $emit- ...

<span class="color-beige"><strong>Kind:</strong></span> Exported constant  

::: syntax EventBus

Access to EventBus instance
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.$EventBus
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  this.$EventBus
  __MYSTATION__.$EventBus
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.$EventBus
  ```
:::


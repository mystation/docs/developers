---
title: LoadingManager class
headline: "MyStation LoadingManager class"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation LoadingManager class

<a name="LoadingManager"></a>

## LoadingManager ⇐ <code>ServiceAsPlugin</code>
MyStation Loading Manager: LoadingManager class.See also [ServiceAsPlugin](../ServiceAsPlugin)```javascript// Importimport LoadingManager from '#mystation/services/loading-manager/LoadingManager'```

<span class="color-beige"><strong>Kind:</strong></span> global class  
**Extends**: <code>ServiceAsPlugin</code>  

<span class="color-beige"><strong>Schema:</strong></span>

* [LoadingManager](#LoadingManager) ⇐ <code>ServiceAsPlugin</code>
    * [new LoadingManager([loadings])](#new_LoadingManager_new)
    * _methods_
        * [.clear()](#LoadingManager+clear) ⇒ <code>void</code>
        * [.wait(description, [cb], [options])](#LoadingManager+wait) ⇒ <code>boolean</code>
    * _properties_
        * [.loadings](#LoadingManager+loadings) ⇒ <code>Array.&lt;Loading&gt;</code>

Constructor
<a name="new_LoadingManager_new"></a>

### new LoadingManager([loadings])
Create a new Loading manager


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [loadings] | <code>Array.&lt;Loading&gt;</code> | <code>[]</code> | The loadings to register |

<a name="LoadingManager+clear"></a>

### loadingManager.clear() ⇒ <code>void</code>
Clear : turn to false all loadings status in this.loadings

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>LoadingManager</code>](#LoadingManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  
<a name="LoadingManager+wait"></a>

### loadingManager.wait(description, [cb], [options]) ⇒ <code>boolean</code>
'Wait' method to run loading during actions execution

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>LoadingManager</code>](#LoadingManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>boolean</code> - Return true when all is executed and loading to false  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| description | <code>string</code> | The description to fill |
| [cb] | <code>function</code> | Callback to wait for. (can be async). |
| [options] | <code>Object</code> | The options |
| [options.loading] | <code>Array.&lt;string&gt;</code> | The target(s) (array of loading names) |

<a name="LoadingManager+loadings"></a>

### loadingManager.loadings ⇒ <code>Array.&lt;Loading&gt;</code>
loadings accessor

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>LoadingManager</code>](#LoadingManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Array.&lt;Loading&gt;</code>  
<span class="color-beige"><strong>Category:</strong></span> properties  

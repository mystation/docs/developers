---
title: Loading class
headline: "MyStation Loading class"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation Loading class

<a name="Loading"></a>

## Loading
MyStation Loading Manager: Loading class```javascript// Importimport Loading from '#mystation/services/loading-manager/Loading'```

<span class="color-beige"><strong>Kind:</strong></span> global class  

<span class="color-beige"><strong>Schema:</strong></span>

* [Loading](#Loading)
    * [new Loading(name, [defaultDescription])](#new_Loading_new)
    * _methods_
        * [.run(description)](#Loading+run) ⇒ <code>void</code>
        * [.stop()](#Loading+stop) ⇒ <code>void</code>
    * _properties_
        * [.name](#Loading+name) ⇒ <code>string</code>
        * [.defaultDescription](#Loading+defaultDescription) ⇒ <code>string</code>
        * [.description](#Loading+description) ⇒ <code>string</code>
        * [.status](#Loading+status) ⇒ <code>boolean</code>

Constructor
<a name="new_Loading_new"></a>

### new Loading(name, [defaultDescription])
Create a new Loading


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| name | <code>string</code> |  | The loading name |
| [defaultDescription] | <code>string</code> | <code>&quot;&#x27;Loading&#x27;&quot;</code> | The default description |

<a name="Loading+run"></a>

### loading.run(description) ⇒ <code>void</code>
Run method: set description and status to true

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>Loading</code>](#Loading)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| description | <code>string</code> | The description to fill |

<a name="Loading+stop"></a>

### loading.stop() ⇒ <code>void</code>
Stop method: reset description and set status to false

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>Loading</code>](#Loading)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  
<a name="Loading+name"></a>

### loading.name ⇒ <code>string</code>
Get the Loading name

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>Loading</code>](#Loading)  
<span class="color-beige"><strong>Returns:</strong></span> <code>string</code>  
<span class="color-beige"><strong>Category:</strong></span> properties  
<a name="Loading+defaultDescription"></a>

### loading.defaultDescription ⇒ <code>string</code>
Get the Loading default description

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>Loading</code>](#Loading)  
<span class="color-beige"><strong>Returns:</strong></span> <code>string</code>  
<span class="color-beige"><strong>Category:</strong></span> properties  
<a name="Loading+description"></a>

### loading.description ⇒ <code>string</code>
Get the Loading description

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>Loading</code>](#Loading)  
<span class="color-beige"><strong>Returns:</strong></span> <code>string</code>  
<span class="color-beige"><strong>Category:</strong></span> properties  
<a name="Loading+status"></a>

### loading.status ⇒ <code>boolean</code>
Get the Loading status

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>Loading</code>](#Loading)  
<span class="color-beige"><strong>Returns:</strong></span> <code>boolean</code>  
<span class="color-beige"><strong>Category:</strong></span> properties  

---
title: LoadingManager
headline: "MyStation service: LoadingManager"
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation service: LoadingManager

<a name="module_loading-manager"></a>

## loading-manager
MyStation service: LoadingManager```javascript// Importimport LoadingManager from '#mystation/services/loading-manager'```

<a name="exp_module_loading-manager--module.exports"></a>

### module.exports : <code>LoadingManager</code> ⏏
LoadingManager serviceSee also [LoadingManager class](LoadingManager)

<span class="color-beige"><strong>Kind:</strong></span> Exported constant  

::: syntax LoadingManager

Access to LoadingManager instance
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.$LoadingManager
  this.$loadings // alias of 'this.$LoadingManager.loadings'
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  this.$LoadingManager
  this.$loadings // alias of 'this.$LoadingManager.loadings'__MYSTATION__.$LoadingManager__MYSTATION__.$loadings // alias of '__MYSTATION__.$LoadingManager.loadings'
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.$LoadingManager
  window.__MYSTATION_VUE_INSTANCE__.$loadings // alias of 'window.__MYSTATION_VUE_INSTANCE__.$LoadingManager.loadings'
  ```
:::


---
title: AppManager class
headline: "MyStation AppManager class"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation AppManager class

<a name="AppManager"></a>

## AppManager ⇐ <code>ServiceAsPlugin</code>
MyStation App Manager: AppManager class.See also [ServiceAsPlugin](../ServiceAsPlugin)```javascript// Importimport AppManager from '#mystation/services/app-manager/AppManager'```

<span class="color-beige"><strong>Kind:</strong></span> global class  
**Extends**: <code>ServiceAsPlugin</code>  

<span class="color-beige"><strong>Schema:</strong></span>

* [AppManager](#AppManager) ⇐ <code>ServiceAsPlugin</code>
    * [new AppManager()](#new_AppManager_new)
    * _methods_
        * [.sync()](#AppManager+sync) ⇒ <code>void</code>
        * [.installApp(appName, appVersion)](#AppManager+installApp) ⇒ <code>boolean</code>
        * [.appInstallRequest(appName, appVersion)](#AppManager+appInstallRequest) ⇒ <code>boolean</code> \| <code>Error</code>
        * [.uninstallApp(appName)](#AppManager+uninstallApp) ⇒ <code>boolean</code> \| <code>Error</code>
        * [.loadSources()](#AppManager+loadSources) ⇒ <code>void</code>
        * [.unloadSources(appName)](#AppManager+unloadSources) ⇒ <code>void</code>
        * [.reloadSources()](#AppManager+reloadSources) ⇒ <code>void</code>
        * [.register(appName, appModule)](#AppManager+register) ⇒ <code>void</code>
        * [.registerAppTranslations(appName, lang, messages)](#AppManager+registerAppTranslations) ⇒ <code>void</code>
        * [.unregisterAppTranslations(appName)](#AppManager+unregisterAppTranslations) ⇒ <code>void</code>
        * [.registerAppComponent(appName, componentName, component)](#AppManager+registerAppComponent) ⇒ <code>void</code>
        * [.registerAppModule(appName, moduleName, moduleObject)](#AppManager+registerAppModule) ⇒ <code>void</code>
        * [.unregisterAppModule(appName, moduleName)](#AppManager+unregisterAppModule) ⇒ <code>void</code>
        * [.generateAppModuleResourceMappers(appName, resourceIdentifier)](#AppManager+generateAppModuleResourceMappers) ⇒ <code>Object</code>
    * _properties_
        * [.apps](#AppManager+apps) ⇒ <code>Array.&lt;Object&gt;</code>
        * [.isAppInstalling](#AppManager+isAppInstalling) ⇒ <code>boolean</code>
        * [.isAppUninstalling](#AppManager+isAppUninstalling) ⇒ <code>boolean</code>

Constructor
<a name="new_AppManager_new"></a>

### new AppManager()
Create a new App Manager

<a name="AppManager+sync"></a>

### appManager.sync() ⇒ <code>void</code>
Synchronize apps data with the store.For each app:```javascriptthis.apps[app.name] = {  version: app.version,  translations: {    en: { status: false },    fr: { status: false }  },  bundles: {    vendors: {      filename: 'js/vendors.js',      url: '',      element: null,      status: false    },    app: {      filename: 'js/app.js',      url: '',      element: null,      status: false    }  },  components: {},  modules: {}}```

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>AppManager</code>](#AppManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  
<a name="AppManager+installApp"></a>

### appManager.installApp(appName, appVersion) ⇒ <code>boolean</code>
Install an app.Request server API (See `appInstallRequest` method) andreload apps and user data in store.

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>AppManager</code>](#AppManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>boolean</code> - True if success, else false  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| appName | <code>string</code> | App unique name |
| appVersion | <code>string</code> | App version |

<a name="AppManager+appInstallRequest"></a>

### appManager.appInstallRequest(appName, appVersion) ⇒ <code>boolean</code> \| <code>Error</code>
Ask to API Server to install app.Request `[POST] /apps/install` (See [API Server documentation](/guide/system/api-server/)).

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>AppManager</code>](#AppManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>boolean</code> \| <code>Error</code> - Return true if success or Error  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| appName | <code>string</code> | App unique name |
| appVersion | <code>string</code> | App version |

<a name="AppManager+uninstallApp"></a>

### appManager.uninstallApp(appName) ⇒ <code>boolean</code> \| <code>Error</code>
Uninstall an app.Request `[POST] /apps/install` (See [API Server documentation](/guide/system/api-server/)).If success:- remove app translations- unregister store modules- remove bundles (scripts)- reload apps and user data in store- synchronize with store data- reload all apps bundles

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>AppManager</code>](#AppManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>boolean</code> \| <code>Error</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| appName | <code>string</code> | The unique app name |

<a name="AppManager+loadSources"></a>

### appManager.loadSources() ⇒ <code>void</code>
Once the instance synchronized (by sync() method),load each app bundles (app.js + vendors.js):add a script tag to DOM with corresponding url

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>AppManager</code>](#AppManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  
<a name="AppManager+unloadSources"></a>

### appManager.unloadSources(appName) ⇒ <code>void</code>
Unload app sources (bundles): remove script tag.

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>AppManager</code>](#AppManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| appName | <code>string</code> | App unique name |

<a name="AppManager+reloadSources"></a>

### appManager.reloadSources() ⇒ <code>void</code>
Reload all apps sources

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>AppManager</code>](#AppManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  
<a name="AppManager+register"></a>

### appManager.register(appName, appModule) ⇒ <code>void</code>
Register an app:- register translations- register components (app component and pancakes)- register store modulesThis method is called by apps entry point. See [App development documentation](/guide/app-development).

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>AppManager</code>](#AppManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| appName | <code>string</code> | The unique app name |
| appModule | <code>Object</code> | The app module (!! Not the store module) |
| appModule.components | <code>Object</code> | The app components |
| appModule.components.app | <code>Object</code> | The main app component |
| [appModule.components.pancakes] | <code>Object</code> | The app pancakes |
| appModule.i18n | <code>Object</code> | The app translations |
| appModule.i18n.en | <code>Object</code> | The english app translations |
| [appModule.i18n.fr] | <code>Object</code> | The french app translations |
| [appModule.store] | <code>Object</code> | The app store modules |
| [appModule.store.modules] | <code>Object</code> | The app store modules |

<a name="AppManager+registerAppTranslations"></a>

### appManager.registerAppTranslations(appName, lang, messages) ⇒ <code>void</code>
Register app translations:merge app translations with I18n locale messages

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>AppManager</code>](#AppManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| appName | <code>string</code> | The app unique name |
| lang | <code>string</code> | The lang ('fr'/'en') |
| messages | <code>object</code> | The messages object |

<a name="AppManager+unregisterAppTranslations"></a>

### appManager.unregisterAppTranslations(appName) ⇒ <code>void</code>
Unregister app translations

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>AppManager</code>](#AppManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| appName | <code>string</code> | The app unique name |

<a name="AppManager+registerAppComponent"></a>

### appManager.registerAppComponent(appName, componentName, component) ⇒ <code>void</code>
Register an app component. Apply a mixinthat checks component options:```javascript// Component{  // ... other Vue component properties  state: {    '<moduleName>': [] // list of module state properties to map  },  getters: {    '<moduleName>': [] // list of module getters to map  },  actions: {    '<moduleName>': [] // list of module actions to map  }}```

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>AppManager</code>](#AppManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| appName | <code>string</code> | The app unique name |
| componentName | <code>boolean</code> \| <code>string</code> | The component name. False if app main component |
| component | <code>Object</code> | The component object |

<a name="AppManager+registerAppModule"></a>

### appManager.registerAppModule(appName, moduleName, moduleObject) ⇒ <code>void</code>
Register an app store module

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>AppManager</code>](#AppManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| appName | <code>string</code> | The app unique name |
| moduleName | <code>string</code> | The store module name |
| moduleObject | <code>Object</code> | The module object |

<a name="AppManager+unregisterAppModule"></a>

### appManager.unregisterAppModule(appName, moduleName) ⇒ <code>void</code>
Unegister an app store module

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>AppManager</code>](#AppManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| appName | <code>string</code> | The app unique name |
| moduleName | <code>string</code> | The store module name |

<a name="AppManager+generateAppModuleResourceMappers"></a>

### appManager.generateAppModuleResourceMappers(appName, resourceIdentifier) ⇒ <code>Object</code>
Generate app module resource mappers (state, getters, actions, mutations, etc...):Create an object like:```javascript// <identifier> is the name of the app resource{  state: {    // camelized    '<identifier>': [],    '<identifier>Loading': false,    '<identifier>Creating': false,    '<identifier>Updating': false,    '<identifier>Deleting': false  },  getters: {    // camelized    'find<identifier>'  },  actions: {    // camelized    'load<identifier>': Function,    'create<identifier>': Function,    'update<identifier>': Function,    'delete<identifier>': Function  },  mutations: {    // uppercase    SET_<identifier>: Function,    SET_<identifier>_LOADING: Function,    SET_<identifier>_CREATING: Function,    SET_<identifier>_UPDATING: Function,    SET_<identifier>_DELETING: Function  }}```

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>AppManager</code>](#AppManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Object</code> - Return a store module type object  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| appName | <code>string</code> | App unique name |
| resourceIdentifier | <code>string</code> | The resource identifier |

<a name="AppManager+apps"></a>

### appManager.apps ⇒ <code>Array.&lt;Object&gt;</code>
apps

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>AppManager</code>](#AppManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Array.&lt;Object&gt;</code>  
<span class="color-beige"><strong>Category:</strong></span> properties  
<a name="AppManager+isAppInstalling"></a>

### appManager.isAppInstalling ⇒ <code>boolean</code>
isAppInstalling

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>AppManager</code>](#AppManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>boolean</code>  
<span class="color-beige"><strong>Category:</strong></span> properties  
<a name="AppManager+isAppUninstalling"></a>

### appManager.isAppUninstalling ⇒ <code>boolean</code>
isAppUninstalling

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>AppManager</code>](#AppManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>boolean</code>  
<span class="color-beige"><strong>Category:</strong></span> properties  

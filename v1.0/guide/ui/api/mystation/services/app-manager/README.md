---
title: AppManager
headline: "MyStation service: AppManager"
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation service: AppManager

<a name="module_app-manager"></a>

## app-manager
MyStation service: AppManager```javascript// Importimport AppManager from '#mystation/services/app-manager'```

<a name="exp_module_app-manager--module.exports"></a>

### module.exports : <code>AppManager</code> ⏏
AppManager serviceSee also [AppManager class](AppManager)

<span class="color-beige"><strong>Kind:</strong></span> Exported constant  

::: syntax AppManager

Access to AppManager instance
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.$AppManager
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  this.$AppManager
  __MYSTATION__.$AppManager
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.$AppManager
  ```
:::


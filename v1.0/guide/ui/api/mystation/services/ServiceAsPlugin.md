---
title: ServiceAsPlugin class
headline: "MyStation ServiceAsPlugin class"
sidebarDepth: 0 # To disable auto sidebar links
---

# MyStation ServiceAsPlugin class

<a name="ServiceAsPlugin"></a>

## *ServiceAsPlugin*
MyStation services: ServiceAsPlugin abstract class.A parent class to declare a service as a Vue plugin```javascript// Importimport ServiceAsPlugin from '#mystation/services/ServiceAsPlugin'```

<span class="color-beige"><strong>Kind:</strong></span> global abstract class  

<span class="color-beige"><strong>Schema:</strong></span>

* *[ServiceAsPlugin](#ServiceAsPlugin)*
    * *[new ServiceAsPlugin([reactive], [vmData])](#new_ServiceAsPlugin_new)*
    * _instance_
        * _methods_
            * *[.setVMProp(name, value)](#ServiceAsPlugin+setVMProp) ⇒ <code>void</code>*
            * *[._initVM(data)](#ServiceAsPlugin+_initVM)*
            * *[.destroyVM()](#ServiceAsPlugin+destroyVM) ⇒ <code>void</code>*
        * _properties_
            * *[.vm](#ServiceAsPlugin+vm) ⇒ <code>Vue</code>*
    * _static_
        * *[.createMixin([reactive], [pluginName], [customMixin])](#ServiceAsPlugin.createMixin) ⇒ <code>Object</code>*
        * **[.install(_Vue, [options])](#ServiceAsPlugin.install)**

Constructor
<a name="new_ServiceAsPlugin_new"></a>

### *new ServiceAsPlugin([reactive], [vmData])*
ServiceAsPlugin abstract class cannot be instantiated with 'new'


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [reactive] | <code>boolean</code> | <code>false</code> | Define if the plugin must be 'reactive'. if true, init VM with vmData argument |
| [vmData] | <code>Object</code> | <code>{}</code> | The data to send to _initVM method for reactivity |

<a name="ServiceAsPlugin+setVMProp"></a>

### *service.setVMProp(name, value) ⇒ <code>void</code>*
Set a property to this._vm

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>ServiceAsPlugin</code>](#ServiceAsPlugin)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>String</code> | Name of the property |
| value | <code>any</code> | The value |

<a name="ServiceAsPlugin+_initVM"></a>

### *service.\_initVM(data)*
Init the view model to use with Vue

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>ServiceAsPlugin</code>](#ServiceAsPlugin)  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type |
| --- | --- |
| data | <code>Object</code> | 

<a name="ServiceAsPlugin+destroyVM"></a>

### *service.destroyVM() ⇒ <code>void</code>*
Destroy the vm

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>ServiceAsPlugin</code>](#ServiceAsPlugin)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  
<a name="ServiceAsPlugin+vm"></a>

### *service.vm ⇒ <code>Vue</code>*
vm

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>ServiceAsPlugin</code>](#ServiceAsPlugin)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Vue</code> - Returns the Vue Instance  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="ServiceAsPlugin.createMixin"></a>

### *ServiceAsPlugin.createMixin([reactive], [pluginName], [customMixin]) ⇒ <code>Object</code>*
Create a mixin to apply to Vue

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>ServiceAsPlugin</code>](#ServiceAsPlugin)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Object</code> - Return the created mixin  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [reactive] | <code>boolean</code> | <code>false</code> | Define if the plugin must be 'reactive' If set to true, it will inject 'beforeCreate' and 'beforeDestroy' method that will manage the inheritance for the reactivity using the _vm property and _initVM() method of the instance |
| [pluginName] | <code>string</code> | <code>null</code> | The plugin name, required if reactive is set to true |
| [customMixin] | <code>Object</code> | <code>{}</code> | An Object that follows the same schema as a Vue mixin, received by the user if he needs to customize the mixin. Note that 'beforeCreate' and 'beforeDestroy' methods declared below will be overwritten if the user provide these methods into the customMixin object param |

<a name="ServiceAsPlugin.install"></a>

### **ServiceAsPlugin.install(_Vue, [options])**
install method for Vue plugin: must be implemented.

<span class="color-beige"><strong>Kind:</strong></span> static abstract method of [<code>ServiceAsPlugin</code>](#ServiceAsPlugin)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| _Vue | <code>Vue</code> |  | The Vue instance |
| [options] | <code>Object</code> | <code>{}</code> | The options of the plugin |


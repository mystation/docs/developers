---
title: APIOptions class
headline: "MyStation APIOptions class"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation APIOptions class

<a name="APIOptions"></a>

## APIOptions
MyStation API Manager: APIOptions class```javascript// Importimport APIOptions from '#mystation/services/api-manager/APIOptions'```

<span class="color-beige"><strong>Kind:</strong></span> global class  

<span class="color-beige"><strong>Schema:</strong></span>

* [APIOptions](#APIOptions)
    * [new APIOptions(name, driver, options)](#new_APIOptions_new)
    * [.name](#APIOptions+name) ⇒ <code>string</code>
    * [.driver](#APIOptions+driver) ⇒ <code>string</code>
    * [.options](#APIOptions+options) ⇒ <code>Object</code>

Constructor
<a name="new_APIOptions_new"></a>

### new APIOptions(name, driver, options)
Create new APIOptions


| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The API name |
| driver | <code>string</code> | The library to use in API.supportedDrivers |
| options | <code>Object</code> | An object that the driver needs to configure itself |

<a name="APIOptions+name"></a>

### apiOptions.name ⇒ <code>string</code>
API name

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>APIOptions</code>](#APIOptions)  
<span class="color-beige"><strong>Returns:</strong></span> <code>string</code> - API name  
<span class="color-beige"><strong>Category:</strong></span> properties  
<a name="APIOptions+driver"></a>

### apiOptions.driver ⇒ <code>string</code>
API driver name

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>APIOptions</code>](#APIOptions)  
<span class="color-beige"><strong>Returns:</strong></span> <code>string</code> - API driver name  
<span class="color-beige"><strong>Category:</strong></span> properties  
<a name="APIOptions+options"></a>

### apiOptions.options ⇒ <code>Object</code>
Driver options

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>APIOptions</code>](#APIOptions)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Object</code> - The driver options  
<span class="color-beige"><strong>Category:</strong></span> properties  

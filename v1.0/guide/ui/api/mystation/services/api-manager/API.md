---
title: API class
headline: "MyStation API class"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation API class

<a name="API"></a>

## API
MyStation API Manager: API class```javascript// Importimport API from '#mystation/services/api-manager/API'```

<span class="color-beige"><strong>Kind:</strong></span> global class  

<span class="color-beige"><strong>Schema:</strong></span>

* [API](#API)
    * [new API(apiOptions)](#new_API_new)
    * _instance_
        * _methods_
            * [.setDriver(name, options)](#API+setDriver) ⇒ <code>void</code>
            * [.mapDriverMethods(methods)](#API+mapDriverMethods) ⇒ <code>void</code>
            * [.request(method, url, [options])](#API+request) ⇒ <code>Promise</code>
            * [.initAxios(options)](#API+initAxios) ⇒ <code>void</code>
        * _properties_
            * [.name](#API+name) ⇒ <code>string</code>
            * [.driver](#API+driver) ⇒ <code>Object</code>
    * _static_
        * [.supportedDrivers](#API.supportedDrivers) : <code>Array</code>

Constructor
<a name="new_API_new"></a>

### new API(apiOptions)
Create a new APISee also [APIOptions class](APIOptions)


| Param | Type | Description |
| --- | --- | --- |
| apiOptions | <code>APIOptions</code> | An APIOptions object |
| apiOptions.name | <code>string</code> | Name of API |
| apiOptions.driver | <code>string</code> | Driver API |
| apiOptions.options | <code>Object</code> | API options |

<a name="API+setDriver"></a>

### api.setDriver(name, options) ⇒ <code>void</code>
Set the driver for the API

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>API</code>](#API)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The driver name |
| options | <code>Object</code> | The driver options/configuration |

<a name="API+mapDriverMethods"></a>

### api.mapDriverMethods(methods) ⇒ <code>void</code>
For each HTTP verb, assign driver method:```javascript{  GET: methods.GET, // this.request('GET', <url>, [<options>])  POST: methods.POST, // this.request('POST', <url>, [<options>])  PUT: methods.PUT, // this.request('PUT', <url>, [<options>])  DELETE: methods.DELETE, // this.request('DELETE', <url>, [<options>])  OPTIONS: methods.OPTIONS // this.request('OPTIONS', <url>, [<options>])}```

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>API</code>](#API)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| methods | <code>Object</code> | A methods object |

<a name="API+request"></a>

### api.request(method, url, [options]) ⇒ <code>Promise</code>
Main request method

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>API</code>](#API)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Promise</code> - The driver Promise response  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| method | <code>string</code> |  | The method name (ex: GET, POST...) |
| url | <code>string</code> |  | The URL to request, without baseURL (Ex: /users) |
| [options] | <code>Object</code> | <code>{}</code> | The specific options for the driver |

<a name="API+initAxios"></a>

### api.initAxios(options) ⇒ <code>void</code>
Assign an axios instance to this.driver

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>API</code>](#API)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| options | <code>AxiosRequestConfig</code> | A AxiosRequestConfig object See axios documentation to see configuration options |

<a name="API+name"></a>

### api.name ⇒ <code>string</code>
API name

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>API</code>](#API)  
<span class="color-beige"><strong>Returns:</strong></span> <code>string</code> - The API name  
<span class="color-beige"><strong>Category:</strong></span> properties  
<a name="API+driver"></a>

### api.driver ⇒ <code>Object</code>
Get the API driver

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>API</code>](#API)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Object</code> - The driver  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  
<a name="API.supportedDrivers"></a>

### API.supportedDrivers : <code>Array</code>
Only 'axios' is supported as a driver for the moment

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>API</code>](#API)  
<span class="color-beige"><strong>Read only:</strong></span> true  

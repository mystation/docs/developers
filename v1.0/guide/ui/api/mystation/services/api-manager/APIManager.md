---
title: APIManager class
headline: "MyStation APIManager class"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation APIManager class

<a name="APIManager"></a>

## APIManager ⇐ <code>ServiceAsPlugin</code>
MyStation API Manager: APIManager class.See also [ServiceAsPlugin](../ServiceAsPlugin)```javascript// Importimport APIManager from '#mystation/services/api-manager/APIManager'```

<span class="color-beige"><strong>Kind:</strong></span> global class  
**Extends**: <code>ServiceAsPlugin</code>  

<span class="color-beige"><strong>Schema:</strong></span>

* [APIManager](#APIManager) ⇐ <code>ServiceAsPlugin</code>
    * [new APIManager(APIs)](#new_APIManager_new)
    * _methods_
        * [.add(api)](#APIManager+add) ⇒ <code>API</code>
        * [.use(apiName)](#APIManager+use) ⇒ <code>API</code>
    * _properties_
        * [.APIs](#APIManager+APIs) ⇒ <code>Array.&lt;API&gt;</code>

Constructor
<a name="new_APIManager_new"></a>

### new APIManager(APIs)
Create a new API ManagerSee also [API class](API)


| Param | Type | Description |
| --- | --- | --- |
| APIs | <code>Array.&lt;API&gt;</code> | An array of API |

<a name="APIManager+add"></a>

### apiManager.add(api) ⇒ <code>API</code>
Add an API to manager anytime

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>APIManager</code>](#APIManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>API</code> - Return this.use(<api.name>)  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| api | <code>API</code> | The API to add to manager |

<a name="APIManager+use"></a>

### apiManager.use(apiName) ⇒ <code>API</code>
Use a specific API that was loaded by the manager

<span class="color-beige"><strong>Kind:</strong></span> instance method of [<code>APIManager</code>](#APIManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>API</code> - Return the API  
<span class="color-beige"><strong>Category:</strong></span> methods  

| Param | Type | Description |
| --- | --- | --- |
| apiName | <code>string</code> | The API name to target |

<a name="APIManager+APIs"></a>

### apiManager.APIs ⇒ <code>Array.&lt;API&gt;</code>
Registered APIs

<span class="color-beige"><strong>Kind:</strong></span> instance property of [<code>APIManager</code>](#APIManager)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Array.&lt;API&gt;</code>  
<span class="color-beige"><strong>Category:</strong></span> properties  
<span class="color-beige"><strong>Read only:</strong></span> true  

---
title: APIManager
headline: "MyStation service: APIManager"
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation service: APIManager

<a name="module_api-manager"></a>

## api-manager
MyStation service: APIManager```javascript// Importimport APIManager from '#mystation/services/api-manager'```

<a name="exp_module_api-manager--module.exports"></a>

### module.exports : <code>APIManager</code> ⏏
APIManager service initialized with default APIs (see configuration).See also [APIManager class](APIManager)

<span class="color-beige"><strong>Kind:</strong></span> Exported constant  

::: syntax APIManager

Access to APIManager instance
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.$APIManager
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  this.$APIManager
  __MYSTATION__.$APIManager
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.$APIManager
  ```
:::


---
title: ui
headline: "MyStation module: ui"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation module: ui

<a name="module_ui"></a>

## ui
MyStation module: ui```javascript// Importimport ui from '#mystation/modules/ui'```


<span class="color-beige"><strong>Schema:</strong></span>

* [ui](#module_ui)
    * [module.exports](#exp_module_ui--module.exports) : <code>Object</code> ⏏
        * [.display(name)](#module_ui--module.exports.display) ⇒ <code>void</code>

<a name="exp_module_ui--module.exports"></a>

### module.exports : <code>Object</code> ⏏
ui module

<span class="color-beige"><strong>Kind:</strong></span> Exported constant  

::: syntax ui

Access to ui module
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.ui
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  __MYSTATION__.ui
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.ui
  ```
:::

<a name="module_ui--module.exports.display"></a>

#### ui.display(name) ⇒ <code>void</code>
display method: shortcut for Store.actions.switchMainframe function

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>module.exports</code>](#exp_module_ui--module.exports)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The component name to display |


::: syntax ui.display

Access to ui.display method, where `<name>` is the component name
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.ui.display(<name>)
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  __MYSTATION__.ui.display(<name>)
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.ui.display(<name>)
  ```
:::


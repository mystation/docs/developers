---
title: log
headline: "MyStation module: log"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation module: log

<a name="module_log"></a>

## log
MyStation module: log```javascript// Importimport log from '#mystation/modules/log'```

<a name="exp_module_log--module.exports"></a>

### module.exports() ⇒ <code>void</code> ⏏
log method: shortcut for Logger.push()

<span class="color-beige"><strong>Kind:</strong></span> Exported function  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  

::: syntax log

Access to log method, where `<args>` are Logger.push parameters
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.log(...<args>)
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  __MYSTATION__.log(...<args>)
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.log(...<args>)
  ```
:::


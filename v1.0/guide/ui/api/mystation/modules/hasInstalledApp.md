---
title: hasInstalledApp
headline: "MyStation module: hasInstalledApp"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation module: hasInstalledApp

<a name="module_hasInstalledApp"></a>

## hasInstalledApp
MyStation module: hasInstalledApp```javascript// Importimport hasInstalledApp from '#mystation/modules/hasInstalledApp'```

<a name="exp_module_hasInstalledApp--module.exports"></a>

### module.exports(appName) ⇒ <code>App</code> \| <code>boolean</code> ⏏
hasInstalledApp method: shortcut for store getter 'Apps/getApp'

<span class="color-beige"><strong>Kind:</strong></span> Exported function  
<span class="color-beige"><strong>Returns:</strong></span> <code>App</code> \| <code>boolean</code> - - See store/Apps  

| Param | Type | Description |
| --- | --- | --- |
| appName | <code>string</code> | The app unique name |


::: syntax hasInstalledApp

Access to hasInstalledApp method, where `<appName>` is the app name
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.hasInstalledApp(<appName>)
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  __MYSTATION__.hasInstalledApp(<appName>)
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.hasInstalledApp(<appName>)
  ```
:::


---
title: emit
headline: "MyStation module: emit"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation module: emit

<a name="module_emit"></a>

## emit
MyStation module: emit```javascript// Importimport emit from '#mystation/modules/emit'```

<a name="exp_module_emit--module.exports"></a>

### module.exports() ⇒ <code>boolean</code> ⏏
emit method: shortcut for EventBus $emit method (same paremeters).

<span class="color-beige"><strong>Kind:</strong></span> Exported function  
<span class="color-beige"><strong>Returns:</strong></span> <code>boolean</code> - See store Users module  

::: syntax emit

Access to emit method, where `<args>` are EventBus.$emit parameters
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.emit(...<args>)
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  __MYSTATION__.emit(...<args>)
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.emit(...<args>)
  ```
:::


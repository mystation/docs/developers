---
title: isAppInitialized
headline: "MyStation module: isAppInitialized"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation module: isAppInitialized

<a name="module_isAppInitialized"></a>

## isAppInitialized
MyStation module: isAppInitialized```javascript// Importimport isAppInitialized from '#mystation/modules/isAppInitialized'```

<a name="exp_module_isAppInitialized--module.exports"></a>

### module.exports(appName) ⇒ <code>boolean</code> ⏏
isAppInitialized method: shortcut for store current user apps => is_initialized

<span class="color-beige"><strong>Kind:</strong></span> Exported function  
<span class="color-beige"><strong>Returns:</strong></span> <code>boolean</code>  

| Param | Type |
| --- | --- |
| appName | <code>string</code> | 


::: syntax isAppInitialized

Access to isAppInitialized method, where `<appName>` is the app name
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.isAppInitialized(<appName>)
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  __MYSTATION__.isAppInitialized(<appName>)
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.isAppInitialized(<appName>)
  ```
:::


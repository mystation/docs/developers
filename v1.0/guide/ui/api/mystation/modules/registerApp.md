---
title: registerApp
headline: "MyStation module: registerApp"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation module: registerApp

<a name="module_registerApp"></a>

## registerApp
MyStation module: registerApp```javascript// Importimport registerApp from '#mystation/modules/registerApp'```

<a name="exp_module_registerApp--module.exports"></a>

### module.exports() ⇒ <code>boolean</code> ⏏
registerApp methods: shortcut for AppManager.register() function

<span class="color-beige"><strong>Kind:</strong></span> Exported function  
<span class="color-beige"><strong>Returns:</strong></span> <code>boolean</code> - See AppManager  

::: syntax registerApp

Access to registerApp method, where `<args>` are AppManager.register parameters
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.registerApp(...<args>)
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  __MYSTATION__.registerApp(...<args>)
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.registerApp(...<args>)
  ```
:::


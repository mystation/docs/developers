---
title: on
headline: "MyStation module: on"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation module: on

<a name="module_on"></a>

## on
MyStation module: on```javascript// Importimport on from '#mystation/modules/on'```

<a name="exp_module_on--module.exports"></a>

### module.exports() ⇒ <code>void</code> ⏏
on method: shortcut for EventBus $on method

<span class="color-beige"><strong>Kind:</strong></span> Exported function  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code>  

::: syntax on

Access to on method, where `<args>` are EventBus.$on parameters
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.on(...<args>)
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  __MYSTATION__.on(...<args>)
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.on(...<args>)
  ```
:::


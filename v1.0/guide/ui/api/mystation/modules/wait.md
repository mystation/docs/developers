---
title: wait
headline: "MyStation module: wait"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation module: wait

<a name="module_wait"></a>

## wait
MyStation module: wait```javascript// Importimport wait from '#mystation/modules/wait'```

<a name="exp_module_wait--module.exports"></a>

### module.exports() ⇒ <code>void</code> ⏏
wait method: shortcut for LoadingManager.wait()

<span class="color-beige"><strong>Kind:</strong></span> Exported function  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code> - See LoadingManager  

::: syntax wait

Access to wait method, where `<args>` are LoadingManager.wait parameters
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.wait(...<args>)
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  __MYSTATION__.wait(...<args>)
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.wait(...<args>)
  ```
:::


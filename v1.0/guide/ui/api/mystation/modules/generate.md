---
title: generate
headline: "MyStation module: generate"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation module: generate

<a name="module_generate"></a>

## generate
MyStation module: generate```javascript// Importimport generate from '#mystation/modules/generate'```


<span class="color-beige"><strong>Schema:</strong></span>

* [generate](#module_generate)
    * [module.exports](#exp_module_generate--module.exports) : <code>Object</code> ⏏
        * [.appModuleResourceMappers()](#module_generate--module.exports.appModuleResourceMappers) ⇒ <code>Object</code> \| <code>boolean</code>

<a name="exp_module_generate--module.exports"></a>

### module.exports : <code>Object</code> ⏏
generate module

<span class="color-beige"><strong>Kind:</strong></span> Exported constant  

::: syntax generate

Access to generate module
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.generate
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  __MYSTATION__.generate
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.generate
  ```
:::

<a name="module_generate--module.exports.appModuleResourceMappers"></a>

#### generate.appModuleResourceMappers() ⇒ <code>Object</code> \| <code>boolean</code>
appModuleResourceMappers methods: shortcut for AppManager.generateAppModuleResourceMappers() function (same parameters).

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>module.exports</code>](#exp_module_generate--module.exports)  
<span class="color-beige"><strong>Returns:</strong></span> <code>Object</code> \| <code>boolean</code> - See AppManager  

::: syntax generate.appModuleResourceMappers

Access to generate.appModuleResourceMappers method, where `<args>` are AppManager.generateAppModuleResourceMappers parameters
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.generate.appModuleResourceMappers(...<args>)
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  __MYSTATION__.generate.appModuleResourceMappers(...<args>)
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.generate.appModuleResourceMappers(...<args>)
  ```
:::


---
title: initializeApp
headline: "MyStation module: initializeApp"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation module: initializeApp

<a name="module_initializeApp"></a>

## initializeApp
MyStation module: initializeApp```javascript// Importimport initializeApp from '#mystation/modules/initializeApp'```

<a name="exp_module_initializeApp--module.exports"></a>

### module.exports(appName) ⇒ <code>Object</code> \| <code>boolean</code> ⏏
initializeApp method: shortcut for store action 'Users/initializeApp'

<span class="color-beige"><strong>Kind:</strong></span> Exported function  
<span class="color-beige"><strong>Returns:</strong></span> <code>Object</code> \| <code>boolean</code> - See store/Users  

| Param | Type | Description |
| --- | --- | --- |
| appName | <code>string</code> | The app unique name |


::: syntax initializeApp

Access to initializeApp method, where `<appName>` is the app name
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.initializeApp(<appName>)
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  __MYSTATION__.initializeApp(<appName>)
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.initializeApp(<appName>)
  ```
:::


---
title: auth
headline: "MyStation module: auth"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation module: auth

<a name="module_auth"></a>

## auth
MyStation module: auth```javascript// Importimport auth from '#mystation/modules/auth'```


<span class="color-beige"><strong>Schema:</strong></span>

* [auth](#module_auth)
    * [module.exports](#exp_module_auth--module.exports) : <code>Object</code> ⏏
        * [.check()](#module_auth--module.exports.check) ⇒ <code>boolean</code>
        * [.isLoading()](#module_auth--module.exports.isLoading) ⇒ <code>boolean</code>
        * [.login()](#module_auth--module.exports.login) ⇒ <code>boolean</code> \| <code>Error</code>
        * [.logout()](#module_auth--module.exports.logout) ⇒ <code>void</code>
        * [.user()](#module_auth--module.exports.user) ⇒ <code>User</code>

<a name="exp_module_auth--module.exports"></a>

### module.exports : <code>Object</code> ⏏
auth module

<span class="color-beige"><strong>Kind:</strong></span> Exported constant  

::: syntax auth

Access to auth module
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.auth
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  __MYSTATION__.auth
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.auth
  ```
:::

<a name="module_auth--module.exports.check"></a>

#### auth.check() ⇒ <code>boolean</code>
check method: shortcut for Store.Users.actions.checkAuthenticated action

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>module.exports</code>](#exp_module_auth--module.exports)  
<span class="color-beige"><strong>Returns:</strong></span> <code>boolean</code> - See store Users module  

::: syntax auth.check

Access to auth.check method
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.auth.check()
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  __MYSTATION__.auth.check()
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.auth.check()
  ```
:::

<a name="module_auth--module.exports.isLoading"></a>

#### auth.isLoading() ⇒ <code>boolean</code>
isLoading method: shortcut for Store.state.Users.currentUserLoading

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>module.exports</code>](#exp_module_auth--module.exports)  
<span class="color-beige"><strong>Returns:</strong></span> <code>boolean</code> - See store Users module  

::: syntax auth.isLoading

Access to auth.isLoading method
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.auth.isLoading()
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  __MYSTATION__.auth.isLoading()
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.auth.isLoading()
  ```
:::

<a name="module_auth--module.exports.login"></a>

#### auth.login() ⇒ <code>boolean</code> \| <code>Error</code>
login method: shortcut for Store.Users.actions.login (same parameters).

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>module.exports</code>](#exp_module_auth--module.exports)  
<span class="color-beige"><strong>Returns:</strong></span> <code>boolean</code> \| <code>Error</code> - See store Users module  

::: syntax auth.login

Access to auth.login method, where `<args>` are Store.Users.actions.login parameters
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.auth.login(...<args>)
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  __MYSTATION__.auth.login(...<args>)
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.auth.login(...<args>)
  ```
:::

<a name="module_auth--module.exports.logout"></a>

#### auth.logout() ⇒ <code>void</code>
logout method: shortcut for Store.Users.actions.logout

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>module.exports</code>](#exp_module_auth--module.exports)  
<span class="color-beige"><strong>Returns:</strong></span> <code>void</code> - See store Users module  

::: syntax auth.logout

Access to auth.logout method
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.auth.logout()
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  __MYSTATION__.auth.logout()
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.auth.logout()
  ```
:::

<a name="module_auth--module.exports.user"></a>

#### auth.user() ⇒ <code>User</code>
user method: shortcut for Store.Users.state.currentUser object

<span class="color-beige"><strong>Kind:</strong></span> static method of [<code>module.exports</code>](#exp_module_auth--module.exports)  
<span class="color-beige"><strong>Returns:</strong></span> <code>User</code> - See store Users module  

::: syntax auth.user

Access to auth.user method
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.auth.user()
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  __MYSTATION__.auth.user()
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.auth.user()
  ```
:::


---
title: Header component
headline: "MyStation component: Header"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation component: Header

# Header

Header component


----

<h2>Summary</h2>

[[toc]]

## Methods

### formatDate () -> `string`
 Format date for dashboard title



<span class="color-beige">Returns:</span> (string)
 
### myStationClick (event: `Object`) -> `boolean`
 On MyStation button click


<span class="color-beige">Params:</span>
| name | type | description
|:-|:-|:-|
|event|`Object`|The event

<span class="color-beige">Returns:</span> (boolean)
 
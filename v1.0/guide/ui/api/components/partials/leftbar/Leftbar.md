---
title: Leftbar component
headline: "MyStation component: Leftbar"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation component: Leftbar

# Leftbar

Leftbar component

Child components:
- LeftbarAppIcon ([components/elements/icons/LeftbarAppIcon](../../elements/icons/LeftbarAppIcon))


----

<h2>Summary</h2>

[[toc]]

## Methods

### appStatus (appName: `string`) -> `boolean`
 Get AppManager app component status


<span class="color-beige">Params:</span>
| name | type | description
|:-|:-|:-|
|appName|`string`|The app unique name

<span class="color-beige">Returns:</span> (boolean)
 
### isCurrentApp (app: `App`) -> `boolean`
 Detect current app


<span class="color-beige">Params:</span>
| name | type | description
|:-|:-|:-|
|app|`App`|The app

<span class="color-beige">Returns:</span> (boolean)
 
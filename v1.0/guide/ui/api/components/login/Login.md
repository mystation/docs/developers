---
title: Login component
headline: "MyStation component: Login"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation component: Login

# Login

Login component


----

<h2>Summary</h2>

[[toc]]

## Methods

### handleOk (evt: `Object`) -> `void`
 Handle click on 'login' submit button.

Call 'auth' module login method ([auth module](../../mystation/modules/auth)).
If success, switch to main interface. Else, show error in modal.


<span class="color-beige">Params:</span>
| name | type | description
|:-|:-|:-|
|evt|`Object`|The event payload

<span class="color-beige">Returns:</span> (void)
 
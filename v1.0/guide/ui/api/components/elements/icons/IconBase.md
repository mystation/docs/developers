---
title: IconBase
---
# IconBase

IconBase component


----

<h2>Summary</h2>

[[toc]]

## Props

### iconName (`string`)
undefined

|type|default|
|:-|:-|
|`string`|'base'|

### width (`number|string`)
undefined

|type|default|
|:-|:-|
|`number|string`|28|

### height (`number|string`)
undefined

|type|default|
|:-|:-|
|`number|string`|28|

### iconColor (`string`)
undefined

|type|default|
|:-|:-|
|`string`|'currentColor'|



## Slots

### default
undefined


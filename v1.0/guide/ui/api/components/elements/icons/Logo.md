---
title: Logo
---
# Logo




----

<h2>Summary</h2>

[[toc]]

## Props

### width (`number`)
undefined

|type|default|
|:-|:-|
|`number`|240|

### height (`number`)
undefined

|type|default|
|:-|:-|
|`number`|240|

### status (`string`)
undefined

|type|default|
|:-|:-|
|`string`|''|

### mColor (`string`)
undefined

|type|default|
|:-|:-|
|`string`|__THEME_CUSTOM_GREEN_COLOR__|

### yColor (`string`)
undefined

|type|default|
|:-|:-|
|`string`|__THEME_CUSTOM_BLUE_COLOR__|

### sColor (`string`)
undefined

|type|default|
|:-|:-|
|`string`|__THEME_CUSTOM_RED_COLOR__|



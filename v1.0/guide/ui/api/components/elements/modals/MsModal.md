---
title: MsModal
---
# MsModal




----

<h2>Summary</h2>

[[toc]]

## Props

### vModel (`boolean`)
undefined

|type|default|
|:-|:-|
|`boolean`|false|

### id (`string`)
undefined

|type|default|
|:-|:-|
|`string`|-|

### title (`string`)
undefined

|type|default|
|:-|:-|
|`string`|-|

### size (`string`)
undefined

|type|default|
|:-|:-|
|`string`|'md'|

### centered (`boolean`)
undefined

|type|default|
|:-|:-|
|`boolean`|true|

### noCloseOnBackdrop (`boolean`)
undefined

|type|default|
|:-|:-|
|`boolean`|true|

### hideButtons (`boolean`)
undefined

|type|default|
|:-|:-|
|`boolean`|false|

### ok (`func`)
undefined

|type|default|
|:-|:-|
|`func`|function() {
  return true
}|

### okTitle (`string`)
undefined

|type|default|
|:-|:-|
|`string`|null|

### okVariant (`string`)
undefined

|type|default|
|:-|:-|
|`string`|'action'|

### cancel (`func`)
undefined

|type|default|
|:-|:-|
|`func`|null|

### cancelTitle (`string`)
undefined

|type|default|
|:-|:-|
|`string`|null|



## Slots

### title
undefined
### default
undefined
### buttons
undefined


---
title: AppInstallModal
---
# AppInstallModal

AppInstallModal component


----

<h2>Summary</h2>

[[toc]]

## Props

### vModel (`boolean`)
undefined

|type|default|
|:-|:-|
|`boolean`|false|

### app (`object`)
undefined

|type|default|
|:-|:-|
|`object`|-|

### ok (`func`)
undefined

|type|default|
|:-|:-|
|`func`|-|

### cancel (`func`)
undefined

|type|default|
|:-|:-|
|`func`|-|

### hideButtons (`boolean`)
undefined

|type|default|
|:-|:-|
|`boolean`|-|



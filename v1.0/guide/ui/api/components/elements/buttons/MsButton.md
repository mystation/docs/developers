---
title: MsButton
---
# MsButton

> Global registered component: `<MsButton></MsButton>`

MsButton component


----

<h2>Summary</h2>

[[toc]]

## Props

### variant (`string`)
Bootstrap variant for the button

|type|default|
|:-|:-|
|`string`|'secondary'|

### textAlign (`string`)
Define text alignment

|type|default|
|:-|:-|
|`string`|'center'|

### cornerEffects (`boolean`)
Define corner effects

|type|default|
|:-|:-|
|`boolean`|true|

### fluid (`boolean`)
Define if button is full width

|type|default|
|:-|:-|
|`boolean`|false|



## Slots

### default
Button content


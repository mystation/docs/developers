---
title: App component
headline: "MyStation component: App"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation component: App

# App

App main component

Child components:
- Login ([components/login/Login](login/Login))
- Header ([components/partials/header/Header](partials/header/Header))
- Leftbar ([components/partials/leftbar/Leftbar](partials/leftbar/Leftbar))
- Rightbar ([components/partials/rightbar/Rightbar](partials/rightbar/Rightbar))
- MainFrame ([components/mainframe/MainFrame](mainframe/MainFrame))
- MainLoading ([components/elements/loadings/MainLoading](elements/loadings/MainLoading))
- GlobalErrorModal ([components/elements/modals/GlobalErrorModal](elements/modals/GlobalErrorModal))


----

---
title: Dashboard component
headline: "MyStation component: Dashboard"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation component: Dashboard

# Dashboard

Dashboard component


----

<h2>Summary</h2>

[[toc]]

## Methods

### pancakeStatus (pancake: `Object`) -> `boolean`
 Get AppManager pancake component status


<span class="color-beige">Params:</span>
| name | type | description
|:-|:-|:-|
|pancake|`Object`|The pancake object

<span class="color-beige">Returns:</span> (boolean)
 
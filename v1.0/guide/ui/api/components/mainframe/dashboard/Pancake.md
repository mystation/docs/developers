---
title: Pancake component
headline: "MyStation component: Pancake"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation component: Pancake

# Pancake

Pancake component


----

<h2>Summary</h2>

[[toc]]

## Props

### appName (`string`)
The app unique name to retrieve the app from store

|type|default|
|:-|:-|
|`string`|''|

### titleColor (`string`)
Make possible to modify the pancake title color.

Default to 'default': (app color)

|type|default|
|:-|:-|
|`string`|'default'|



## Methods

### updateMask () -> `void`
 Update css clip-path when pancake container DOM element size changes.

In mounted hook, a ResizeObserver is created to observe the DOM element.



<span class="color-beige">Returns:</span> (void)
 
## Slots

### title
undefined
### content
undefined


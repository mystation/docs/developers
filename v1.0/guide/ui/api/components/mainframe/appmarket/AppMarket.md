---
title: AppMarket component
headline: "MyStation component: AppMarket"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation component: AppMarket

# AppMarket

AppMarket component.

Child components:
- AppInstallModal ([components/elements/modals/AppInstallModal](../../elements/modals/AppInstallModal))
- AppUninstallModal ([components/elements/modals/AppUninstallModal](../../elements/modals/AppUninstallModal))


----

<h2>Summary</h2>

[[toc]]

## Methods

### install () -> `void`
 Install an app.

Call AppManager.installApp method



<span class="color-beige">Returns:</span> (void)
 
### confirmInstall (app: `MarketApp`) -> `void`
 Display modal to confirm app installation


<span class="color-beige">Params:</span>
| name | type | description
|:-|:-|:-|
|app|`MarketApp`|The app to install

<span class="color-beige">Returns:</span> (void)
 
### cancelInstall () -> `void`
 Hide app installation modal



<span class="color-beige">Returns:</span> (void)
 
### uninstall () -> `void`
 Uninstall an app

Call AppManager.uninstallApp method



<span class="color-beige">Returns:</span> (void)
 
### confirmUninstall (app: `MarketApp`) -> `void`
 Display modal to confirm app uninstall


<span class="color-beige">Params:</span>
| name | type | description
|:-|:-|:-|
|app|`MarketApp`|The app to uninstall

<span class="color-beige">Returns:</span> (void)
 
### cancelUninstall () -> `void`
 Hide app uninstall modal



<span class="color-beige">Returns:</span> (void)
 
---
title: AppContainer component
headline: "MyStation component: AppContainer"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation component: AppContainer

# AppContainer

> Global registered component: `<AppContainer></AppContainer>`

AppContainer component.

Generic component for app components.

Child components:
- dynamic named component for each 'tab' in this.tabs (see component props).


----

<h2>Summary</h2>

[[toc]]

## Props

### app (`string`)
The app unique name

|type|default|
|:-|:-|
|`string`|''|

### tabs (`array`)
Tabs

Object array like:
```javascript
[
  {
    title: String,
    component: Object (Vue component),
    click: Function
  },
  ...
]
```

|type|default|
|:-|:-|
|`array`|[]|

### defaultTab (`string|number`)
Index of default tab to display when component is mounted (default 0: first tab).

|type|default|
|:-|:-|
|`string|number`|function () {
  return 0
}|



## Methods

### tabClickFunction (tab: `Object`,index: `number`) -> `void`
 Handle click on a tab.

Call tab.click if it's defined (function).


<span class="color-beige">Params:</span>
| name | type | description
|:-|:-|:-|
|tab|`Object`|The tab object
|index|`number`|The tab index

<span class="color-beige">Returns:</span> (void)
 
### componentName (index: `number`) -> `string`
 Return tab component name based on index.

Each tab is registered in beforeCreate hook with a formatted name like:
`<appName>-tab-component-<index>` where `<appName>` is the app unique name and
`<index>` the tab index in this.tabs


<span class="color-beige">Params:</span>
| name | type | description
|:-|:-|:-|
|index|`number`|The tab index

<span class="color-beige">Returns:</span> (string)
 
## Slots

### content
Slot 'content' for app content


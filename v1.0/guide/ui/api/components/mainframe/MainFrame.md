---
title: MainFrame component
headline: "MyStation component: MainFrame"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation component: MainFrame

# MainFrame

MainFrame component.

Container for: dashboard, settings, app market or an app component.

Child components:
- Dashboard ([components/mainframe/dashboard/Dashboard](mainframe/dashboard/Dashboard))
- Settings ([components/mainframe/settings/Header](mainframe/settings/Settings))
- AppMarket ([components/mainframe/appmarket/Leftbar](mainframe/appmarket/AppMarket))


----

---
title: I18n
headline: "MyStation Internationalization (I18n)"
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# MyStation Internationalization (I18n)

<a name="module_i18n"></a>

## i18n
MyStation internationalization: VueI18n (see: [https://kazupon.github.io/vue-i18n/](https://kazupon.github.io/vue-i18n/))```javascript// Importimport I18n from '@/i18n'```


<span class="color-beige"><strong>Schema:</strong></span>

* [i18n](#module_i18n)
    * [I18n](#exp_module_i18n--I18n) : <code>VueI18n</code> ⏏
        * [.locale](#module_i18n--I18n.locale) : <code>string</code>
        * [.fallbackLocale](#module_i18n--I18n.fallbackLocale) : <code>string</code>
        * [.messages](#module_i18n--I18n.messages) : <code>Object</code>

<a name="exp_module_i18n--I18n"></a>

### I18n : <code>VueI18n</code> ⏏
I18n

<span class="color-beige"><strong>Kind:</strong></span> Exported constant  

::: syntax I18n

Access to i18n instance
- <span class="color-green">{ _root context_ }</span>
  ```javascript
  this.$i18n
  ```
- <span class="color-green">{ _apps context_ }</span>
  ```javascript
  this.$i18n
  __MYSTATION__.$i18n
  ```
- <span class="color-green">{ _any context_ }</span>
  ```javascript
  window.__MYSTATION_VUE_INSTANCE__.$i18n
  ```
:::

<span class="color-beige"><strong>Example:</strong></span>  
```js
// Change localethis.$i18n.locale = 'en'// Translatethis.$t('path.to.message') // in a component// Learn more about vue-i18n at https://kazupon.github.io/vue-i18n/
```
<a name="module_i18n--I18n.locale"></a>

#### I18n.locale : <code>string</code>
VueI18n option: locale. See: [Configuration](/guide/ui/internal-flow/#configuration) for default value

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>I18n</code>](#exp_module_i18n--I18n)  
<span class="color-beige"><strong>Default:</strong></span> <code>&quot;fr&quot;</code>  
<a name="module_i18n--I18n.fallbackLocale"></a>

#### I18n.fallbackLocale : <code>string</code>
VueI18n option: fallbackLocale. See: [Configuration](/guide/ui/internal-flow/#configuration) for default value

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>I18n</code>](#exp_module_i18n--I18n)  
<span class="color-beige"><strong>Default:</strong></span> <code>&quot;en&quot;</code>  
<a name="module_i18n--I18n.messages"></a>

#### I18n.messages : <code>Object</code>
VueI18n option: messages.The translations for each app are registered asynchronouslyin `<locale>.apps.<app_name>` property.See [AppManager service documentation](/guide/ui/api/mystation/services/AppManager).

<span class="color-beige"><strong>Kind:</strong></span> static property of [<code>I18n</code>](#exp_module_i18n--I18n)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| en | <code>Object</code> | MyStation English translations |
| fr | <code>Object</code> | MyStation French translations |


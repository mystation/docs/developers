const config = require('./guide/ui/api/config.js')

const apiSidebar = config.sidebarTree()['/output/']

module.exports = {
  // Guide sidebar
  '/guide/': [
    {
      title: 'Introduction',
      path: '/guide/introduction/'
    },
    {
      title: 'Get started',
      path: '/guide/get-started/'
    },
    {
      title: 'User interface',
      path: '/guide/ui/',
      children: [
        {
          title: 'Internal flow',
          path: '/guide/ui/internal-flow/'
        },
        {
          title: 'Global API',
          path: '/guide/ui/api/',
          children: apiSidebar
      //     children: [
      //       {
      //         title: 'Components',
      //         children: [
      //           '/guide/ui/api/components/elements/collapses/MsCollapse',
      //           '/guide/ui/api/components/elements/icons/IconBase',
      //           '/guide/ui/api/components/elements/icons/IconBasicLoad',
      //           '/guide/ui/api/components/elements/loadings/MainLoading',
      //           '/guide/ui/api/components/elements/modals/AppInstallModal',
      //           '/guide/ui/api/components/elements/modals/AppUninstallModal',
      //           '/guide/ui/api/components/elements/modals/GlobalErrorModal',
      //           '/guide/ui/api/components/elements/modals/MsModal',
      //           '/guide/ui/api/components/login/Login',
      //           '/guide/ui/api/components/mainframe/appmarket/AppMarket',
      //           '/guide/ui/api/components/mainframe/dashboard/Dashboard',
      //           '/guide/ui/api/components/mainframe/dashboard/Pancake',
      //           '/guide/ui/api/components/mainframe/MainFrame',
      //           '/guide/ui/api/components/partials/header/Header',
      //           '/guide/ui/api/components/partials/leftbar/LeftBar',
      //           '/guide/ui/api/components/partials/rightbar/RightBar'
      //         ]
      //       },
      //       {
      //         title: 'I18n',
      //         path: '/guide/ui/api/i18n/'
      //       },
      //       {
      //         title: 'MyStation',
      //         path: '/guide/ui/api/mystation/',
      //         children: [
      //           {
      //             title: 'Models',
      //             children: [
      //               '/guide/ui/api/mystation/models/App',
      //               '/guide/ui/api/mystation/models/MarketApp',
      //               '/guide/ui/api/mystation/models/User'
      //             ]
      //           },
      //           {
      //             title: 'Modules',
      //             children: [
      //               '/guide/ui/api/mystation/modules/auth',
      //               '/guide/ui/api/mystation/modules/emit',
      //               '/guide/ui/api/mystation/modules/generate',
      //               '/guide/ui/api/mystation/modules/hasInstalledApp',
      //               '/guide/ui/api/mystation/modules/initializeApp',
      //               '/guide/ui/api/mystation/modules/isAppInitialized',
      //               '/guide/ui/api/mystation/modules/log',
      //               '/guide/ui/api/mystation/modules/on',
      //               '/guide/ui/api/mystation/modules/registerApp',
      //               '/guide/ui/api/mystation/modules/ui',
      //               '/guide/ui/api/mystation/modules/wait'
      //             ]
      //           },
      //           {
      //             title: 'Services',
      //             children: [
      //               {
      //                 title: 'APIManager',
      //                 path: '/guide/ui/api/mystation/services/api-manager/',
      //                 children: [
      //                   '/guide/ui/api/mystation/services/api-manager/API',
      //                   '/guide/ui/api/mystation/services/api-manager/APIManager',
      //                   '/guide/ui/api/mystation/services/api-manager/APIOptions'
      //                 ]
      //               },
      //               {
      //                 title: 'AppManager',
      //                 path: '/guide/ui/api/mystation/services/app-manager/',
      //                 children: [
      //                   '/guide/ui/api/mystation/services/app-manager/AppManager'
      //                 ]
      //               },
      //               {
      //                 title: 'ErrorManager',
      //                 path: '/guide/ui/api/mystation/services/error-manager/',
      //                 children: [
      //                   '/guide/ui/api/mystation/services/error-manager/ErrorManager'
      //                 ]
      //               },
      //               { 
      //                 title: 'EventBus',
      //                 path: '/guide/ui/api/mystation/services/event-bus/'
      //               },
      //               {
      //                 title: 'Helpers',
      //                 path: '/guide/ui/api/mystation/services/helpers/',
      //                 children: [
      //                   '/guide/ui/api/mystation/services/helpers/Helpers'
      //                 ]
      //               },
      //               {
      //                 title: 'LoadingManager',
      //                 path: '/guide/ui/api/mystation/services/loading-manager/',
      //                 children: [
      //                   '/guide/ui/api/mystation/services/loading-manager/Loading',
      //                   '/guide/ui/api/mystation/services/loading-manager/LoadingManager'
      //                 ]
      //               },
      //               {
      //                 title: 'Logger',
      //                 path: '/guide/ui/api/mystation/services/logger/',
      //                 children: [
      //                   '/guide/ui/api/mystation/services/logger/Logger'
      //                 ]
      //               },
      //               {
      //                 title: 'ServiceAsPlugin',
      //                 path: '/guide/ui/api/mystation/services/ServiceAsPlugin'
      //               }
      //             ]
      //           },
      //           '/guide/ui/api/mystation/Hook'
      //         ]
      //       },
      //       {
      //         title: 'Store',
      //         path: '/guide/ui/api/store/',
      //         children: [
      //           '/guide/ui/api/store/modules/Apps',
      //           '/guide/ui/api/store/modules/Users'
      //         ]
      //       }
      //     ]
        }
      ]
    },
    {
      title: 'Monitor',
      path: '/guide/monitor/'
    },
    {
      title: 'System',
      path: '/guide/system/',
      children: [
        {
          title: 'API Server',
          path: '/guide/system/api-server/'
        },
        {
          title: 'Database',
          path: '/guide/system/database/'
        },
        {
          title: 'Raspberry Pi',
          path: '/guide/system/raspberry-pi/'
        },
        {
          title: 'Processes',
          path: '/guide/system/processes/'
        }
      ]
    },
    {
      title: 'Apps',
      path: '/guide/apps/',
      children: [
        {
          title: 'Develop an app',
          path: '/guide/apps/develop/'
        },
        {
          title: 'Examples',
          path: '/guide/apps/examples/'
        }
      ]
    }
  ]
}
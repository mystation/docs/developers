module.exports = {
  // Guide sidebar
  '/fr/guide/': [
    {
      title: 'Introduction',
      path: '/fr/guide/introduction/'
    },
    {
      title: 'Démarrer',
      path: '/fr/guide/get-started/'
    },
    {
      title: 'Interface utilisateur',
      path: '/fr/guide/ui/',
      children: [
        {
          title: 'Fonctionnement interne',
          path: '/fr/guide/ui/internal-flow/'
        },
        {
          title: 'API globale (en)',
          path: '/guide/ui/api/'
        }
      ]
    },
    {
      title: 'Monitor',
      path: '/fr/guide/monitor/'
    },
    {
      title: 'Système',
      path: '/fr/guide/system/',
      children: [
        {
          title: 'Serveur API',
          path: '/fr/guide/system/api-server/'
        },
        {
          title: 'Base de données',
          path: '/fr/guide/system/database/'
        },
        {
          title: 'Raspberry Pi',
          path: '/fr/guide/system/raspberry-pi/'
        },
        {
          title: 'Processus enfants',
          path: '/fr/guide/system/processes/'
        }
      ]
    },
    {
      title: 'Apps',
      path: '/fr/guide/apps/',
      children: [
        {
          title: 'Développer une app',
          path: '/fr/guide/apps/develop/'
        },
        {
          title: 'Exemples',
          path: '/fr/guide/apps/examples/'
        }
      ]
    }
  ]
}
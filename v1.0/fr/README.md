---
home: true
heroImage: http://mystation.fr/static/img/logo.png
heroText: MyStation
tagline: for developers
actionText: Démarrer →
actionLink: /fr/guide/introduction/
features:
- title: Node.js
  details: Majeure utilisation de Javascript, langage prometteur, notamment grâce à Node.js
- title: Vue + Webpack
  details: Une interface utilisateur basée sur Vue.js, couplé au puissant Webpack
- title: Laravel
  details: Un serveur API développé avec le célèbre framework PHP Laravel
footer: GPL v3 Licensed | Copyright © 2019-present Hervé Perchec
---
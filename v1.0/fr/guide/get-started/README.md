---
next: /fr/guide/ui/
---

# Démarrer

MyStation est conçu pour être installé sur une carte mère Raspberry Pi, équipée de la dernière version de Raspberry Pi OS ([https://www.raspberrypi.org/downloads/raspberry-pi-os/](https://www.raspberrypi.org/downloads/raspberry-pi-os/)).
Toutes les spécifications sont disponibles dans la section [Système > Raspberry Pi](/fr/guide/system/raspberry-pi).

Veuillez consulter le groupe de dépôt de code de MyStation.
> [https://gitlab.com/mystation/](https://gitlab.com/mystation/)

Pour le développement:

```bash
# Cloner le dépôt
git clone https://gitlab.com/mystation/mystation.git
# Lancer l'initialisation
npm run init
# Prenez un café ;)
```

---
prev: /fr/guide/ui/
---

# Fonctionnement interne

[[toc]]

## Présentation

<mermaid class="mermaid-flowchart" filePath="/fr/guide/ui/internal-flow/mermaid/internal_flow.mmd"></mermaid>

## Instance de Vue


::: tip Vue.js framework
Consultez la documentation officielle de Vue.js: [https://vuejs.org/v2/guide/](https://vuejs.org/v2/guide/)
:::

L'interface de _MyStation_ étant basée sur le framework Vue.js, son système repose sur une instance racine de Vue.

1. 🧱 Création de l'instance de Vue à partir des propriétés principales de _MyStation_:
   
   - [internationalisation](#internationalisation) (i18n)
   - [store](#store) (vuex)
   - méthode [boot()](#initialisation)
   - chaque [service](#services) directement en tant que propriété d'instance

2. 🔒 Assignation à `window.__MYSTATION_VUE_INSTANCE__`

3. ⏳ Appel dynamique de la méthode `this.boot()` de manière _asynchrone_ lors du hook __'created'__ de l'instance de Vue. Plus de détails dans [la rubrique concernant l'initialisation](#initialisation)

> Certaines dépendances de _MyStation_ sont partagées et accessibles de manière globale:

```javascript
/* MyStation instance */
window.__MYSTATION_VUE_INSTANCE__
/* Shared dependencies */
// axios
window.lib.axios
// color
window.lib.color
// lodash
window.lib._.camelcase
window.lib._.capitalize
window.lib._.kebabcase
window.lib._.maxBy
window.lib._.minBy
window.lib._.snakecase
window.lib._.sortBy
window.lib._.uppercase
// vue
window.lib.Vue
```
Pour plus d'informations sur les versions, voir la liste des dépendances _MyStation_.

## Fonctionnement de MyStation

### Internationalisation

Les traductions de l'interface sont gérées avec le plugin [vue-i18n](https://kazupon.github.io/vue-i18n/) pour Vue.js.

> Les erreurs (messages pour l'utilisateur) sont traduites séparément pour une utilisation par le service [ErrorManager](/guide/ui/api/mystation/services/error-manager)

#### Langues supportées

> Les fichiers de traductions sont disponibles dans les sources au format JSON

| Langue   | locale | source |
| -        | -      | -      |
| Anglais  | en     | [en.json](https://gitlab.com/mystation/mystation-front/-/blob/master/src/i18n/en.js) |
| Français | fr     | [fr.json](https://gitlab.com/mystation/mystation-front/-/blob/master/src/i18n/fr.js) |

::: see-also
- [Global API → I18n](/guide/ui/api/i18n)
:::

### Store

::: tip Vuex store
Consultez la documentation officielle du plugin Vuex pour Vue.js: [https://vuex.vuejs.org/guide/](https://vuex.vuejs.org/guide/)
:::

_MyStation_ utilise __Vuex__, le plugin Vue officiel pour la gestion des données partagées entre les composants et instances de Vue.

Pour simplifier la gestion des données, le store est composé de plusieurs modules, en plus du store racine.

Le store (et ses modules) utilisent les services:
- [APIManager](/guide/ui/api/mystation/services/app-manager) pour la gestion des requêtes API (__axios__ par défaut)
- [ErrorManager](/guide/ui/api/mystation/services/error-manager) pour la gestion des erreurs

::: see-also
- [Global API → Store](/guide/ui/api/store)
:::

#### Modules du store

##### Apps

Module du store permettant la gestion et le partage des données relatives aux apps.

- Gestion des données du serveur
- Gestion des apps du serveur _MyStation_ officiel

_MyStation_ possède un modèle de données pour les apps.

::: see-also
- [Global API → Store → Modules → Apps](/guide/ui/api/store/modules/Apps)
- [Global API → MyStation → Models → App](/guide/ui/api/mystation/models/App)
:::

##### Users

Module du store permettant la gestion et le partage des données relatives aux utilisateurs.

- Gestion des données du serveur
- Gestion des données de l'utilisateur courant
  - Méthode de connexion
  - Méthode de vérification de l'identification (Voir: [Identification](#identification))
  - Méthode de déconnexion

_MyStation_ possède un modèle de données pour les utilisateurs.

::: see-also
- [Global API → Store → Modules → Users](/guide/ui/api/store/modules/Users)
- [Global API → MyStation → Models → User](/guide/ui/api/mystation/models/User)
:::

### Services

Les services constituent le système interne de _MyStation_.

::: see-also
- [Global API → MyStation → Services](/guide/ui/api/mystation)
:::

#### Gestionnaire d'apps

Le gestionnaire d'apps permet:
- l'installation d'apps (l'installation est déclenchée via une requête vers le serveur, plus d'infos dans la rubrique [Serveur API](/guide/system/api-server))
- la désinstallation d'apps
- le chargement des bundles des apps (les scripts des apps sont chargés de manière asynchrone)
- l'enregistrement des traductions des apps
- l'enregistrement des composants des apps
  - composant principal
  - pancake (élément du Dashboard (widget) lié à une app (facultatif))
- l'enregistrement des modules de store de chaque apps (une app peut avoir ou non un ou plusieurs modules de store)

::: see-also
- [Global API → MyStation → Services → AppManager](/guide/ui/api/mystation/services/app-manager)
:::

#### Bus d'évènements

Le bus d'évènements est tout simplement une instance de Vue qui utilise les méthodes `$emit` et `$on`.

Les évenèments des __hooks__, par exemple, sont repertoriés dans le bus d'évènements.

::: see-also
- [Global API → MyStation → Services → EventBus](/guide/ui/api/mystation/services/event-bus)
:::

#### Gestionnaire d'API

Le gestionnaire d'API (APIManager) permet la gestion des requêtes HTTP asynchrone vers n'importe quel serveur API.

Par défaut, le gestionnaire d'API inclut l'API de MyStation (serveur web) ainsi que l'API de MyStation Web (http://mystation.fr/api/).

> Les APIs par défaut sont décrites dans le fichier de configuration (Voir: [Configuration](/fr/guide/ui/internal-flow/#configuration))

::: see-also
- [Global API → MyStation → Services → APIManager](/guide/ui/api/mystation/services/api-manager)
:::

### Modules

Les __modules__ de l'interface de _MyStation_ sont des fonctions ou des objets composés de fonctions au sein desquelles le context `this` de l'instance racine de _MyStation_ (`window.__MYSTATION_VUE_INSTANCE__`) est accessible.

Chaque __module__ vient donc se "greffer" à l'instance et constitue une sur-couche applicative au système interne de _MyStation_.

Ils sont généralement des fonctions raccourci vers des actions du __store__, des __services__, etc...
```javascript
/** 
 * Exemple: module 'auth'
 */
this.auth.user() // Contexte de l'instance racine
this.$root.auth.user() // Au sein de n'importe quel composant
```

::: see-also
- [Global API → MyStation → Modules](/guide/ui/api/mystation)
:::

### Initialisation

Une fois la méthode `this.boot()` appelée lors du hook __'created'__ de l'instance de Vue, _MyStation_ s'initialise:

- Assignation des __modules__ de _MyStation_ (Voir [Modules](/fr/guide/ui/internal-flow/#modules) ci-dessus)
- Initialisation de chaque __hook__ dans le bus d'évènements
- Emission de l'évènement `'boot-hook'` pour déclencher le démarrage (voir [Hooks](#hooks-de-cycle-de-vie))

::: see-also
- [Global API → MyStation](/guide/ui/api/mystation)
:::

### Hooks de cycle de vie

::: warning
Ne pas confondre les __hooks__ de _MyStation_ à ceux d'une instance de Vue.js.
:::

Le programme principal de l'interface de _MyStation_ est divisé en plusieurs méthodes appelées __hooks__ de cycle de vie, qui exécutent des instructions de manière _synchrone_:
- `BOOT` : vérification de l'identification
- `USER_LOGGED` : chargement des données du système, des utilisateurs et des apps depuis le serveur
- `APPS_DATA_LOADED` : synchronisation du gestionnaire d'apps (service AppManager) puis chargement des bundles des apps
- `APPS_BUNDLES_LOADED` : chaque app est ensuite enregistrée en parallèle

Ces méthodes sont associées à un évènement dans le bus d'évènements et appelées de manière _asynchrone_. (Voir schéma ci-dessous)

> Chaque hook possède une méthode `before()` et `after()` qui peuvent être surchargées.

<mermaid class="mermaid-sequence" filePath="/fr/guide/ui/internal-flow/mermaid/hooks.mmd"></mermaid>

::: see-also
- [Global API → MyStation → Hook](/guide/ui/api/mystation/hook)
:::

### Identification

L'identification fonctionne par token (jeton), selon le standard oAuth2. Côté serveur, c'est avec la librairie Passport pour Laravel qu'est géré ce système de connexion.

L'utilisateur entre d'abord ces identifiants dans le formulaire de connexion de l'interface qui sont envoyés au serveur API. Ce dernier va retourner un token qui sera stocké dans le LocalStorage du navigateur.

### Configuration

Le fichier de configuration principale de l'interface contient diverses informations nécessaires au fonctionnement du système. Il est situé à l'emplacement: `src/config/config.js`.

## Dépendances partagées

**MyStation** expose certaines dépendances dans l'objet `window.lib`:

|Nom                     |Cible                         |Documentation                                                                                                |
|-                       |-                             |-                                                                                                            |
|axios                   |`window.lib.axios`            |[https://axios-http.com/docs/intro](https://axios-http.com/docs/intro)                                       |
|color                   |`window.lib.color`            |[https://www.npmjs.com/package/color](https://www.npmjs.com/package/color)                                   |
|lodash.camelcase        |`window.lib._.camelcase`      |[https://www.npmjs.com/package/lodash.camelcase](https://www.npmjs.com/package/lodash.camelcase)             |
|lodash.capitalize       |`window.lib._.capitalize`     |[https://www.npmjs.com/package/lodash.capitalize](https://www.npmjs.com/package/lodash.capitalize)           |
|lodash.kebabcase        |`window.lib._.kebabcase`      |[https://www.npmjs.com/package/lodash.kebabcase](https://www.npmjs.com/package/lodash.kebabcase)             |
|lodash.maxby            |`window.lib._.maxby`          |[https://www.npmjs.com/package/lodash.maxby](https://www.npmjs.com/package/lodash.maxby)                     |
|lodash.minby            |`window.lib._.minby`          |[https://www.npmjs.com/package/lodash.minby](https://www.npmjs.com/package/lodash.minby)                     |
|lodash.snakecase        |`window.lib._.snakecase`      |[https://www.npmjs.com/package/lodash.snakecase](https://www.npmjs.com/package/lodash.snakecase)             |
|lodash.sortby           |`window.lib._.sortby`         |[https://www.npmjs.com/package/lodash.sortby](https://www.npmjs.com/package/lodash.sortby)                   |
|lodash.uppercase        |`window.lib._.uppercase`      |[https://www.npmjs.com/package/lodash.uppercase](https://www.npmjs.com/package/lodash.uppercase)             |
|vue                     |`window.lib.Vue`              |[https://vuejs.org/v2/guide/](https://vuejs.org/v2/guide/)                                                   |

## Apps & extensions

Les Apps et extensions sont des scripts indépendants du système de MyStation.

Pour plus d'informations concernant les Apps, veuillez vous référer à la documentation liée au [Développement d'app](/fr/guide/apps/).

::: warning
Les extensions ne sont pas encore implémentées dans cette version de MyStation.
:::

---
editLink: false
prev: /fr/guide/get-started/
next: /fr/guide/ui/internal-flow/
---

# Interface utilisateur

## Fonctionnement interne

[Explications détaillées du fonctionnement interne](/fr/guide/ui/internal-flow/)

## API globale

La documentation de l'API globale est disponible en [anglais uniquement](/guide/ui/api/)
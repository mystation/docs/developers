---
prev: /fr/guide/apps/
---

#  Développer une app pour MyStation

## Créer un nouveau projet

::: tip NOTE
Avant de démarrer, assurez-vous d'avoir installé **MyStation** ([documentation](/fr/guide/get-started/))
:::

Cloner le repository (remplacer `myapp` par le nom de votre app):

```bash
git clone https://gitlab.com/mystation/adk.git myapp
```

Installer les dépendances:

```bash
npm install
```

Mettez à jour le fichier `src/app.config.json`:

```json
{
    "name": "myapp",
    "version": "1.0.0",
    "mystation_version": "1.0.0",
    "title": "MyApp",
    "pancakes": [],
    "color": "#14DD56",
    "resources": []
}
```

::: tip CONSEIL
Pensez à modifier le fichier `package.json` pour mettre à jour les informations de votre projet.
:::

N'oubliez pas de mettre à jour l'adresse "remote" de votre répertoire Git (remplacer `<repository_url>`):
```bash
git remote rm origin
git remote add origin <repository_url>
```

Générer le fichier README.md:

```bash
npm run readme
```

::: tip CONSEIL
Vous pouvez maintenant effectuer votre premier commit ;)
:::

## Configuration

La configuration du projet s'effectue principalement dans le fichier `src/app.config.json`:

- `name` : Le nom (identifiant unique) de l'app en lettres minuscules
- `version` : La version de l'app
- `mystation_version` : La version de **MyStation** minimale compatible
- `title` : Le titre de l'app à afficher
- `pancakes` : Les pancakes de l'app (voir [la section dédiée](#pancakes))
- `color` : La couleur représentant l'app
- `resources` : Les resources de l'app (voir [la section dédiée](#resources))

## Développement

Pour démarrer le développement de l'app (webpack-dev-server), lancer la commande:

```bash
npm run dev
```

L'app devrait être lancée sur `localhost`, port `8081`.

Si vous souhaitez changer le port du serveur de développement, modifiez le fichier `vue.config.js`: 
```js{4}
// Configuration
module.exports = {
  // Full url for development, else default
  publicPath: process.env.NODE_ENV === 'development' ? 'http://localhost:8081/' : '/',
  ...
```

::: warning PRÉ-REQUIS
**MyStation** doit être lancé pour pouvoir développer votre app. Consultez la [documentation](/fr/guide/get-started/) pour démarrer **MyStation** en mode développement.
:::

Nous avons besoin de signaler à **MyStation** quelle app nous sommes en train de développer. Pour cela, il faut modifier le fichier [mystation/ui/src/config/config.js](https://gitlab.com/mystation/ui/-/blob/master/src/config/config.js) (remplacer `<myapp>` par le nom / identifiant unique définit précédemment et adapter en conséquence l'url et le port):

```js{7-15}
  ...
  /**
   * Development options
   */
  DEVELOPMENT: {
    ...
    /**
     * Enable app development mode
     * Use it when app development
     */
    appDevelopment: {
      <myapp>: {
        URL: 'http://localhost:8081'
      }
    }
  }
  ...
```

Vous devriez maintenant avoir **MyStation** lancé en mode développement (sur `localhost:8080` par défaut) et votre **app** lancée en mode développement (sur `localhost:8081` par défaut).

Rendez-vous sur l'interface **MyStation** (`localhost:8080` par défaut) dans l'**App Market**. Vous devriez voir votre app s'afficher en tête de liste.

Votre app est prête à être installée en mode développement:

![Installable app](/img/guide_apps_develop_img1.png)

Une fois votre app installée, ouvrez la console de votre navigateur. Vous devriez voir apparaître l'information suivante:

![Console screenshot](/img/guide_apps_develop_img2.png)

Lorsque vous installez l'app en mode développement, une nouvelle entrée est insérée en base de données dans la table 'apps'.
Les fichiers de l'app, quant à eux, sont chargées depuis `localhost:8081` (ou le port que vous avez choisi). D'où la nécessité d'avoir démarré le webpack-dev-server de votre app.

Voici comment **MyStation** procède pour charger votre app: au démarrage, lorsque le Hook `USER_LOGGED` est déclenché (voir [la section dédiée](/fr/guide/ui/internal-flow/#hooks-de-cycle-de-vie)), **MyStation** charge les données des apps installées depuis le serveur (déclenche le Hook `APPS_DATA_LOADED`), et synchronise le gestionnaire d'apps (service [AppManager](/fr/guide/ui/internal-flow/#services)) afin de charger les bundles de chaque app individuelle.

Regardons plus en détails comment le gestionnaire d'apps de **MyStation** procède pour chaque app:

- **1** - Chargement du bundle 'vendors' (vendors.js)
- **2** - Chargement du bundle 'app' (app.js)
- **3** - Enregistrement de l'app via l'appel de `__MYSTATION__.registerApp(__APP_NAME__, app)` à la fin du ficher `myapp/src/app.js` (voir la documentation concernant le [fichier d'entrée](#fichier-d-entree))
- **4** - Injection des fichiers de style
- **5** - Injection des traductions
- **6** - Enregistrement du composant principal de l'app
- **7** - Enregistrement du ou des composant(s) 'Pancake(s)' de l'app
- **8** - Injection du ou des module(s) de store de l'app

Une fois toutes ces étapes effectuées pour chaque app installée dans le système, le Hook `APPS_BUNDLES_LOADED` est déclenché.

## Structure du dossier

Nous pouvons maintenant démarré le développement de notre app.

Voyons dans un premier temps comment est organisé le dossier contenant les fichiers de l'app.

```{3-4,8,10,13,18,21,24-27}
.
├── node_modules/
├── public/
│   └── app.icon.png
├── readme/
├── src/
│   ├── components/
│   │   ├── pancakes/
│   │   │   └── MyAppPancake.vue (Optional)
│   │   └── tabs/
│   │       ├── MyApp.vue (Optional)
│   │       └── Options.vue (Optional)
│   ├── i18n/
│   │   ├── en.js
│   │   ├── fr.js
│   │   └── index.js
│   ├── init/
│   ├── scss/
│   │   ├── _variables.scss
│   │   └── main.scss
│   ├── store/
│   │   └── MyAppModule.js (Optional)
│   ├── utils/
│   ├── app.config.json
│   ├── app.js
│   ├── App.vue
│   └── update.js
├── tests/
├── package.json
├── README.md
├── vue.config.js
├── ...
```

Les dossiers et fichiers surlignés ci-dessus sont détaillés ci-dessous:

- `public/` : contient le fichier `app.icon.png` et les fichiers qui seront accessibles publiquement
- `src/components/` : dossier contenant les différents composants de l'app
- `src/components/pancakes/` : dossier contenant les différents pancakes de l'app
- `src/components/tabs/` : dossier contenant les composants des différents tabs de l'app
- `src/i18n/` : dossier contenant les composants des différents tabs de l'app
- `src/scss/` : dossier contenant les fichiers de style en language SASS
- `src/store/` : dossier contenant les différents modules de store de l'app
- `src/app.config.json` : fichier de configuration de l'app
- `src/app.js` : fichier d'entrée de l'app
- `src/App.vue` : composant racine de l'app
- `src/update.js` : fichier de traitement de mise à jour de l'app


## Fichier d'entrée (src/app.js)

Le fichier de l'entrée de l'application par défaut est le suivant:

```js
// Main app component
import App from '@/App.vue'
// Translations
import i18n from '@/i18n'
// Pancakes
import MyAppPancake from '@/components/pancakes/MyAppPancake'
// Store modules
import MyAppModule from '@/store/MyAppModule'
// Styles
import '#styles/main.scss'

// Define app object
const app = {
  // Translations
  i18n: i18n,
  // Components
  components: {
    // Main app component
    app: App,
    // Pancakes (MyStation Dashboard)
    pancakes: {
      MyAppPancake: MyAppPancake
    }
  },
  // Shared data
  store: {
    modules: {
      MyAppModule: MyAppModule
    }
  },
  // App style files to import
  styles: {
    // Main app styles (built as dist/css/app.css)
    app: 'app.css'
  }
}

// You can access to MyStation everywhere using '__MYSTATION__'
__MYSTATION__.registerApp(__APP_NAME__, app)
```

C'est dans ce fichier que se fait l'appel de la fonction `registerApp()` du système de **MyStation**. 
Il s'agit notamment du fichier où vous déclarez vos pancakes et modules de store.

Sauf si vous n'utilisez pas de composant pancakes ou de modules de store, vous ne devriez pas avoir à modifier ce fichier pour le moment.

::: warning ATTENTION
Les noms que vous utilisez et déclarez dans ce fichier pour les pancakes de votre app doivent être les mêmes que ceux déclarés dans le fichier `app.config.json`
:::

## Composants

### Composant racine (src/App.vue)

Le composant racine de l'app se trouve dans le fichier `src/App.vue`.

Le template pour ce composant racine doit être constitué d'un composant `AppContainer` pour pouvoir fonctionner avec **MyStation**.

Par défaut, le composant `AppContainer` propose une interface divisée en onglets (tabs).

```vue
<template>
  <!-- MyStation 'AppContainer' global component -->
  <AppContainer
    :app="GLOBALS.__APP_NAME__"
    :tabs="tabs"
    :defaultTab=0
  >
    <!-- Empty content -->
  </AppContainer>
</template>
```

Les propriétés du composant `AppContainer` sont les suivantes:

- `app` : (obligatoire) l'identifiant unique de l'app (par défaut `GLOBALS.__APP_NAME__` retourne dynamiquement le nom de l'app)
- `tabs` : (facultatif) un tableau d'onglets: objets Javascript au format:
  ```js
  {
    title: String, // (obligatoire) le texte de l'onglet
    component: VueComponent, // (obligatoire) le composant de l'onglet
    click: Function // (facultatif) la fonction à appeler lors du clic sur l'onglet
  }
  ```
- `defaultTab` : (facultatif) dans le cas d'utilisation des onglets, définit l'onglet affiché par défaut lors de l'ouverture de l'app (indice de 0 à X par rapport aux onglets définis dans `tabs`)

Vous pouvez choisir de ne pas utiliser le système d'onglets proposé par **MyStation**. Dans ce cas, les propriétés `tabs` et `defaultTab` ne sont pas nécessaires, c'est à vous de réaliser votre propre logique et d'appeler les différents composants à l'intérieur de composant `AppContainer`.

Jetons maintenant un oeil à la déclaration du composant racine:

```vue
<script>
import { BaseComponent } from '@/utils/'

import MyAppTab from '@/components/tabs/MyApp'
import OptionsTab from '@/components/tabs/Options'

import { init } from '@/init/'

export default {
  extends: BaseComponent,
  data () {
    return {
      tabs: [
        {
          title: __MYSTATION__.$h.capitalize(__MYSTATION__.$t(`${__T_PREFIX__}.tabs.myapp`)),
          component: MyAppTab,
          click: function () {
            console.log('MyApp tab clicked!')
          }
        },
        {
          title: __MYSTATION__.$h.capitalize(__MYSTATION__.$t(`${__T_PREFIX__}.tabs.options`)),
          component: OptionsTab,
          click: function () {
            console.log('Options tab clicked!')
          }
        }
      ]
    }
  },
  async mounted () {
    // Init call in main app component 'mounted' hook
    await init()
    // Do other stuff in mounted hook
    // ...
  }
}
</script>
```

Le composant racine doit hériter des propriétés du composant `BaseComponent` fourni avec l'ADK. Notez également l'appel à la fonction `init()` dans le hook *mounted* de l'instance de Vue. Cette fonction s'exécute à l'initialisation de l'app (voir ).

### Tabs

Les composants constituant les onglets de l'interface de votre app se situent par défaut dans le dossier `src/components/tabs`.

Le contenu des ces composants variera évidemment en fonction de chaque projet.

Par défaut, un onglet *Options* est proposé, voici son contenu:

```vue
<template>
  <div id="options-tab-content">
    <b-container>
      <b-row>
        <p>Options tab component</p>
      </b-row>
    </b-container>
  </div>
</template>

<script>
import { BaseComponent } from '@/utils/'

export default {
  extends: BaseComponent,
  data () {
    return {}
  }
}
</script>
```

Notez que ce composant hérite également des propriétés de `BaseComponent`.

### Pancakes

Les *Pancakes* de **MyStation** correspondent aux "gadgets" ajoutés au tableau de bord du système.

Les *Pancakes* sont chacuns liés à une app et permettent notamment de condenser des informations d'une app sur le tableau de bord (une sorte de raccourci).

::: tip A SAVOIR
Pourquoi les avoir appelé "Pancakes"? Il s'agit d'une métaphore pour désigner des éléments qui s'empilent sur le tableau de bord et offrent du contenu "consommable" à l'utilisateur 😁
:::

Au niveau de l'architecture, les composants des Pancakes sont situés par défaut dans le dossier `src/components/pancakes`.

Voici à quoi doit ressembler un composant "Pancake":

```vue
<template>
  <!-- MyStation 'Pancake' global component -->
  <pancake
    :appName="GLOBALS.__APP_NAME__"
    :ready="true"
    titleColor="default"
  >
    <!-- Pancake title -->
    <template slot="title">Title</template>
    <!-- Pancake content -->
    <template slot="content">
      <p>I'm a pancake of MyApp. I should display some things...</p>
    </template>
  </pancake>
</template>

<script>
import { BaseComponent } from '@/utils/'

export default {
  extends: BaseComponent
}
</script>
```

Vous devez utiliser le composant `pancake` fourni par **MyStation** pour l'intégrer au tableau de bord.

Ces composants héritent également des propriétés de `BaseComponent`.

Les propriétés du composant `pancake` sont les suivantes:

- `appName` : (obligatoire) l'identifiant unique de l'app (par défaut `GLOBALS.__APP_NAME__` retourne dynamiquement le nom de l'app)
- `ready` : (obligatoire) définit si le composant est prêt à s'afficher (affiche un chargement dans le cas contraire)
- `titleColor` : (facultatif) la couleur du titre du *Pancake*

Utilisez les slots:

- `title` pour le titre du *Pancake*...
- `content` pour le contenu du *Pancake*...

Pour être activé, nous avons vu précédemment qu'il faut déclarer le *Pancake* dans le fichier `src/app.js`:

```js{1,11}
import MyAppPancake from '@/components/pancakes/MyAppPancake'
...
const app = {
  ...
  // Components
  components: {
    // Main app component
    app: App,
    // Pancakes (MyStation Dashboard)
    pancakes: {
      MyAppPancake: MyAppPancake
    }
  }
  ...
}
```

Ainsi que dans le fichier `src/app.config.json`:

```json{3}
...
  "pancakes": [
    "MyAppPancake"
  ],
...
```

## Internationalisation

Un système de traduction est directement intégré dans **MyStation** à l'aide du package [vue-i18n](https://kazupon.github.io/vue-i18n/).

**MyStation** supporte actuellement les traductions françaises et anglaises.

Les fichiers contenant les traductions des textes se situent dans le dossier `src/i18n`:

```{4-5}
.
├── src/
│   ├── i18n/
│   │   ├── en.js
│   │   ├── fr.js
│   │   └── index.js
│   ├── ...
├── ...
```

Les traductions des apps installées sont chargées dynamiquement par **MyStation** et référencées par langue dans un objet `apps.<app_name>` (où <app_name> est l'identifiant unique de l'app).

Pour simplifier le développement, une variable globale `__T_PREFIX__` est mise à disposition:

```vue {6,13}
<template>
  <!--
      Par exemple, pour afficher le titre de l'onglet 'options'
      de notre app dans la partie HTML:
  -->
  <p>{{ $t(`${GLOBALS.__T_PREFIX__}.tabs.options`) }}</p>
</template>

<script>
// Pour afficher le titre de l'onglet 'options' de notre app dans la partie Javascript:
export default {
  mounted () {
    console.log(this.$t(`${__T_PREFIX__}.tabs.options`))
  }
}
</script>
```

## Modules de store vuex

Vous pouvez bénéficier des fonctionnalités du plugin [vuex](https://vuex.vuejs.org/) et créer des modules de store pour partager des données de votre app entre composants.

Le format d'un objet de module est simple:

```js
export default {
  namespaced: true,
  state: () => {
    return {
      // State here
    }
  },
  getters: {
    // Getters here
  },
  actions: {
    // Actions here
  },
  mutations: {
    // Mutations here
  }
}
```

De la même manière que pour les traductions de votre app, les modules de store vont être intégrés dynamiquement par **MyStation**.

Il suffit simplement de les déclarer dans le fichier `src/app.js`:

```js{8}
import MyAppModule from '@/store/MyAppModule'
...
const app = {
  ...
  // Shared data
  store: {
    modules: {
      MyAppModule: MyAppModule
    }
  },
  ...
}
```

Les modules de store sont nommés automatiquement par **MyStation** au format `<app_name>-<module_name>`. Dans l'exemple ci-dessus, le module sera nommé: `myapp-MyAppModule` mais vous n'avez pas besoin de retenir le nom et de l'écrire dans votre code, ni d'appeler les fonctions de vuex pour mapper les données de votre module (`mapState`, `mapActions`, etc..) puisque les composants de votre app héritent des propriétés `state`, `getters`, `actions` et `mutations` pour faciliter le développement. 

Par exemple, dans un composant de votre app:

```vue {16,21,24,27}
<template>
  <div>
    <ul>
      <li v-for="todo in todos">
        {{ todo }}
      </li>
    </ul>
  </div>
</template>

<script>
import { BaseComponent } from '@/utils/'

export default {
  extends: BaseComponent,
  state: {
    MyAppModule: [
      'todos'
    ]
  },
  getters: {
    MyAppModule: []
  },
  actions: {
    MyAppModule: []
  },
  mutations: {
    MyAppModule: []
  }
}
</script>

```

## Variables globales

Ces différentes variables globales sont accessibles dans le code Javascript et dans la partie `<script>...</script>` des composants Vue:

|Globale               |Type      |Description                                                                                                                                   |
|-                     |-         |-                                                                                                                                             |
|`__VUE__`             |Object    |Bibliothèque Vue (window.lib.Vue)                                                                                                             |
|`__MYSTATION__`       |Object    |Instance de **MyStation** racine. Raccourci pour `window.__MYSTATION_VUE_INSTANCE__`                                                          |
|`__MYSTATION_API__`   |Object    |Instance du gestionnaire d'API MyStation liée au serveur. Raccourci pour `window.__MYSTATION_VUE_INSTANCE__.$APIManager.use('myStationAPI')`  |
|`__APP_NAME__`        |String    |Identifiant unique de l'app (déclaré dans `src/app.config.json`)                                                                              |
|`__APP_VERSION__`     |String    |Version de l'app (déclarée dans `src/app.config.json`)                                                                                        |
|`__APP_COLOR__`       |String    |Couleur de l'app (déclarée dans `src/app.config.json`)                                                                                        |
|`__T_PREFIX__`        |String    |Préfixe de traduction. Chaîne de caractère: `apps.<app_name>`, où <app_name> est l'identifiant unique de l'app.                               |

À l'intérieur du code HTML (`<template>...</template>`) des composants Vue:

|Globale                   |Type      |Description                                                                                                              |
|-                         |-         |-                                                                                                                        |
|`GLOBALS.__APP_NAME__`    |String    |Identifiant unique de l'app (déclaré dans `src/app.config.json`)                                                         |
|`GLOBALS.__APP_VERSION__` |String    |Version de l'app (déclarée dans `src/app.config.json`)                                                                   |
|`GLOBALS.__APP_COLOR__`   |String    |Couleur de l'app (déclarée dans `src/app.config.json`)                                                                   |
|`GLOBALS.__T_PREFIX__`    |String    |Préfixe de traduction. Chaîne de caractère: `apps.<app_name>`, où <app_name> est l'identifiant unique de l'app.          |
|`GLOBALS.__UI_VERSION__`  |String    |Version de l'UI de **MyStation**.                                                                                        |
|`GLOBALS.__THEME_*...`    |String    |Toutes les variables globales de thème définies dans le fichier `globals.config.json` de **MyStation**.                  |

Exemple: pour afficher la couleur de l'app dans un composant:

```vue
<template>
  <p>Couleur de l'app: {{ GLOBALS.__APP_COLOR__ }}</p>
</template>

<script>
export default {
  mounted () {
    console.log(`Couleur de l'app: ${__APP_COLOR__}`)
    // Cela fonctionne aussi:
    console.log(`Couleur de l'app ${this.GLOBALS.__APP_COLOR__}`)
  }
}
</script>
```

## Resources

Pour **MyStation**, les *resources* de votre app sont les données liées à votre app qui ont besoin d'être stockées en base de données.

Le serveur de **MyStation** fonctionne avec le framework Laravel et suit donc sa logique de création de table et d'opérations sur la base de données (jetez un oeil à la [documentation Laravel](https://laravel.com/docs/5.4/migrations)).

Ces différentes *resources* doivent être déclarées dans le fichier `app.config.json`.

Par exemple, si l'on souhaite stocker des *tâches* (todos) en base de données et leur attribuer une catégorie:

```json
{
    "name": "myapp",
    "version": "1.0.0",
    "mystation_version": "1.0.0",
    "title": "MyApp",
    "pancakes": [
        "MyAppPancake"
    ],
    "color": "#14DD56",
    "resources": [
        {
            "table": "categories",
            "userRelated": true,
            "timestamps": false,
            "primaryKey": "id",
            "keyType": "int",
            "incrementing": true,
            "columns": [
                {
                    "name": "id", 
                    "type": "increments",
                    "comment": "Todo category id"
                },
                {
                    "name": "label",
                    "type": "string",
                    "comment": "Todo category label"
                },
                {
                    "name": "color",
                    "type": "string",
                    "comment": "Todo category color"
                }
            ]
        },
        {
            "table": "todos",
            "userRelated": true,
            "timestamps": false,
            "primaryKey": "id",
            "keyType": "int",
            "incrementing": true,
            "columns": [
                {
                    "name": "id", 
                    "type": "increments",
                    "comment": "Todo id"
                },
                {
                    "name": "label",
                    "type": "string",
                    "comment": "Todo label"
                }
            ],
            "foreigns": [
                {
                    "foreign": "category_id",
                    "references": "id",
                    "type": "unsignedInteger",
                    "on": "categories"
                }
            ]
        }
    ]
}
```

Voici une liste des différentes propriétés JSON supportées pour un élément du tableau *resources* dans le fichier `app.config.json`:

|Propriété                |Type         |Description                                                                        |
|-                        |-            |-                                                                                  |
|`table`                  |String       |Le nom de la table (et de la *resource*)                                           |
|`userRelated`            |Boolean      |Relation avec l'utilisateur (génère un champ *user_id*)                            |
|`timestamps`             |Boolean      |Génère les champs *created_at* et *updated_at*                                     |
|`primaryKey`             |String       |Nom de la colonne qui sera la clé primaire                                         |
|`keyType`                |String       |Type de la clé primaire                                                            |
|`incrementing`           |Boolean      |Détermine si la clé primaire doit être auto-incrémentée (doit être de type `int`)  |
|`columns`                |Array        |Définit les colonnes de la table                                                   |
|`columns[x].name`        |String       |Nom du champ (nom de la colonne)                                                   |
|`columns[x].type`        |String       |Type du champ                                                                      |
|`columns[x].comment`     |String       |Commentaire sur la colonne                                                         |
|`foreigns`               |Array        |Définitions des clés étrangères                                                    |
|`foreigns[x].foreign`    |String       |Nom de la clé étrangère (nom du champ)                                             |
|`foreigns[x].references` |String       |Nom du champ dans la table de référence                                            |
|`foreigns[x].type`       |String       |Type de la clé étrangère                                                           |
|`foreigns[x].on`         |String       |Nom de la table de référence                                                       |

**MyStation** met à disposition des outils permettant de faciliter la récupération et la manipulation des *resources* d'une app.

Il est possible de générer un ensemble de propriétés de *state*, de *getters*, d'*actions* et de *mutations* pour une *resource* dans le module de store de votre app.

Par exemple, ci-dessous le contenu du fichier `src/store/MyAppModule.js` pour une app permettant de gérer des tâches (*todos*) ayant des catégories attribuées:

```js
// Generate resource module mappers
const {
  todosState,
  todosGetters,
  todosActions,
  todosMutations
} = __MYSTATION__.generate.appModuleResourceMappers(__APP_NAME__, 'todos')

const {
  categoriesState,
  categoriesGetters,
  categoriesActions,
  categoriesMutations
} = __MYSTATION__.generate.appModuleResourceMappers(__APP_NAME__, 'categories')

export default {
  namespaced: true,
  state: () => {
    return {
      // Generate:
      // todos: [],
      // todosLoading: false,
      // todosCreating: false,
      // todosUpdating: false,
      // todosDeleting: false
      ...todosState,
      // Same for categories
      ...categoriesState
    }
  },
  getters: {
    // Generate:
    // findTodos()
    ...todosGetters,
    // Same for categories
    ...categoriesGetters
  },
  actions: {
    // Generate:
    // loadTodos()
    // createTodos()
    // updateTodos()
    // deleteTodos()
    ...todosActions,
    // Same for categories
    ...categoriesActions
  },
  mutations: {
    // Generate:
    // SET_TODOS()
    // SET_TODOS_LOADING()
    // SET_TODOS_CREATING()
    // SET_TODOS_UPDATING()
    // SET_TODOS_DELETING()
    ...todosMutations,
    // Same for categories
    ...categoriesMutations
  }
}

```

::: see-also
Consultez les [exemples](/fr/guide/apps/examples) d'apps dans la documentation.
:::

## Dépendances

Vous pouvez ajouter les dépendances que vous souhaitez à votre projet via la commande `npm install ...` mais sachez que **MyStation** expose certains packages que vous pouvez utiliser (voir la [liste des dépendances partagées](/fr/guide/ui/internal-flow/#dependances-partagees)).

Pour utiliser une dépendance partagée par **MyStation**, il suffit de le déclarer dans le fichier `vue.config.js` se situant à la racine du dossier de votre app:

```js{10-11}
...
module.exports = {
  ...
  // Webpack configuration
  configureWebpack: {
    ...
    // Externals
    externals: {
      'vue': 'lib.Vue' // target window.lib.Vue
      // Exemple: pour utiliser le package 'axios' partagé par MyStation
      'axios': 'lib.axios'
    },
    ...
  }
  ...
}
```

Puis de l'importer et l'utiliser comme n'importe quel package installé:

```js
import axios from 'axios'

axios.get(...)
```

## Compilation

Pour compiler le code de votre app, exécutez la commande:

```bash
npm run build
```

Cela va compiler le code source et générer le fichier *README.md* de votre app (voir la partie [documentation](./#documentation)).

Les fichiers compilés ainsi que l'archive de l'app se trouvent dans le dossier `dist/` généré:

```sh {7}
.
├── dist/
│   ├── css/              # CSS files
│   ├── js/               # JS files
│   ├── app.config.json   
│   ├── app.icon.png
│   └── myapp.zip         # App archive
├── ...
```

## Production

qergeqerg

## Mise à jour

qrgqerg

## Documentation

### Fichier README.md
---
editLink: false
prev: /fr/guide/monitor/
next: /fr/guide/system/database/
---

# Système

Cette partie de la documentation est consacrée au système global.

MyStation a besoin d'une base de données pour fonctionner et utilise nativement une base de données MySQL dont la documentation détaillée se trouve [ici](/fr/guide/system/database/).

Le [Serveur API](/fr/guide/system/api-server/), écrit en PHP, avec le framework Laravel, permet la communication entre l'interface utilisateur et la base de données.

Enfin, MyStation utilise différents outils (appelés *processes*) comme un serveur UDP, un watchdog, etc...

Veuillez consulter la documentation de ces [processus](/fr/guide/system/processes/).

## Dépendances

MyStation a besoin des dépendances suivantes pour fonctionner correctement:

- Apache2 v2.4.38 (Raspbian)
- PHP v7.3.X
- Composer v1.8.X
- MySQL (mariadb) v10.3.X
- Node.js v10.21.X & NPM v6.14.X
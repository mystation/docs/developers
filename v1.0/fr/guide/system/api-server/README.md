# Serveur API

Le serveur API de MyStation est écrit en PHP, à l'aide du framework Laravel.

Il s'agit de l'intermédiaire entre l'interface utilisateur et le système (ainsi que la base de données):

<mermaid class="mermaid-flowchart" filePath="/fr/guide/system/api-server/mermaid/api_server_relations.mmd"></mermaid>

## Routes

| Method    | URI                                          | Action                                                                     | Middleware                                |
|-----------|----------------------------------------------|----------------------------------------------------------------------------|-------------------------------------------|
| GET\|HEAD | /                                            | Closure                                                                    | web                                       |
| GET\|HEAD | api/apps                                     | App\Http\Controllers\AppController@index                                   | api, auth:api, admin_psswd_changed        |
| POST      | api/apps/activate                            | App\Http\Controllers\AppController@activate                                | api, auth:api, admin, admin_psswd_changed |
| POST      | api/apps/deactivate                          | App\Http\Controllers\AppController@deactivate                              | api, auth:api, admin, admin_psswd_changed |
| POST      | api/apps/install                             | App\Http\Controllers\AppController@install                                 | api, auth:api, admin, admin_psswd_changed |
| POST      | api/apps/uninstall                           | App\Http\Controllers\AppController@uninstall                               | api, auth:api, admin, admin_psswd_changed |
| POST      | api/apps/updateResources                     | App\Http\Controllers\AppController@updateResources                         | api, auth:api, admin, admin_psswd_changed |
| GET\|HEAD | api/apps/{name}                              | App\Http\Controllers\AppController@find                                    | api, auth:api, admin_psswd_changed        |
| POST      | api/apps/{name}/init                         | App\Http\Controllers\AppController@init                                    | api, auth:api, admin_psswd_changed        |
| GET\|HEAD | api/apps/{name}/resources                    | App\Http\Controllers\AppResourceController@allByApp                        | api, auth:api, admin_psswd_changed        |
| POST      | api/apps/{name}/resources                    | App\Http\Controllers\AppResourceController@find                            | api, auth:api, admin_psswd_changed        |
| GET\|HEAD | api/apps/{name}/resources/{resource}         | App\Http\Controllers\AppResourceGenericController@all                      | api, auth:api, admin_psswd_changed        |
| DELETE    | api/apps/{name}/resources/{resource}         | App\Http\Controllers\AppResourceGenericController@delete                   | api, auth:api, admin_psswd_changed        |
| PATCH     | api/apps/{name}/resources/{resource}         | App\Http\Controllers\AppResourceGenericController@update                   | api, auth:api, admin_psswd_changed        |
| POST      | api/apps/{name}/resources/{resource}         | App\Http\Controllers\AppResourceGenericController@create                   | api, auth:api, admin_psswd_changed        |
| GET\|HEAD | api/apps/{name}/resources/{resource}/{pkval} | App\Http\Controllers\AppResourceGenericController@find                     | api, auth:api, admin_psswd_changed        |
| POST      | api/changeAdminPassword                      | App\Http\Controllers\UserController@changeAdminPassword                    | api                                       |
| GET\|HEAD | api/me                                       | App\Http\Controllers\UserController@me                                     | api, auth:api, admin_psswd_changed        |
| POST      | api/me/logout                                | App\Http\Controllers\UserController@logout                                 | api, auth:api, admin_psswd_changed        |
| POST      | api/oauth/authorize                          | \Laravel\Passport\Http\Controllers\ApproveAuthorizationController@approve  | web, auth                                 |
| GET\|HEAD | api/oauth/authorize                          | \Laravel\Passport\Http\Controllers\AuthorizationController@authorize       | web, auth                                 |
| DELETE    | api/oauth/authorize                          | \Laravel\Passport\Http\Controllers\DenyAuthorizationController@deny        | web, auth                                 |
| POST      | api/oauth/clients                            | \Laravel\Passport\Http\Controllers\ClientController@store                  | web, auth                                 |
| GET\|HEAD | api/oauth/clients                            | \Laravel\Passport\Http\Controllers\ClientController@forUser                | web, auth                                 |
| PUT       | api/oauth/clients/{client_id}                | \Laravel\Passport\Http\Controllers\ClientController@update                 | web, auth                                 |
| DELETE    | api/oauth/clients/{client_id}                | \Laravel\Passport\Http\Controllers\ClientController@destroy                | web, auth                                 |
| POST      | api/oauth/personal-access-tokens             | \Laravel\Passport\Http\Controllers\PersonalAccessTokenController@store     | web, auth                                 |
| GET\|HEAD | api/oauth/personal-access-tokens             | \Laravel\Passport\Http\Controllers\PersonalAccessTokenController@forUser   | web, auth                                 |
| DELETE    | api/oauth/personal-access-tokens/{token_id}  | \Laravel\Passport\Http\Controllers\PersonalAccessTokenController@destroy   | web, auth                                 |
| GET\|HEAD | api/oauth/scopes                             | \Laravel\Passport\Http\Controllers\ScopeController@all                     | web, auth                                 |
| POST      | api/oauth/token                              | \Laravel\Passport\Http\Controllers\AccessTokenController@issueToken        | throttle                                  |
| POST      | api/oauth/token/refresh                      | \Laravel\Passport\Http\Controllers\TransientTokenController@refresh        | web, auth                                 |
| GET\|HEAD | api/oauth/tokens                             | \Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController@forUser | web, auth                                 |
| DELETE    | api/oauth/tokens/{token_id}                  | \Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController@destroy | web, auth                                 |
| GET\|HEAD | api/system/database                          | App\Http\Controllers\MsController@database                                 | api, auth:api, admin_psswd_changed        |
| GET\|HEAD | api/system/network                           | App\Http\Controllers\MsController@network                                  | api, auth:api, admin_psswd_changed        |
| POST      | api/system/update                            | App\Http\Controllers\MsController@updateSystem                             | api, auth:api, admin, admin_psswd_changed |
| GET\|HEAD | api/system/version                           | App\Http\Controllers\MsController@version                                  | api, auth:api, admin_psswd_changed        |
| GET\|HEAD | api/users                                    | App\Http\Controllers\UserController@all                                    | api, auth:api, admin_psswd_changed        |
| POST      | api/users/create                             | App\Http\Controllers\UserController@create                                 | api, auth:api, admin, admin_psswd_changed |
| POST      | api/users/{userId}/delete                    | App\Http\Controllers\UserController@delete                                 | api, auth:api, admin_psswd_changed        |
| POST      | api/users/{userId}/resetPassword             | App\Http\Controllers\UserController@resetUserPassword                      | api, auth:api, admin_psswd_changed        |
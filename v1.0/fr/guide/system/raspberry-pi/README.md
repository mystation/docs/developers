# Raspberry Pi

## Image Raspberry Pi OS

Basée sur l'image d'origine:

`Raspberry Pi OS (32-bit) - RELEASED: 2020-05-27`

### Modifications

#### Configuration (facultatif)

> Cette étape est facultative. Cela permet d'outrepasser le comportement par défaut qui va redimensionner la taille de la partition principale en l'adaptant à la carte SD utilisée.
> C'est important si vous prévoyez de partager l'image plus tard ou si vous voulez optimiser la taille de l'image.

Avant la première utilisation de la carte SD (juste après avoir écrit l'image par défaut sur la carte):
- Parcourir le contenu de la carte SD
- Trouver le fichier *cmdline.txt* (il contient une commande lancée au premier boot de la carte)
- Copier/coller ce fichier sous un autre nom: *old_cmdline.txt* (par exemple)
- Modifier le contenu du fichier *cmdline.txt*:
  - supprimer 'init=/usr/lib/raspi-config/init_resize.sh'
- Insérer la carte SD dans la Raspberry Pi et démarrer
- Lancer les commandes suivantes:
  ```bash
  # D'abord, récupérer le secteur START de la partition
  echo $(sudo parted -m /dev/mmcblk0 unit s print | tr -d 's' | grep -e "^2:" | cut -d ":" -f 2)
  # Increase rootfs partition size: use fdisk
  # Augmenter la taille de la partition rootfs: utiliser fdisk
  # mmcblk0 représente le disque
  sudo fdisk /dev/mmcblk0
  ```
  - `d` pour supprimer
  - `2` pour supprimer la partition 2
  - `n` pour créer une nouvelle partition
  - `p` popur "primaire"
  - `2` pour créer la partition 2
  - Start: entrer la valeur trouvée précédemment pour le secteur START
  - `+6G` pour redimensionner à 6GB
  - Choisir 'No' lorsqu'il est demandé de supprimer la signature
  - `w` pour appliquer les changements
  Redémarrer `sudo reboot`
  Ensuite, redimensionner le système de fichier à la taille de la nouvelle partition:
  ```bash
  # mmcblk0p2 est la partition rootfs (id: 2)
  sudo resize2fs /dev/mmcblk0p2
  ```

#### 1 - raspi-config

- Activer SSH
- Configurer les locales:
  - en_GB utf_8
  - en_US utf_8 (default)
  - fr_FR utf_8
- Changer le nom d'hôte: `rpi-mystation`

#### 2 - Installation des packages

- Mettre à jour:
  ```bash
  sudo apt-get update && sudo apt-get upgrade
  ```
- apache2:
  ```bash
  sudo apt-get install apache2
  ```
- php7.3, composer & extensions nécessaires:
  ```bash
  sudo apt-get install php7.3 composer php-xml php-zip php-mysql php7.3-gd
  ```
- mariadb-server:
  ```bash
  sudo apt-get install mariadb-server
  ```
- node & npm:
  ```bash
  # Ajouter le dépôt
  curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
  # Installer
  sudo apt-get install nodejs
  ```

#### 3 - Utilisateurs

- Créer *admin* en tant que sudoer (login: `admin`, mdp: `admin`):
  ```bash
  sudo adduser admin
  sudo adduser admin sudo
  sudo visudo /etc/sudoers.d/010_admin-nopasswd
  # insérer le contenu:
  admin ALL=(ALL) NOPASSWD: ALL
  # donner les droits à www-data
  sudo visudo
  # ajouter à la fin:
  # Permet à www-data d'exécuter les commandes
  www-data ALL=(ALL) NOPASSWD:ALL
  ```
- Vérouiller l'utilisateur *pi*:
  ```bash
  sudo usermod -L pi
  ```

#### 4 - Configuration Apache

- Désactiver le site par défaut:
  ```bash
  sudo a2dissite 000-default
  ```

#### 5 - Configuration MySQL

- Désactiver l'utilisateur *root* et créer l'utilisateur *admin*:
  ```bash
  sudo mysql
  # SQL script:
  CREATE USER 'admin'@'localhost' IDENTIFIED BY 'admin';
  GRANT ALL ON *.* TO 'admin'@'localhost';
  FLUSH PRIVILEGES;
  REVOKE ALL ON *.* FROM 'root'@'localhost';
  ```
  Reboot system `sudo reboot`

#### 6 - Extras

Se connecter en tant que `admin`

- Ajouter les alias
  ```bash
  # as 'admin' user
  echo "alias ll='ls -lArth'" >> ~/.bash_aliases
  ```
- Désactiver l'affichage sur le bureau de l'utilitaire 'Welcome to Raspberry Pi':
  ```bash
  sudo rm /etc/xdg/autostart/piwiz.desktop
  sudo reboot now
  ```

#### 7 - Installation MyStation

- Créer le dossier mystation:
  ```bash
  sudo mkdir /mystation
  ```
- Rendre l'utilisateur admin propriétaire:
  ```bash
  sudo chown admin:admin /mystation
  ```
- Cloner les sources depuis git:
  ```bash
  cd /mystation
  git clone https://gitlab.com/mystation/mystation.git .
  ```
- Exécuter le script 'install':
  ```bash
  bash /mystation/install/install.sh
  ```

#### Configuration post-installation (facultatif)
> Cette étape est facultative. Elle est cependant requise si vous avez suivi la première étape *Configuration de l'image*.

Ne pas oublier de modifier le fichier *cmdline.txt* (ajouter le contenu suivant AVANT de lire la carte SD pour créer une image):
```bash
init=/usr/lib/raspi-config/init_resize.sh
# Notez que l'ancien fichier est sauvegardé en tant que  'old_cmdline.txt'
# (ou le nom que vous avez choisi) pour retrouver le contenu
```

Ceci réactivera le redimensionnement automatique de la partition principale (comportement par défaut).
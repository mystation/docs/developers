# Base de données

Le système de gestion de base de données est de type MySQL (MariaDB).

L'utilisateur de la base de données est:

- nom d'utilisateur : `admin`
- mot de passe : `admin`

## Tables

- app_pancakes | Pancakes des Apps
- app_resources | Ressources des Apps
- apps | Apps installées sur le système
- migrations | Propre à Laravel
- oauth_access_tokens | Générée avec Passport
- oauth_auth_codes | Générée avec Passport
- oauth_clients | Générée avec Passport
- oauth_personal_access_clients | Générée avec Passport
- oauth_refresh_tokens | Générée avec Passport
- password_resets
- processes | Processes enfants de MyStation
- system_data | Données systèmes diverses
- user_apps | Apps par utilisateur
- user_pancakes | Pancakes d'apps activés par utilisateur
- users | Utilisateurs

## Procédures stockées

- getDatabaseInfos | Retourne différentes informations concernant la base de données *mystation*
- getSystemData | Retourne différentes informations propres au système *mystation*

## Initialisation

Remplacer `<username` et `<password>` par les identifiants MySQL:

```bash
/mystation/database/init.sh <username> <password>
```

## Sauvegarde

Remplacer `<username` et `<password>` par les identifiants MySQL:

```bash
/mystation/database/dump.sh <username> <password>
```

Cette commande va créer une sauvegarde de la base de données nommée selon le format suivant `mystation_YYYY_MM_DD_HHMMSS.sql` dans le sous-dossier `dumps`.
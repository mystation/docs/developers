const sidebar_en = require('../sidebar')
const sidebar_fr = require('../fr/sidebar')

// Export config
module.exports = {
  //Base 
  base: '/v1.0/',
  // VuePress plugins
  plugins: [
    // Add copy button for code blocks
    ['vuepress-plugin-code-copy', {
      // selector: String,
      align: 'bottom',
      color: '#7BC6AF' // $ms-blue
    }],
    // For custom containers
    ['vuepress-plugin-container', {
      type: 'syntax',
      before: info => `<div class="custom-block syntax"><p class="title color-beige">Syntax: <code>${info}</code></p>`,
      after: '</div>',
    }],
    ['vuepress-plugin-container', {
      type: 'see-also',
      defaultTitle: {
        '/': 'See also',
        '/fr/': 'Voir aussi'
      },
      before: info => `<div class="custom-block see-also"><p class="custom-block-title">${info}:</p>`,
      after: '</div>',
    }],
    // Check dead links
    ['check-md', {
      pattern: '**/*.md',
      ignore: '**/node_modules|**/ui/api'
    }],
    // Reading progress bar
    'reading-progress',
    // Smooth scrolling
    'vuepress-plugin-smooth-scroll',
    // Make images zoomable
    'vuepress-plugin-zooming',
    // For SEO
    ['seo', { /* options */ }],
    // For sitemap
    ['sitemap', {
      hostname: 'http://developers.mystation.fr'
    }],
    // For canonical urls
    ['vuepress-plugin-canonical', {
        // baseURL: 'https://mina.wiki', // base url for your canonical link, optional, default: ''
        // stripExtension: true // strip '.html' , optional, default: false
      }
    ],
    // To improve last updated
    ['@vuepress/last-updated']
  ],
  // Display line numbers in code blocks
  markdown: {
    lineNumbers: true
  },
  // Overwrite webpack config
  chainWebpack: config => {
    config.module
      .rule('mmd')
      .test(/\.mmd$/)
      .exclude
        .add(/node_modules/)
        .end()
      .use('raw-loader')
        .loader('raw-loader')
  },
  // Locales
  locales: {
    // The key is the path for the locale to be nested under.
    // As a special case, the default locale can use '/' as its path.
    '/': {
      lang: 'en-US', // this will be set as the lang attribute on <html>
      title: 'MyStation for developers',
      description: 'English documentation'
    },
    '/fr/': {
      lang: 'fr-FR',
      title: 'MyStation for developers',
      description: 'Documentation française',
    }
  },
  // Theme config
  themeConfig: {
    logo: 'http://mystation.fr/static/img/logo.png',
    // Assumes GitHub. Can also be a full GitLab url.
    repo: 'https://gitlab.com/mystation',
    // Customising the header label
    // Defaults to "GitHub"/"GitLab"/"Bitbucket" depending on `themeConfig.repo`
    repoLabel: 'GitLab',
    // Optional options for generating "Edit this page" link
    // if your docs are in a different repo from your main project:
    docsRepo: 'https://gitlab.com/mystation/mystation-docs-developers/',
    // if your docs are in a specific branch (defaults to 'master'):
    docsBranch: 'master',
    // defaults to false, set to true to enable
    editLinks: true,
    // Sidebar depth
    sidebarDepth: 6,
    // Languages specific
    locales: {
      // The key is the path for the locale to be nested under.
      // As a special case, the default locale can use '/' as its path.
      '/': {
        // Navbar button label for translations
        selectText: 'Language',
        label: 'English',
        // custom text for edit link. Defaults to "Edit this page"
        editLinkText: 'Edit this page',
        searchPlaceholder: 'Search...',
        lastUpdated: 'Last Updated', // string | boolean
        nav: [
          { text: 'Home', link: '/' },
          { text: 'Guide', link: '/guide/introduction/' }
        ],
        sidebar: sidebar_en
      },
      '/fr/': {
        // Navbar button label for translations
        selectText: 'Langue',
        label: 'Français',
        // custom text for edit link. Defaults to "Edit this page"
        editLinkText: 'Editer cette page',
        searchPlaceholder: 'Recherche...',
        lastUpdated: 'Dernière modification', // string | boolean
        nav: [
          { text: 'Accueil', link: '/fr/' },
          { text: 'Guide', link: '/fr/guide/introduction/' }
        ],
        sidebar: sidebar_fr
      }
    }
  }
}